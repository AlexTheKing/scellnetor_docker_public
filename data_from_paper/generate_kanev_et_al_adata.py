#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 15:07:11 2019

@author: alexander
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as pl
import scanpy.api as sc

sc.settings.verbosity = 3



def splitter(w):
    t = w.split(".")
    tt = []
    for i in t:
        tt += i.split("-")
        
    ttt = []
    for i in tt:
        ttt += i.split("_")
    return "_".join(ttt)



### below is the on they used in their paper --> everything has been preporcessed
## https://www.pnas.org/content/pnas/suppl/2019/09/16/1902701116.DCSupplemental/pnas.1902701116.sapp.pdf
adata =sc.read_text("GSE137007_Run36_KK_UMI_Top2000_Scale.txt")



mtx = adata.X

cells = list(adata.var.index)

genes = list(adata.obs.index)


df = pd.DataFrame(mtx.T,index=None, columns=genes)
df.insert(loc=0, column='Cells', value=cells)
df.to_csv("test.csv",sep=",", index=False)
    

adata = sc.read_csv("test.csv")

#################
#Above it is read correctly
##########



df1 = pd.read_csv("GSE137007_Run36_KK_Cell_ClusterID.txt", sep=" ")
adata.obs["paper_clust"] = [str(i) for i in list(df1["cluster_id"])]



"""
cluster 1: effector-progenitor cells
cluster 2: helper dependent
cluster 3: shared helped and unhelped
cluster 4: mainly unhelped 
cluster 5: unique unhelped


1 is their 3
2 is their 1
3 is their 2
4 is their 4
5 is their 5
"""
## their clusters are wrongly annotated on GEO
change_clsts = {"1":"3", "2":"1","3":"2", "4":"4", "5":"5"}
true_clsts = []
for i in adata.obs["paper_clust"]:
    true_clsts.append(change_clsts[i])

adata.obs["true_clsts"] = true_clsts 


## reading control cells and deplt cells
cont = ['L1-Seq36-scExp04-1_S1',
 'L13-Seq36-scExp04-1_S13',
 'L14-Seq36-scExp04-1_S14',
 'L15-Seq36-scExp04-1_S15',
 'L2-Seq36-scExp04-1_S2',
 'L3-Seq36-scExp04-1_S3',
 'L7-Seq36-scExp04-1_S7',
 'L8-Seq36-scExp04-1_S8',
 'L9-Seq36-scExp04-1_S9']


deplt= ['L10-Seq36-scExp04-1_S10',
 'L11-Seq36-scExp04-1_S11',
 'L12-Seq36-scExp04-1_S12',
 'L16-Seq36-scExp04-1_S16',
 'L17-Seq36-scExp04-1_S17',
 'L18-Seq36-scExp04-1_S18',
 'L4-Seq36-scExp04-1_S4',
 'L5-Seq36-scExp04-1_S5',
 'L6-Seq36-scExp04-1_S6']


con = [splitter(i) for i in cont]

dep = [splitter(i) for i in deplt]


con_or_not = []
for i in adata.obs.index:
    tmp = "_".join(i.split("_")[:-1])
    tmp = splitter(tmp)
    if tmp in con and tmp in dep:
        print("SHOT")
        break
    if tmp in con:
        con_or_not.append("control")
    if tmp in dep:
        con_or_not.append("w/o CD4")


def get_timeline_coordinates(adata, type1="draw_graph_fa"):
    """
    type can either be diffmap or draw_graph_fa
    """
    from scanpy.plotting._tools.scatterplots import _get_data_points
    V = _get_data_points(adata, basis=type1, projection="2d", components='1,2')#img_key=None, library_id=None) # for sc 1.6.0
    return V[0][0]



adata.obs["controls"] = con_or_not 




## PCA
sc.tl.pca(adata, svd_solver='arpack')
sc.pl.pca(adata)
sc.pl.pca_variance_ratio(adata, log=True)


"""
The Seurat package was used for further processing (6). 
Cell  subpopulations  were  detected  by  Louvain  clustering (7) 
with  top  5  principle components and K.para = 150.
"""
sc.pp.neighbors(adata, n_neighbors=150, n_pcs=5)

##3 PAGA
sc.tl.paga(adata, groups='true_clsts')
sc.pl.paga(adata)#, groups='paper_clust')


### UMMP
sc.tl.umap(adata, init_pos="paga")
sc.tl.louvain(adata)
sc.pl.umap(adata, color=["true_clsts", "controls"])

sc.pl.umap(adata, color=['true_clsts', "Tcf7", "Gzma", "Gzmb", "Gzmk", "Fasl", "Id2", "Tbx21"], save="Fig2_from_paper.pdf")
sc.pl.umap(adata, color=['true_clsts', "Tcf7", "Gzma", "Gzmb", "Gzmk", "Fasl", "Id2", "Tbx21"], save="Fig2_from_paper.png")
sc.pl.umap(adata, color=['true_clsts', "Klf2", "S1pr1", "S1pr5", "Cx3cr1"], save="Fig3_from_paper.pdf")
sc.pl.umap(adata, color=['true_clsts', "Klf2", "S1pr1", "S1pr5", "Cx3cr1"], save="Fig3_from_paper.png")



sc.tl.diffmap(adata, n_comps=15)
sc.pl.diffmap(adata)
co = get_timeline_coordinates(adata, type1="diffmap")

## Depending on the shape of the plot.
start = np.argmin(co[:,1])
#start = np.argmax(co[:,1])


"""
## sanity check
import sklearn.metrics
zz = sklearn.metrics.pairwise.euclidean_distances(dif, dif)
zz1 = zz[adata.obs["true_clsts"] == "1"]
zz2 = zz1[:,adata.obs["true_clsts"] != "1"]
start = np.arange(len(zz))[adata.obs["true_clsts"] == "1"][np.argmax(np.sum(zz2,axis=-1))]
"""

## This is how they suggest how to do it in the documentation
adata.uns['iroot'] = start #np.flatnonzero(adata.obs['true_clsts']  == '1')[0]
## Don't know what difference this makes wrt. the parameters
sc.tl.dpt(adata, n_dcs=15)
sc.pl.diffmap(adata, color=["dpt_pseudotime","true_clsts", "controls"], save="diffmap.pdf")
sc.pl.diffmap(adata, color=["dpt_pseudotime","true_clsts", "controls"], save="diffmap.png")

#sc.tl.paga(adata)#, groups='paper_clust')
sc.tl.draw_graph(adata, init_pos='paga')
sc.pl.draw_graph(adata, color=["dpt_pseudotime","true_clsts", "controls"], save="draw_graph.pdf")
sc.pl.draw_graph(adata, color=["dpt_pseudotime","true_clsts", "controls"], save="draw_graph.png")


adata.write_h5ad("dietmar.h5ad")


sc.pl.umap(adata, color=['dpt_pseudotime', "true_clsts", "controls"], save="pseudo_paper-clst_controls.pdf")
sc.pl.umap(adata, color=['dpt_pseudotime', "true_clsts", "controls"], save="pseudo_paper-clst_controls.png")


