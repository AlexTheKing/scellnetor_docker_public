#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  7 09:14:39 2020

@author: alexander
"""
import scanpy as sc
import pickle
import numpy as np
import pandas as pd

# From the Paul_et_al folder in the gitlab repos
adata = sc.read_h5ad("test1.h5ad")
with open("objects.pkl", "rb") as f:
    ob = pickle.load(f)
    
    
z = np.zeros((len(adata.X,)))
z[ob["red"]] = 1    #amber
z[ob["green"]] = 2  #navy blue
z[list(set(ob["red"]).intersection(set(ob["green"])))] = 0
z = [str(int(i)) for i in z]


adata.obs["grr"] = list(z)


# Finding the significantly different expressed genes in group 1 vs. 2 (amber vs blue)
sc.tl.rank_genes_groups(adata,"grr", groups=["1"], method="wilcoxon", reference="2", n_genes=100)
_12 = adata.uns["rank_genes_groups"]
n12 = [i[0] for i in _12["names"]]

# Finding the significantly different expressed genes in group 2 vs. 1 (blue vs. amber)
sc.tl.rank_genes_groups(adata,"grr", groups=["2"], method="wilcoxon", reference="1", n_genes=100)
_21 = adata.uns["rank_genes_groups"]
n21 = [i[0] for i in _21["names"]]


# From the Paul_et_al folder in the gitlab repos
# Getting the used genes from the Scellnetor cluster-search
with open("genes_in_set.pkl", "rb") as f:
    gn = pickle.load(f)

# Getting the mouse gene symbols for all used genes in the Scellnetor cluster-search
mgns = list(np.array(adata.var.index)[gn[2]])
# Getting the human gene symbols for all used genes in the Scellnetor cluster-search
hmgns = gn[1]


# Converting the significantly expressed genes to human gene symbols
n12_ = set([hmgns[mgns.index(ii)] for ii in n12 if ii in mgns])
n21_ = set([hmgns[mgns.index(ii)] for ii in n21 if ii in mgns])

# some of the significant genes have been removed in the Scellneto pipeline
print("% of genes included by Scellnetor for 1 vs. 2:",len(n12_)/len(n12), len(n12_),len(n12))
print("% of genes included by Scellnetor for 2 vs. 1:", len(n21_)/len(n21),len(n21_),len(n21))




# From the Paul_et_al folder in the gitlab repos
for i in range(7):
    print(str(i)+"_graph_symbol.csv")
    tmp = pd.read_csv(str(i)+"_graph_symbol.csv", header=None, sep="\t")
    tmp = set(list(tmp[0]) + list(tmp[1]))
    #tmp = set([hmgns[mgns.index(ii)] for ii in mgns])
    print("overlap with 1 vs. 2.: ", len(n12_.intersection(tmp)), "out of", len(tmp), "percent:",len(n12_.intersection(tmp))/len(tmp))
    print("overlap with 2 vs. 1.: ", len(n21_.intersection(tmp)), "out of", len(tmp), "percent:",len(n21_.intersection(tmp))/len(tmp))




"""
for 100 significant genes:
    
% of genes included by Scellnetor for 1 vs. 2: 0.9 90 100
% of genes included by Scellnetor for 2 vs. 1: 0.8 80 100

0_graph_symbol.csv
overlap with 1 vs. 2.:  11 out of 13 percent: 0.8461538461538461
overlap with 2 vs. 1.:  0 out of 13 percent: 0.0
1_graph_symbol.csv
overlap with 1 vs. 2.:  0 out of 11 percent: 0.0
overlap with 2 vs. 1.:  4 out of 11 percent: 0.36363636363636365
2_graph_symbol.csv
overlap with 1 vs. 2.:  0 out of 5 percent: 0.0
overlap with 2 vs. 1.:  2 out of 5 percent: 0.4
3_graph_symbol.csv
overlap with 1 vs. 2.:  5 out of 8 percent: 0.625
overlap with 2 vs. 1.:  0 out of 8 percent: 0.0
4_graph_symbol.csv
overlap with 1 vs. 2.:  0 out of 5 percent: 0.0
overlap with 2 vs. 1.:  1 out of 5 percent: 0.2
5_graph_symbol.csv
overlap with 1 vs. 2.:  0 out of 81 percent: 0.0
overlap with 2 vs. 1.:  32 out of 81 percent: 0.3950617283950617
6_graph_symbol.csv
overlap with 1 vs. 2.:  0 out of 6 percent: 0.0
overlap with 2 vs. 1.:  5 out of 6 percent: 0.8333333333333334

----------------------------------------------------------------------------

For 200 significant genes:
    
% of genes included by Scellnetor for 1 vs. 2: 0.875 175 200
% of genes included by Scellnetor for 2 vs. 1: 0.835 167 200

0_graph_symbol.csv
overlap with 1 vs. 2.:  13 out of 13 percent: 1.0
overlap with 2 vs. 1.:  0 out of 13 percent: 0.0
1_graph_symbol.csv
overlap with 1 vs. 2.:  0 out of 11 percent: 0.0
overlap with 2 vs. 1.:  9 out of 11 percent: 0.8181818181818182
2_graph_symbol.csv
overlap with 1 vs. 2.:  0 out of 5 percent: 0.0
overlap with 2 vs. 1.:  4 out of 5 percent: 0.8
3_graph_symbol.csv
overlap with 1 vs. 2.:  7 out of 8 percent: 0.875
overlap with 2 vs. 1.:  0 out of 8 percent: 0.0
4_graph_symbol.csv
overlap with 1 vs. 2.:  0 out of 5 percent: 0.0
overlap with 2 vs. 1.:  2 out of 5 percent: 0.4
5_graph_symbol.csv
overlap with 1 vs. 2.:  0 out of 81 percent: 0.0
overlap with 2 vs. 1.:  62 out of 81 percent: 0.7654320987654321
6_graph_symbol.csv
overlap with 1 vs. 2.:  0 out of 6 percent: 0.0
overlap with 2 vs. 1.:  6 out of 6 percent: 1.0
"""


###################################################################
# GO-term enrichment studies
# All files used here can be found in dockerstuff/static
from goatools.goea.go_enrichment_ns import GOEnrichmentStudyNS
import numpy as np
from goatools.obo_parser import GODag

def get_goeaobj(acc_stuff, taxid, dr, method="fdr_bh"):

    #from goatools.test_data.genes_NCBI_9606_ProteinCoding import GENEID2NT
    from goatools.test_data.genes_NCBI_9606_All import GENEID2NT
    obo_dag = GODag(dr + "go-basic.obo")
    
    goeaobj = GOEnrichmentStudyNS(
        GENEID2NT.keys(), # List of mouse protein-coding genes
        acc_stuff, # geneid/GO associations
        obo_dag, # Ontologies
        propagate_counts = False,
        alpha = 0.05, # default significance cut-off
        methods = ['fdr_bh'])

    return goeaobj


with open("all_goterms.pkl", "rb") as F:
    acc_go = pickle.load(F)
t = get_goeaobj(acc_go, 9606, "")


## When above DEG search has been changed accordingly
#v = t.run_study(set([int(gn[0][hmgns.index(i)]) for i in n12_]))
#sig = [r for r in v if r.p_fdr_bh < 0.05]
#t.wr_tsv("100_genes_1_vs_2_goterms.tsv", sig)

#v = t.run_study(set([int(gn[0][hmgns.index(i)]) for i in n21_]))
#sig = [r for r in v if r.p_fdr_bh < 0.05]
#t.wr_tsv("100_genes_2_vs_1_goterms.tsv", sig)


## When above DEG search has been changed accordingly
#v = t.run_study(set([int(gn[0][hmgns.index(i)]) for i in n12_]))
#sig = [r for r in v if r.p_fdr_bh < 0.05]
#t.wr_tsv("200_genes_1_vs_2_goterms.tsv", sig)

#v = t.run_study(set([int(gn[0][hmgns.index(i)]) for i in n21_]))
#sig = [r for r in v if r.p_fdr_bh < 0.05]
#t.wr_tsv("200_genes_2_vs_1_goterms.tsv", sig)





go10012 = pd.read_csv("100_genes_1_vs_2_goterms.tsv", sep="\t")
go10021 = pd.read_csv("100_genes_2_vs_1_goterms.tsv", sep="\t")
go20012 = pd.read_csv("200_genes_1_vs_2_goterms.tsv", sep="\t")
go20021 = pd.read_csv("200_genes_2_vs_1_goterms.tsv", sep="\t")


for i in range(7):
    print(str(i)+"_goterms.csv")
    tmp = pd.read_csv(str(i)+"_goterms.tsv", sep="\t")
    tmp = set(tmp["name"]) 
    #tmp = set([hmgns[mgns.index(ii)] for ii in mgns])
    print("overlap with go10012.: ", len(set(go10012["name"]).intersection(tmp)), "scl:", len(tmp), "DEG:",len(go10012["name"]))
    print("overlap with go10021.: ", len(set(go10021["name"]).intersection(tmp)), "scl:", len(tmp), "DEG:",len(go10021["name"]))
    print("overlap with go20012.: ", len(set(go20012["name"]).intersection(tmp)), "scl:", len(tmp), "DEG:",len(go20012["name"]))
    print("overlap with go20021.: ", len(set(go20021["name"]).intersection(tmp)), "scl:", len(tmp), "DEG:",len(go20021["name"]))


"""
0_goterms.csv
overlap with go10012.:  23 scl: 29 DEG: 137
overlap with go10021.:  8 scl: 29 DEG: 86
overlap with go20012.:  25 scl: 29 DEG: 253
overlap with go20021.:  8 scl: 29 DEG: 148
1_goterms.csv
overlap with go10012.:  9 scl: 13 DEG: 137
overlap with go10021.:  9 scl: 13 DEG: 86
overlap with go20012.:  9 scl: 13 DEG: 253
overlap with go20021.:  12 scl: 13 DEG: 148
2_goterms.csv
overlap with go10012.:  2 scl: 4 DEG: 137
overlap with go10021.:  3 scl: 4 DEG: 86
overlap with go20012.:  2 scl: 4 DEG: 253
overlap with go20021.:  4 scl: 4 DEG: 148
3_goterms.csv
overlap with go10012.:  16 scl: 30 DEG: 137
overlap with go10021.:  5 scl: 30 DEG: 86
overlap with go20012.:  24 scl: 30 DEG: 253
overlap with go20021.:  7 scl: 30 DEG: 148
4_goterms.csv
overlap with go10012.:  0 scl: 1 DEG: 137
overlap with go10021.:  0 scl: 1 DEG: 86
overlap with go20012.:  0 scl: 1 DEG: 253
overlap with go20021.:  0 scl: 1 DEG: 148
5_goterms.csv
overlap with go10012.:  8 scl: 68 DEG: 137
overlap with go10021.:  29 scl: 68 DEG: 86
overlap with go20012.:  13 scl: 68 DEG: 253
overlap with go20021.:  41 scl: 68 DEG: 148
6_goterms.csv
overlap with go10012.:  1 scl: 4 DEG: 137
overlap with go10021.:  3 scl: 4 DEG: 86
overlap with go20012.:  1 scl: 4 DEG: 253
overlap with go20021.:  3 scl: 4 DEG: 148
"""


# GO-terms in Scellnetor neutrophil clusters, but NOT IN 1 vs. 2 DEG
# And GO-terms in neutrophil clusters AND IN 2 vs. 1 DEG
for i in [0,3]:
    print("\n----------\n",str(i)+"_goterms.csv")
    tmp = pd.read_csv(str(i)+"_goterms.tsv", sep="\t")
    tmp = set(tmp["name"]) 
    #tmp = set([hmgns[mgns.index(ii)] for ii in mgns])
    print("overlap with go10012.: ", len(set(go10012["name"]).intersection(tmp)), "scl:", len(tmp), "DEG:",len(go10012["name"]))
    print(tmp.difference(set(go10012["name"])))
    print("\n\n")
    print("overlap with go10021.: ", len(set(go10021["name"]).intersection(tmp)), "scl:", len(tmp), "DEG:",len(go10021["name"]))
    print(tmp.intersection(set(go10021["name"])))
    print("\n\n")
    print("overlap with go20012.: ", len(set(go20012["name"]).intersection(tmp)), "scl:", len(tmp), "DEG:",len(go20012["name"]))
    print(tmp.difference(set(go20012["name"])))
    print("\n\n")
    print("overlap with go20021.: ", len(set(go20021["name"]).intersection(tmp)), "scl:", len(tmp), "DEG:",len(go20021["name"]))
    print(tmp.intersection(set(go20021["name"])))


"""
0_goterms.csv
overlap with go10012.:  23 scl: 29 DEG: 137
{'chaperone binding', 'procollagen-proline 4-dioxygenase complex', 'protein folding chaperone', 'unfolded protein binding', 'cysteine-type endopeptidase inhibitor activity involved in apoptotic process', 'tubulobulbar complex'}



overlap with go10021.:  8 scl: 29 DEG: 86
{'cytosol', 'extracellular exosome', 'nucleus', 'negative regulation of apoptotic process', 'focal adhesion', 'protein binding', 'perinuclear region of cytoplasm', 'enzyme binding'}



overlap with go20012.:  25 scl: 29 DEG: 253
{'tubulobulbar complex', 'unfolded protein binding', 'procollagen-proline 4-dioxygenase complex', 'protein folding chaperone'}



overlap with go20021.:  8 scl: 29 DEG: 148
{'cytosol', 'extracellular exosome', 'nucleus', 'negative regulation of apoptotic process', 'focal adhesion', 'protein binding', 'perinuclear region of cytoplasm', 'enzyme binding'}

----------
 3_goterms.csv
overlap with go10012.:  16 scl: 30 DEG: 137
{'phagolysosome', 'phosphatidylinositol binding', 'electron transport chain', 'Rho protein signal transduction', 'superoxide-generating NADPH oxidase activity', 'Rac GTPase binding', 'superoxide anion generation', 'electron transfer activity', 'positive regulation of catalytic activity', 'superoxide-generating NADPH oxidase activator activity', 'dendrite', 'superoxide metabolic process', 'cellular defense response', 'innate immune response'}



overlap with go10021.:  5 scl: 30 DEG: 86
{'cell redox homeostasis', 'cytosol', 'protein binding', 'plasma membrane', 'membrane'}



overlap with go20012.:  24 scl: 30 DEG: 253
{'phosphatidylinositol binding', 'Rac GTPase binding', 'superoxide-generating NADPH oxidase activity', 'superoxide anion generation', 'superoxide-generating NADPH oxidase activator activity', 'dendrite'}



overlap with go20021.:  7 scl: 30 DEG: 148
{'cell redox homeostasis', 'cytosol', 'protein binding', 'antigen processing and presentation of exogenous peptide antigen via MHC class I, TAP-dependent', 'plasma membrane', 'cellular response to oxidative stress', 'membrane'}
"""




# GO-terms in Scellnetor erythrocyte clusters AND IN 1 vs. 2 DEG
# And GO-terms in erythrocyte clusters but NOT IN 2 vs. 1 DEG
for i in [1,2,4,5,6]:
    print("\n----------\n",str(i)+"_goterms.csv")
    tmp = pd.read_csv(str(i)+"_goterms.tsv", sep="\t")
    tmp = set(tmp["name"]) 
    #tmp = set([hmgns[mgns.index(ii)] for ii in mgns])
    print("overlap with go10012.: ", len(set(go10012["name"]).intersection(tmp)), "scl:", len(tmp), "DEG:",len(go10012["name"]))
    print(tmp.intersection(set(go10012["name"])))
    print("\n\n")
    print("overlap with go10021.: ", len(set(go10021["name"]).intersection(tmp)), "scl:", len(tmp), "DEG:",len(go10021["name"]))
    print(tmp.difference(set(go10021["name"])))
    print("\n\n")
    print("overlap with go20012.: ", len(set(go20012["name"]).intersection(tmp)), "scl:", len(tmp), "DEG:",len(go20012["name"]))
    print(tmp.intersection(set(go20012["name"])))
    print("\n\n")
    print("overlap with go20021.: ", len(set(go20021["name"]).intersection(tmp)), "scl:", len(tmp), "DEG:",len(go20021["name"]))
    print(tmp.difference(set(go20021["name"])))


"""
1_goterms.csv
overlap with go10012.:  9 scl: 13 DEG: 137
{'cell redox homeostasis', 'extracellular exosome', 'cytosol', 'nucleus', 'secretory granule lumen', 'ficolin-1-rich granule lumen', 'neutrophil degranulation', 'cytoplasm', 'hydrogen peroxide catabolic process'}



overlap with go10021.:  9 scl: 13 DEG: 86
{'positive regulation of mRNA splicing, via spliceosome', 'thioredoxin peroxidase activity', 'hydrogen peroxide catabolic process', 'proteasome complex'}



overlap with go20012.:  9 scl: 13 DEG: 253
{'cell redox homeostasis', 'extracellular exosome', 'cytosol', 'nucleus', 'secretory granule lumen', 'ficolin-1-rich granule lumen', 'neutrophil degranulation', 'cytoplasm', 'hydrogen peroxide catabolic process'}



overlap with go20021.:  12 scl: 13 DEG: 148
{'positive regulation of mRNA splicing, via spliceosome'}

----------
 2_goterms.csv
overlap with go10012.:  2 scl: 4 DEG: 137
{'extracellular exosome', 'membrane'}



overlap with go10021.:  3 scl: 4 DEG: 86
{'cadherin binding'}



overlap with go20012.:  2 scl: 4 DEG: 253
{'extracellular exosome', 'membrane'}



overlap with go20021.:  4 scl: 4 DEG: 148
set()

----------
 4_goterms.csv
overlap with go10012.:  0 scl: 1 DEG: 137
set()



overlap with go10021.:  0 scl: 1 DEG: 86
{'cristae formation'}



overlap with go20012.:  0 scl: 1 DEG: 253
set()



overlap with go20021.:  0 scl: 1 DEG: 148
{'cristae formation'}

----------
 5_goterms.csv
overlap with go10012.:  8 scl: 68 DEG: 137
{'cytosol', 'extracellular exosome', 'nucleus', 'nucleoplasm', 'RNA binding', 'protein binding', 'cytoplasm', 'membrane'}



overlap with go10021.:  29 scl: 68 DEG: 86
{'mitochondrial respiratory chain complex I', 'regulation of mRNA stability', 'RNA splicing', 'mitochondrial respiratory chain complex IV', 'NADH dehydrogenase (ubiquinone) activity', 'nuclear envelope lumen', 'T cell receptor signaling pathway', 'transcription factor TFTC complex', 'proteasome-activating ATPase activity', 'mRNA splicing, via spliceosome', 'cytoplasmic ribonucleoprotein granule', 'proteasome regulatory particle, base subcomplex', 'transcription factor TFIID complex', 'mitochondrial large ribosomal subunit', "mRNA 3'-end processing", 'proteasome complex', 'citrulline biosynthetic process', 'NAD binding', 'regulation of cellular amino acid metabolic process', 'Wnt signaling pathway, planar cell polarity pathway', 'nuclear origin of replication recognition complex', 'mitochondrial electron transport, NADH to ubiquinone', 'mitochondrial ribosome', 'mitochondrial translational termination', 'mitochondrial translational elongation', 'proteasome accessory complex', 'AMP binding', 'mitochondrial respiratory chain complex I assembly', 'structural constituent of ribosome', 'Fc-epsilon receptor signaling pathway', 'SAGA complex', 'mRNA processing', 'STAGA complex', 'tRNA aminoacylation for protein translation', 'negative regulation of G2/M transition of mitotic cell cycle', 'origin recognition complex', 'MICOS complex', 'mitochondrial electron transport, ubiquinol to cytochrome c', 'TBP-class protein binding'}



overlap with go20012.:  13 scl: 68 DEG: 253
{'negative regulation of transcription by RNA polymerase II', 'cytosol', 'extracellular exosome', 'nucleus', 'ATP binding', 'nucleoplasm', 'RNA binding', 'protein binding', 'mitochondrion', 'cytoplasm', 'identical protein binding', 'mitochondrial matrix', 'membrane'}



overlap with go20021.:  41 scl: 68 DEG: 148
{'response to amine', 'UTP biosynthetic process', 'regulation of mRNA stability', 'mitochondrial respiratory chain complex IV', 'nuclear envelope lumen', 'transcription factor TFTC complex', 'proteasome-activating ATPase activity', 'p53 binding', 'transcription factor TFIID complex', 'mitochondrial large ribosomal subunit', "mRNA 3'-end processing", 'citrulline biosynthetic process', 'nuclear origin of replication recognition complex', 'mitochondrial electron transport, NADH to ubiquinone', 'mitochondrial ribosome', 'mitochondrial electron transport, ubiquinol to cytochrome c', 'mitochondrial translational termination', 'mitochondrial translational elongation', 'AMP binding', 'mitochondrial respiratory chain complex I assembly', 'Fc-epsilon receptor signaling pathway', 'SAGA complex', 'mRNA processing', 'STAGA complex', 'origin recognition complex', 'positive regulation of mitochondrial outer membrane permeabilization involved in apoptotic signaling pathway', 'TBP-class protein binding'}

----------
 6_goterms.csv
overlap with go10012.:  1 scl: 4 DEG: 137
{'protein binding'}



overlap with go10021.:  3 scl: 4 DEG: 86
{'cell leading edge'}



overlap with go20012.:  1 scl: 4 DEG: 253
{'protein binding'}



overlap with go20021.:  3 scl: 4 DEG: 148
{'cell leading edge'}
"""



