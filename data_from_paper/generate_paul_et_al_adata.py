#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 23 12:31:42 2019

@author: alexander
"""
import pandas as pd
import numpy as np
import scanpy as sc
import matplotlib.pyplot as pl

## read the expression data
df1 = pd.read_csv("umitab.txt", delimiter="\t")

M = df1.iloc[:].values
M_nm = list(df1.index)

## read the experimental design file
df_info = pd.read_csv("GSE72857_experimental_design_use.txt",delimiter="\t")




## Batch IDs used for their clustering. See table S2 in Paul et al. (2015).
myl_unsrt= [
"AB173",
"AB174",
"AB175",
"AB176",
"AB203",
"AB204",
"AB205",
"AB206",
"AB207",
"AB208",
"AB209",
"AB210",
"AB211",
"AB212",
"AB213",
"AB214"]


## retrieving expression data baed on batch and Well IDs.
dct = {}
for i in myl_unsrt:
    dct[i] = list(df_info["Well_ID"][df_info["Amp_batch_ID"] == i])

al = []
for k,v in dct.items():
    al += v

ws = list(df1.columns)

## Selecting cells that has more than 501 gene expression counts.
## you will get 2730 cell which is the same as in the paper.
cells = df1.loc[:,al]
c = cells.iloc[:].values
maybe_correct = c[:,np.sum(c,axis=0) > 501]



## reading average expression values of Paul et al. clusters (Table S3 paul et al. (2016))
df_av = pd.read_excel("mmc2.xlsx")
names = list(df_av["Unnamed: 0"])


## Filter away genes they don't use in their result table, Table S3 (paul et al. (2016))
new_pos1 = []
for i in names:
    tmp = []
    for jj,ii in enumerate(M_nm):
        if i in ii.split(";"):
            tmp.append([jj, i, ii])
            new_pos1.append(jj)
    if len(tmp) > 1:
        print(tmp)
        break


## Dummy-checking that the summed expression values for every gene are similar
m2 = maybe_correct[new_pos1]
avs = []
for i in m2:
    avs.append(np.sum(i))
    
bbbb = list(df_av["total_umis"])
t = 0
real_ind = {}
for j,i in enumerate(df_av["Unnamed: 0"]):
    print(i, bbbb[j], avs[t])
    if bbbb[j] - avs[t] != 0:
        print(names[j], type(names[j]))
        pass
    else:
        real_ind[i] = t
        t+= 1


## retrieving positions of the different cluster items
adata2 = sc.datasets.paul15()
pls = set(list(adata2.obs["paul15_clusters"]))

dct_pls = {}
for i in pls:
    tmp = list(adata2.obs["paul15_clusters"][adata2.obs["paul15_clusters"] == i].index)
    tmp = [int(II) for II in tmp]
    dct_pls[i] = tmp


f = ['1Ery', 
 '2Ery', 
 '3Ery', 
 '4Ery',
 '5Ery',
 '6Ery',
 '7MEP',
 '8Mk',
  '9GMP', 
 '10GMP', 
 '11DC', 
 '12Baso', 
 '13Baso', 
 '14Mo',
 '15Mo', 
 '16Neu',
 '17Neu', 
 '18Eos', 
 '19Lymph']

#corr_dct = {"13Baso": "17Neu",
#"14M0" : "13Baso",
#"15Mo" : "14Mo",
#"16Neu" : "15Mo",
#"17Neu" : "16Neu"}


## Changing the cluster names such that the data from Table S2 (paul et al. 
## (2016)) can be reproduced

dct_pls2 = dct_pls.copy()

dct_pls2["13Baso"] = dct_pls["17Neu"]
dct_pls2["14Mo"] = dct_pls["13Baso"]
dct_pls2["15Mo"] = dct_pls["14Mo"]
dct_pls2["16Neu"] = dct_pls["15Mo"]
dct_pls2["17Neu"] = dct_pls["16Neu"]


#for k in f:
#    tmp = m2[:,dct_pls2[k]]
#    print(k, np.sum(tmp[real_ind["Gata2"]])/len(dct_pls2[k])   )


############
## Checking if I can recrate the values from Table S3 (Paul et al. (2016))
## Some genes in Table S3 are not annotated as gene symbols or anything 
## that relates to genes.
new_exp = []
t = 0
for j,i in enumerate(df_av["Unnamed: 0"]):
    tmp_lst = []
    if bbbb[j] - np.sum(m2[t]) != 0:
        pass
    else:
        for k in f:
            tmp = m2[:,dct_pls2[k]]
            tmp_lst.append(np.sum(tmp[t])/len(dct_pls2[k]))
        tmp_lst.append(np.sum(m2[t]))
        t+= 1
        new_exp.append(tmp_lst)

new_exp1 = np.array(new_exp)

vals = df_av.iloc[:].values
vals = vals[:,1:-2]

t = 0
for i in range(len(vals)):
    print(t, vals[i][-1],  new_exp1[t][-1])
    if vals[i][-1] != new_exp1[t][-1]:
        continue
    else:
        t += 1

"""
t = 0
diff = []
for i in range(len(vals)):
    #print(t, np.sum(vals[i] - new_exp1[t]))
   
    if vals[i][-1] != new_exp1[t][-1]:
        continue
    else:
        diff.append(np.sum(vals[i] - new_exp1[t]))
        t += 1
"""
## gives zero in diff. have succesfully recreated their data.
## Some genes in Table S3 are not annotated as gene symbols or anything 
## that relates to genes.
t = 0
diff = []
bbbb = list(df_av["Unnamed: 0"])
new_names = []
for i in range(len(vals)):
    print(t, np.sum(np.array(vals[i],dtype="float16") - np.array(new_exp1[t],dtype="float16")))

    if vals[i][-1] != new_exp1[t][-1]:
        continue
    else:
        diff.append(np.sum(np.array(vals[i],dtype="float16") - np.array(new_exp1[t],dtype="float16")))
        t += 1
        new_names.append(bbbb[i])

diff.sort()
diff.reverse()
############

#####################################
# GENERATING ANNDATA OBJECT
#####################################
from scipy.spatial.distance import cdist

to_adata = m2.T
new_names ## names of genes


df1 = pd.DataFrame(to_adata, index=list(range(len(to_adata))), columns=new_names)
df1.to_csv("newhem_h5ad_.csv",sep=",", index=False)
adata_x = sc.read_csv("newhem_h5ad_.csv")


pl_cls_new = []
for i in range(len(to_adata)):
    for k in f:
        if i in dct_pls2[k]:
            pl_cls_new.append(k)

"""
#make a check...
ff = new_names.index("Gata2")
tmp = np.array(pl_cls_new)
for k in f:
    print(k, np.sum([tmp == k]), np.sum(adata_x.X[[tmp == k]][:,ff]/np.sum([tmp == k])))
"""

adata_x.obs["paul15_clusters"] = pl_cls_new
n_counts = list(np.sum(adata_x.X,axis=-1))
adata_x.obs["n_counts_all"] = n_counts
adata_x.write_h5ad("newhem_h5ad.h5ad")


#############################################################################
## OBS!!!!! If you don't get exactly the same cell map as in out paper,
## then run the segment encapsulated by the '###' again. 
adata_x = sc.read_h5ad("newhem_h5ad.h5ad")


#sc.pp.recipe_zheng17(adata_x)
sc.pp.filter_genes(adata_x, min_counts=1)  # only consider genes with more than 1 count
sc.pp.normalize_per_cell(                # normalize with total UMI count per cell
     adata_x, key_n_counts='n_counts_all')
filter_result = sc.pp.filter_genes_dispersion(  # select highly-variable genes
    adata_x.X, flavor='cell_ranger', n_top_genes=int(len(adata_x.X[0])/2), log=False)
adata_x = adata_x[:, filter_result.gene_subset]     # subset the genes
sc.pp.normalize_per_cell(adata_x)          # renormalize after filtering
sc.pp.log1p(adata_x)               # log transform: adata.X = log(adata.X + 1)
sc.pp.scale(adata_x)  



sc.tl.pca(adata_x, svd_solver='arpack')
sc.pp.neighbors(adata_x, n_neighbors=4, n_pcs=20)
sc.tl.diffmap(adata_x)
sc.pp.neighbors(adata_x, n_neighbors=10, use_rep='X_diffmap')
sc.tl.draw_graph(adata_x)
sc.pl.draw_graph(adata_x, color='paul15_clusters', legend_loc='on data')

sc.tl.louvain(adata_x, resolution=1.0)
sc.tl.paga(adata_x, groups='paul15_clusters')
sc.pl.paga(adata_x, color="paul15_clusters")

sc.tl.draw_graph(adata_x, init_pos='paga')
sc.pl.draw_graph(adata_x, color="paul15_clusters", legend_loc='on data')
#############################################################################

def get_timeline_coordinates(adata, wi, he, type1="draw_graph_fa"):
    """
    'type can either be diffmap or draw_graph_fa'
    """
    from scanpy.plotting._tools.scatterplots import _get_data_points
    V = _get_data_points(adata, basis=type1, projection="2d", components='1,2')#img_key=None, library_id=None) # for sc 1.6.0
    return V[0][0]

cc = get_timeline_coordinates(adata_x, 0, 0)

tmp = 0
av = 0
for i in ["7MEP", "8Mk" ,"9GMP","10GMP"]:
    av += len(cc[adata_x.obs["paul15_clusters"] == i])
    tmp += np.sum(cc[adata_x.obs["paul15_clusters"] == i], axis=0)

tmp = tmp / av

start = np.argmin(cdist(cc, [tmp]))

x = 2
pl.scatter(cc[:,0], cc[:,1])
#pl.scatter(cc[adata_x.obs["paul15_clusters"] =="7MEP"][x][0], cc[adata_x.obs["paul15_clusters"] =="7MEP"][x][1])
pl.scatter(tmp[0], tmp[1])
pl.scatter(cc[start][0], cc[start][1])
print(start)




## roghly in between MEP and GMP
adata_x.uns['iroot'] = start
sc.tl.dpt(adata_x, n_dcs=15)

adata_x.write_h5ad("newhem_h5ad_processed_1.h5ad")
adata_x = sc.read_h5ad("newhem_h5ad_processed_1.h5ad")





