#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 10:45:31 2020

@author: alexander
"""

import networkx as nx
import pandas as pd
import numpy as np


all_ppis1 = pd.read_csv("BIOGRID-ALL-3.5.187.mitab.txt", sep="\t")

A = set(list(all_ppis1["Taxid Interactor A"][all_ppis1["Taxid Interactor A"] == "taxid:9606"].index))

B = set(list(all_ppis1["Taxid Interactor B"][all_ppis1["Taxid Interactor B"] == "taxid:9606"].index))

AB = list(A.intersection(B))

IDA = list(all_ppis1["#ID Interactor A"][AB])
IDB = list(all_ppis1["ID Interactor B"][AB])


with open("BIOGRID_PPI_3.5.187_tab_all_9606_interactions.tab2", "w") as f:
    for i in range(len(AB)):
        f.write(IDA[i].split(":")[-1]+"\t"+IDB[i].split(":")[-1]+"\n")



int_t = {'psi-mi:"MI:0403"(colocalization)',
 'psi-mi:"MI:0407"(direct interaction)',
 'psi-mi:"MI:0914"(association)',
 'psi-mi:"MI:0915"(physical association)'}

types = list(all_ppis1["Interaction Types"])
use_types = []
for i in AB:
    if types[i] in int_t:
        use_types.append(i)

AB = use_types


for i in range(len(AB)):
    IDA[i] = IDA[i].split(":")[-1]
    IDB[i] = IDB[i].split(":")[-1]    



with open("BIOGRID_PPI_3.5.187_tab_physical_9606_interactions.tab2", "w") as f:
    for i in range(len(AB)):
        f.write(IDA[i]+"\t"+IDB[i]+"\n")



