import numpy as np
import pandas as pd



def boil_down_data(g1, g2, reps1, reps2, mid):

    """

    :param g1: time series data from group 1
    :param g2: time series data from group 2
    :param reps1: number of samples for group 1
    :param reps2: number of samples for group 2
    :param tmstps: number of timesteps
    :param mid: average or median when calculating the timeline that should represent the group samples
    :return: timelines that represent the group samples, list of entrex id still in data and
             R^2 score for every time line


    """


    def coef_of_determination(y, yhat):

        """
        y is the matrix with observed values
        yhat is vector with modelled values (or the average)

        R2 is a statistic that will give some information about the goodness of
        fit of a model. In regression, the R2 coefficient of determination is a
        statistical measure of how well the regression predictions approximate
        the real data points. An R2 of 1 indicates that the regression predictions
        perfectly fit the data.

        In all instances where R2 is used, the predictors are calculated by
        ordinary least-squares regression: that is, by minimizing SSres (residuals).

        Suppose R2 = 0.49. This implies that 49% of the variability of the
        dependent variable has been accounted for, and the remaining 51% of
        the variability is still unaccounted for.
        """
        y = np.array(y)
        yhat = np.array(yhat)
        mean_of_obs = np.mean(y)  # , axis=0)
        tot_sum_of_sqrs = np.sum((y - mean_of_obs) ** 2)
        # reg_sum_of_sqrs = np.sum((yhat-mean_of_obs)**2)
        residuals = 0
        for i in y:
            residuals += np.sum((i - yhat) ** 2)
        R = 1 - (residuals / tot_sum_of_sqrs)
        return R



    def sorting_order_of_genes(list_entrz):


        for i in list_entrz[0]:
            if list_entrz[0].count(i) > 1:
                print(i, list_entrz[0].count(i))
        true = list(set(list_entrz[0])) ## remove potential replications...
        true.sort()

        ##removing genes from true that are not in all of the other lists
        no = 0
        temp = []
        temp1 = [set(i) for i in list_entrz[1:]]

        for i in true:
            for ii in temp1:
                if i not in ii:
                    no += 1
                    temp.append(i)
                    break

        for i in temp:
            true.remove(i)

        ## Sorting genes and removing genes from the other replicates
        ## that are not in the "true" list
        no1 = -1
        rearrange_list = []
        for j, i in enumerate(list_entrz):
            if true != i:
                no1 += 1
                temp = []
                for ii in true:
                    temp.append(i.index(ii))
                rearrange_list.append(temp)
                print("\nThe order of the genes in", no1, "replicate(s) was changes")
                print("Removed", len(i) - len(true), "genes in total \n")
            else:
                rearrange_list.append([])

        return rearrange_list, true


    def merge_samples(g1, reps1, mid):
        G1 = pd.read_table(g1[0], index_col=0)
        G1["Entrez"] = [str(i) for i in list(G1["Entrez"])]
        G1 = G1.sort_values(by=['Entrez'])
        #print(G1['Entrez'])
        #G1 = pd.read_table(g1[0]) ## This is for the control data
        #G1 = pd.read_table(g1[0], index_col=0, sep=",")

        lng1 = int(len(G1)/reps1)
        #lng1 = int(len(G1)/reps1)
        lst1 = []
        #entrez1 = G1['EntrezID'][:lng1]  # g1.index
        entrz = []

        for i in range(reps1):
            temp = G1[i * lng1: (i + 1) * lng1]

            #lst1.append(temp.iloc[:, -tmstps:].values)
            lst1.append(temp.iloc[:, 2:].values)
            #print(G1.columns, lng1, len(temp), len(lst1[0][0]))
            ##entrz.append([str(gn) for gn in G1['EntrezID'][i * lng1: (i + 1) * lng1]])
            #entrz.append([str(gn) for gn in G1.iloc[:,1][i * lng1: (i + 1) * lng1]])
            entrz = [str(gn) for gn in G1.iloc[:,1][i * lng1: (i + 1) * lng1]]
            symbs = [str(gn) for gn in G1.iloc[:,0][i * lng1: (i + 1) * lng1]]

        
        tmstps = len(lst1[0][0])
        lst1 = np.array(lst1, dtype='float32').reshape((reps1, lng1, tmstps))
        #lst1 = np.array(lst1, dtype='float32')
        """
        print("sorting the order of genes and removing genes that are not present in all replicates")
        # entrez is a list with lists of all the different entrez ids in the samples
        sorted_lst, entrez1 = sorting_order_of_genes(entrz)

        for srt in range(len(sorted_lst)):
            if len(sorted_lst[srt]) > 0:
                lst1[srt] = lst1[srt][sorted_lst[srt]]

        ###################
        #entrz[0] = entrez1
        for i in range(len(sorted_lst)):
            if len(sorted_lst[i]) > 0:
                entrz[i] = [entrz[i][x] for x in sorted_lst[i]]

            assert entrz[0] == entrz[i]

        ###################
        """

        if mid == 'average':
            GG1 = np.average(lst1,axis=0)

        if mid == 'median':
            GG1 = np.median(lst1, axis=0)


        ### Calcualte the coefficient of determinant
        if reps1 == 1:
            r1 = np.ones((lng1,))
        if reps1 > 1:
            r1 = []
            for i in range(lng1):
                r1.append( coef_of_determination(lst1[:,i], GG1[i]) )

        return GG1, entrz, r1, symbs  #entrez1



    if len(g1) == 1:
        GG1, entrez1, r1, symbs1 = merge_samples(g1, reps1, mid)
        print("shape of group 1 times series matrix:", GG1.shape)
        print("length of group 1 gene list:", len(entrez1), len(r1))
    #if len(g1) > 1:
    #    ####
    #    # Make it such that it can take replicates with different order of genes
    #    ####
    #    reps1 = len(g1)
    #    G1 = []
    #    for i in g1:
    #        G1.append(pd.read_table(i, index_col=0))


    if len(g2) > 0:
        if len(g2) == 1:
            GG2, entrez2, r2, symbs2 = merge_samples(g2, reps2, mid)
            print("shape of group 2 times series matrix:", GG2.shape)
            print("length of group 2 gene list:", len(entrez2), len(r2))

        #if len(g2) > 1:
        #    reps2 = len(g2)
        #    G2 = []
        #    for i in g2:
        #        G2.append(pd.read_table(i, index_col=0))


        return GG1, GG2, entrez1, entrez2, r1, r2, symbs1, symbs2

    return GG1, entrez1, r1, symbs1
