

import numpy as np
import time





def complete(mtrx, adj, ss, max_val, arg_val, v, c, no_val, fr,lx, not_in):  ### Skal have en omvendt trac

    mtrx[v][c] = mtrx[c][v] = 0
    ss[v][c] = ss[c][v] = 0

    mtrx[v] = mtrx[:, v] = np.min((mtrx[v], mtrx[c]), 0)
    adj[v] = adj[:, v] = np.max((adj[v], adj[c]), 0)
    ss[v] = ss[:, v] = mtrx[v] * adj[v]

    mtrx[c] = mtrx[:, c] = 0
    adj[c] = adj[:, c] = 0
    ss[c] = ss[:, c] = 0

    for i,j in enumerate(arg_val):
        if i not in not_in:
            if j == v or j == c:
                #for ii in range(0,lx,fr):
                #tmp = [np.argmax(ss[i][ii:ii+fr])+ii for ii in range(0,lx,fr)]
                #max_val[i] = np.max(ss[i][tmp])
                #arg_val[i] = np.argmax(ss[i][tmp])

                arg_val[i] = np.argmax(ss[i][i:]) + i
                max_val[i] = ss[i][arg_val[i]]

    #tmp = np.where(arg_val == v)[0]
    #tmp1 = np.where(arg_val == c)[0]

    #arg_val[tmp] = np.argmax(ss[tmp], -1)
    #max_val[tmp] = ss[tmp, arg_val[tmp]]

    #arg_val[tmp1] = np.argmax(ss[tmp1], -1)
    #max_val[tmp1] = ss[tmp1, arg_val[tmp1]]

    arg_val[c] = c
    max_val[c] = 0
    not_in.add(c)



def single(mtrx, adj, ss, max_val, arg_val, v, c, no_val,fr, lx, not_in):  ### Skal have en omvendt trac

    mtrx[v][c] = mtrx[c][v] = 0
    ss[v][c] = ss[c][v] = 0

    mtrx[v] = mtrx[:, v] = np.max((mtrx[v], mtrx[c]), 0)
    adj[v] = adj[:, v] = np.max((adj[v], adj[c]), 0)
    ss[v] = ss[:, v] = mtrx[v] * adj[v]

    mtrx[c] = mtrx[:, c] = 0
    adj[c] = adj[:, c] = 0
    ss[c] = ss[:, c] = 0


    for i,j in enumerate(arg_val):
        if i not in not_in:
            if j == v or j == c:
                #for ii in range(0,lx,fr):
                #tmp = [np.argmax(ss[i][ii:ii+fr])+ii for ii in range(0,lx,fr)]
                #max_val[i] = np.max(ss[i][tmp])
                #arg_val[i] = np.argmax(ss[i][tmp])

                arg_val[i] = np.argmax(ss[i][i:]) + i
                max_val[i] = ss[i][arg_val[i]]

    #tmp = np.where(arg_val == v)[0]
    #tmp1 = np.where(arg_val == c)[0]

    #arg_val[tmp] = np.argmax(ss[tmp], -1)
    #max_val[tmp] = ss[tmp, arg_val[tmp]]

    #arg_val[tmp1] = np.argmax(ss[tmp1], -1)
    #max_val[tmp1] = ss[tmp1, arg_val[tmp1]]

    arg_val[c] = c
    max_val[c] = 0
    not_in.add(c)



def average(mtrx, adj, ss, max_val, arg_val, v, c, no_val,fr,lx, not_in):  ### Skal have en omvendt trac

    """
    :param mtrx: background value matrix, contains the sums of the cluster merges
    :param adj: adjecency matrix
    :param ss: working value matrix, contins the values to be clustered
    :param max_val: vector with max valuse for each row of ss
    :param arg_val: vector with argmax values for each row od ss
    :param v: row number of merged cluster
    :param c: column number of merged cluster
    :param no_val: number of items in each cluster - reference is v, c gets deleted
    :return: updates the matrices and vectors
    """

    x = no_val[v] + no_val[c]
    mtrx[v][c] = mtrx[c][v] = 0
    adj[v][c] = adj[c][v] = 0

    mtrx[v] = mtrx[:, v] = ((mtrx[v] * (no_val[v] / x)) + (mtrx[c] * (no_val[c] / x)))
    mtrx[c] = mtrx[:, c] = 0

    adj[v] = adj[:, v] = np.max((adj[v], adj[c]), 0)
    adj[c] = adj[:, c] = 0

    ss[v] = ss[:, v] = mtrx[v] * adj[v]
    ss[c] = ss[:, c] = 0

    ########
    # Jeg ved nu hvorfor dendrogrammerne ligner lort --> man kan ikke mindske distancen i forhold til
    # en allerede indelt cluster.
    # Jeg bliver nødt til at clustre og så lave dentrogrammet igen forfra.

    no_val[v] += no_val[c]
    no_val[c] = 0

    #### changing the values if any of the other items had either v or c as the highest values.
    for i,j in enumerate(arg_val):
        if i not in not_in:
            if j == v or j == c:
                #for ii in range(0,lx,fr):
                #tmp = [np.argmax(ss[i][ii:ii+fr])+ii for ii in range(0,lx,fr)]
                #max_val[i] = np.max(ss[i][tmp])
                #arg_val[i] = np.argmax(ss[i][tmp])

                arg_val[i] = np.argmax(ss[i][i:]) + i
                max_val[i] = ss[i][arg_val[i]]

    #tmp = np.where(arg_val == v)[0]
    #tmp1 = np.where(arg_val == c)[0]

    #arg_val[tmp] = np.argmax(ss[tmp], -1)
    #max_val[tmp] = ss[tmp, arg_val[tmp]]

    #arg_val[tmp1] = np.argmax(ss[tmp1], -1)
    #max_val[tmp1] = ss[tmp1, arg_val[tmp1]]

    arg_val[c] = c
    max_val[c] = 0
    not_in.add(c)





def weird_maker(x10, lx):

    use_clu = {}
    clst = []  ## Output matricen the sklearn plotting functionen
    clu = {}  ## lister med lister af de numre der bliver clusteret sammen

    x10.append([0,0,0])

    for it in range(lx-1):
        #time1 = time.time()
        #print(it)

        o,k = x10[it][1],x10[it][2]#np.unravel_index(ss.argmax(), ss.shape)
        X = x10[it][0]
        #ss[o][k] = ss[k][o] = 0

        #print('QQQQQ', X, o,k)
        i, j = o, k


        if X == 0:
            print('XXXXXXXXXXXXXXXXX')

            gg = set()
            clx1 = []
            for key, val in clu.items():
                cop = val.copy()
                use_clu[key] = cop
                gg.update(val.copy())
                clx1.append([int(key), val.copy()])
            ## finding the item numbers not in any clusters
            clx1.sort()
            deler = set(range(lx))
            deler = gg ^ deler
            ln_deler = len(deler)

            gg = list(gg)
            gg.sort()

            del_again = {}
            for gu in range(len(gg)):
                del_again[gg[gu]] = gg[gu] - gu

            ## extracting the true indx of genes
            trgn = {}
            for clcl in clst:
                if clcl[0] < lx:
                    _ = clcl[0]
                    clcl[0] -= del_again[clcl[0]]
                    if _ not in trgn:
                        trgn[_] = clcl[0]
                if clcl[1] < lx:
                    _ = clcl[1]
                    clcl[1] -= del_again[clcl[1]]
                    if _ not in trgn:
                        trgn[_] = clcl[1]
                if clcl[0] >= lx:
                    clcl[0] -= ln_deler
                if clcl[1] >= lx:
                    clcl[1] -= ln_deler
            lx -= ln_deler

            for ep in range(1, len(clx1)):
                clst.append(
                    [clx1[0][0] + lx, clx1[ep][0] + lx, 1, len(clx1[0][1]) + len(clx1[ep][1])])
                clx1[0][0] = it + ep - 1
                clx1[0][1].update(clx1[ep][1])
            break

        tri_lst = None
        for ke, va in clu.items():
            if i in va:
                tri_lst = ke
        trj_lst = None
        for ke, va in clu.items():
            if j in va:
                trj_lst = ke


        if tri_lst == None and trj_lst == None:
            # print('Making a new cluster')
            clu[str(it)] = {i, j}  # used it before, but use len(clu) now... perhaps change
            clst.append([i, j, 1 - X, 2])

        elif tri_lst != None and trj_lst == None:
            # print('adding to cluster, --i--')
            clu[tri_lst].add(j)
            clst.append([j, int(tri_lst) + lx, 1 - X, len(clu[tri_lst])])
            clu[str(it)] = clu.pop(tri_lst)

        elif tri_lst == None and trj_lst != None:
            # print('adding to cluster, --j--')
            clu[trj_lst].add(i)
            clst.append([i, int(trj_lst) + lx, 1 - X, len(clu[trj_lst])])
            clu[str(it)] = clu.pop(trj_lst)

        elif tri_lst != None and trj_lst != None:
            # print('Merging big clusters')
            tmp = [int(tri_lst), int(trj_lst)]
            tmp.sort()
            clst.append(
                [tmp[0] + lx, tmp[1] + lx, 1 - X, len(clu[tri_lst]) + len(clu[trj_lst])])
            clu[tri_lst].update(clu[trj_lst])
            clu.pop(trj_lst)
            clu[str(it)] = clu.pop(tri_lst)

        #time2 = time.time()
        #print('function took s', time2 - time1)

    if len(use_clu) == 0:
        use_clu = clu
        trgn = []
        print('MIIIIIIILK')

    return np.array(clst), use_clu, trgn









def cluster(x10, adj1, glst, l, K, link="complete"):


    if link == "complete":
        deleterx = complete

    if link == "single":
        deleterx = single

    if link == "average":
        deleterx = average

    weird = []

    s = x10.copy()
    adj = adj1.copy()

    s[np.diag_indices_from(s)] = 0
    adj[np.diag_indices_from(adj)] = 0

    use_clu = {}
    lx = len(s)
    clst = []  ## Output matricen the sklearn plotting functionen
    clu = {}  ## lister med lister af de numre der bliver clusteret sammen

    ss = np.multiply(s, adj)
    max_val = np.max(ss,axis=-1)

    arg_val = np.argmax(ss,axis=-1)
    no_val = np.ones(lx)
    fr = int(lx/30)
    not_in = set()
    #############

    for it in range(lx - 1):
        time1 = time.time()
        print(it)

        if len(glst) > 0:
            o = glst[0][0]
            k = glst[0][1]
            X = 1
            del glst[0]
        if len(glst) == 0:
            o = np.argmax(max_val)
            k = arg_val[o]#np.argmax(ss[o])
            X = max_val[o]


        i, j = o, k
        print('QQQQQ', ss[o][k], X, o, k, i, j)


        if sum([len(size) >= l for size in clu.values()]) == K:
            X = 0

        ################
        if X == 0:
            print("Reached a zero node - clustering will stop")

            ## copying the already formed clusters
            gg = set()
            clx1 = []
            for key, val in clu.items():
                cop = val.copy()
                use_clu[key] = cop
                gg.update(cop)
                clx1.append([int(key), cop])

            ## finding the item numbers not in any clusters
            clx1.sort()
            deler = set(range(lx))
            deler = gg ^ deler ### the genes not in a cluster
            ln_deler = len(deler)

            gg = list(gg)
            gg.sort()


            ## ordering the total number of clusters such that scipy.cluster.hierarchy.dendrogram can read it.
            del_again = {}
            for gu in range(len(gg)):
                del_again[gg[gu]] = gg[gu] - gu

            trgn = {}

            for clcl in clst:
                if clcl[0] < lx:
                    _ = clcl[0]
                    clcl[0] -= del_again[clcl[0]]
                    if _ not in trgn:
                        trgn[_] = clcl[0]
                if clcl[1] < lx:
                    _ = clcl[1]
                    clcl[1] -= del_again[clcl[1]]
                    if _ not in trgn:
                        trgn[_] = clcl[1]

                if clcl[0] >= lx:
                    clcl[0] -= ln_deler
                if clcl[1] >= lx:
                    clcl[1] -= ln_deler
            lx -= ln_deler


            for ep in range(1, len(clx1)):
                clst.append(
                    [clx1[0][0] + lx, clx1[ep][0] + lx, 1, len(clx1[0][1]) + len(clx1[ep][1])])
                clx1[0][0] = it + ep - 1
                clx1[0][1].update(clx1[ep][1])
            break
        ###############

        tri_lst = None
        for ke, va in clu.items():
            if i in va:
                tri_lst = ke
        trj_lst = None
        for ke, va in clu.items():
            if j in va:
                trj_lst = ke

        weird.append([X, i, j])
        if tri_lst == None and trj_lst == None:
            # print('Making a new cluster')
            clu[str(it)] = {i, j}  # used it before, but use len(clu) now... perhaps change
            clst.append([i, j, 1 - X, 2])

        elif tri_lst != None and trj_lst == None:
            # print('adding to cluster, --i--')
            clu[tri_lst].add(j)
            clst.append([j, int(tri_lst) + lx, 1 - X, len(clu[tri_lst])])
            clu[str(it)] = clu.pop(tri_lst)

        elif tri_lst == None and trj_lst != None:
            # print('adding to cluster, --j--')
            clu[trj_lst].add(i)
            clst.append([i, int(trj_lst) + lx, 1 - X, len(clu[trj_lst])])
            clu[str(it)] = clu.pop(trj_lst)

        elif tri_lst != None and trj_lst != None:
            # print('Merging big clusters')
            tmp = [int(tri_lst), int(trj_lst)]
            tmp.sort()
            clst.append(
                [tmp[0] + lx, tmp[1] + lx, 1 - X, len(clu[tri_lst]) + len(clu[trj_lst])])

            clu[tri_lst].update(clu[trj_lst])
            clu.pop(trj_lst)
            clu[str(it)] = clu.pop(tri_lst)



        deleterx(s, adj, ss, max_val, arg_val, i, j, no_val, fr,lx, not_in)

        time2 = time.time()
        print('function took s', time2 - time1)

    if len(use_clu) == 0:
        use_clu = clu
        trgn = []
        print('MIIIIIIILK')

    if link == "average" or link == "single":
        #tr = np.zeros((lx,lx))
        #tr = np.zeros(s.shape)
        #print(tr.shape)
        #for i in weird:
        #    tr[i[0]][i[1]] = tr[i[1]][i[0]] = i[2]
        print("changing order of avereraged linked nodes")
        weird.sort()
        weird.reverse()
        return weird_maker(weird,len(s))#, np.ones(s.shape))



    return np.array(clst), use_clu, trgn

