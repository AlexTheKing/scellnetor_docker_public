import numpy as np



def make_combined_simmatrix_pearson(d1, d2, lst_g, adj_mtrx, r1, r2, reps1, reps2, movav, corr_type=False, dist="euclidean", umap_dim=2):

    """
    Before entering this function, the replicates have already been bioled down to a single "replicate"
    based on the average or the median.

    :param d1:
    :param d2:
    :param lst_g:
    :param adj_mtrx:
    :param corr_type:
    :return:
    """
    from scipy.spatial.distance import cdist
    import sklearn.metrics
    new_d1 = []
    new_d2 = []
    v = []
    indx = []
    labls = []
    dels = []
    R1 = []
    R2 = []

    r1 = np.array(r1)
    r2 = np.array(r2)
    r1[r1 < 0] = 0
    r2[r2 < 0] = 0
    nei = movav
    dist = "correlation"

    ln_d1 = len(d1)
    print('length of dataset 1: ', ln_d1)



    if nei > ln_d1:
        nei = len(d1[0]) - 1
    if nei < 0:
        ne1 = 0


    if len(d2) == 0:

        
        ### condition 1
        M = []#= np.zeros((d1.shape[0], d1.shape[1] - (nei + x1) + 1))
        #tmp_len = len(d1[0]) - (nei + x1) + 1
        for i in range(len(d1)):
            M.append(np.convolve(d1[i], np.ones(nei,)/nei, mode="valid"))
            #for ii in range(tmp_len):
            #    M[i][ii] += np.average(d1[i][ii:ii + nei + x1])
        M = np.array(M).copy()
        M = M - np.min(M)
        M1 = M / np.max(M)
        #M1 = M.copy()
        ln_d1 = len(M1)

        if umap_dim >= len(M1[0]):
            print("The inserted UMAP dims are unvalid - will be reduced to:", 2)
            umap_dim = 2

        import umap
        d1 = umap.UMAP(n_neighbors=30, n_components = umap_dim, 
                       min_dist=0.0,
                       metric=dist, random_state=42).fit_transform(M1)
        print("size of umapped data:", d1.shape)
        slk = sklearn.metrics.pairwise.euclidean_distances(np.array(d1),np.array(d1))
        slk = np.array(slk, dtype='float32')
        slk = slk - np.min(slk)
        slk = slk / (np.max(slk) + 0.000001) 
        slk = abs(slk - 1)
        return slk, adj_mtrx, lst_g, M1



    if len(d2) != 0:


        if nei > len(d2[0]):
            nei = len(d2[0]) - 1
        if nei < 0:
            ne1 = 0


        def find_size_dif(d1,d2):
            t = [d1,d2]   
            tmp = np.argmin(t)
            if tmp == 0:
                x1 = 0
                x2 = abs(t[0] - t[1])
            else:
                x1 = abs(t[0] - t[1])
                x2 = 0
            return x1, x2

        x1, x2 = find_size_dif(len(d1[0]),len(d2[0]))

        ### condition 1
        M = []#= np.zeros((d1.shape[0], d1.shape[1] - (nei + x1) + 1))
        #tmp_len = len(d1[0]) - (nei + x1) + 1
        for i in range(len(d1)):
            M.append(np.convolve(d1[i], np.ones((nei + x1,))/(nei + x1), mode="valid"))
            #for ii in range(tmp_len):
            #    M[i][ii] += np.average(d1[i][ii:ii + nei + x1])
        M1 = np.array(M).copy()
        ln_d1 = len(M1)

        ### condition 2
        M = []#np.zeros((d2.shape[0], d2.shape[1] - (nei + x2) + 1))
        #tmp_len = len(d2[0]) - (nei + x2) + 1
        for i in range(len(d2)):
            M.append(np.convolve(d2[i], np.ones((nei + x2,))/(nei + x2), mode="valid"))
            #for ii in range(tmp_len):
            #    M[i][ii] += np.average(d2[i][ii:ii + nei + x2])
        M2 = np.array(M).copy()
        print("here",M1.shape, M2.shape, x1, x2)

        M3 = np.concatenate((M1,M2), axis=0)
        M3_1 = np.concatenate((M2,M1), axis=0)

        if umap_dim >= len(M3[0]):
            print("The inserted UMAP dims are unvalid - will be reduced to:", 2)
            umap_dim = 2

        mi = np.min(M3)
        ma = np.max(M3-np.min(M3))
        M1 = M1 - mi
        M1 = M1 / ma
        M2 = M2 - mi
        M2 = M2 / ma

        import umap
        ## see here for clustering with UMAP: https://umap-learn.readthedocs.io/en/latest/clustering.html
        print("Reducing dimentions with UMAP")
        d3 = umap.UMAP(n_neighbors=30, n_components = umap_dim, 
                       min_dist=0.0,
                       metric=dist, random_state=42).fit_transform(M3) #M3
        d1 = d3[:ln_d1]
        d2 = d3[ln_d1:]
        print("size of umapped data:", d3.shape)

        d3_1 = umap.UMAP(n_neighbors=30, n_components = umap_dim,
                       min_dist=0.0,
                       metric=dist, random_state=42).fit_transform(M3_1) #M3
        d1_1 = d3_1[ln_d1:]
        d2_1 = d3_1[:ln_d1]
        print("size of umapped data:", d3_1.shape)
        assert d3.shape == d3_1.shape
        d3_1 = np.concatenate((d1_1,d2_1), axis=0)

        ## make sure the datasets have identical lengths...
        assert len(d1) == len(d2) == len(d1_1) == len(d2_1)
        print('length of dataset 2: ', ln_d1)
        print('\nCalculating correlation coeff. of data1(gene1) vs. data2(gene2) and etc')


        v = np.linalg.norm(np.array(d1) - np.array(d2), axis=-1) 
        v_1 = np.linalg.norm(np.array(d1_1) - np.array(d2_1), axis=-1)


        v = (np.array(v) + np.array(v_1)) / 2
        if corr_type == "neg":
            v = v - (np.min(v)-0.000001)
            v = v / np.max(v)

        if corr_type == "pos":
            v = v - np.min(v)
            v = v / (np.max(v)+0.000001)
            v = abs(v-1)


        indx = list(range(ln_d1))
        labls = lst_g

        r1[r1 < 0] = 0 ## Do this such that the furthest distance is not zeroed out.
        r2[r2 < 0] = 0

        print('calculating similarity matrix based on the two groups')
        ln = len(d1)


        zz = sklearn.metrics.pairwise.euclidean_distances(np.array(d3), np.array(d3))
        zz_1 = sklearn.metrics.pairwise.euclidean_distances(np.array(d3_1), np.array(d3_1))
        zz = (zz + zz_1) / 2

        zz = np.array(zz, dtype='float32')
        zz = zz / (np.max(zz)+0.000001)

        D1 = zz[:ln, :ln].copy()
        D1 = abs(D1 - 1)

        D2 = zz[ln:, ln:].copy()
        D2 = abs(D2 - 1)

        D21 = zz[:ln, ln:].copy()
        if corr_type == 'pos':
            D21 = abs(D21 - 1)
        D21[np.tril_indices(D21.shape[0], 0)] = 0.

        D12 = zz[:ln, ln:].copy()
        D12 = D12.T
        if corr_type == 'pos':
            D12 = abs(D12 - 1)
        D12[np.tril_indices(D12.shape[0], 0)] = 0.

        slk = D1 * D2 * D12 * D21

        #slk = dd.copy()

        if reps1 == 1 and reps2 == 1:
            X = v
        if reps1 == 1 and reps2 != 1:
            X = v * r2
            #X = (v+r2) / 2
        if reps1 != 1 and reps2 == 1:
            X = v * r1
            #X = (v+r1) / 2
        if reps1 != 1 and reps2 != 1:
            #X = (v+r1+r2) / 3
            X = v * r1 * r2
        X = X / np.max(X)

        slk[np.tril_indices(slk.shape[0], 0)] = 0.
        slk += slk.T

        slk = slk * X
        slk = slk * X.reshape((len(X),1))

        #slk += slk.T
        #slk= slk**2
        slk = slk - (np.min(slk) - 0.000001)
        slk = slk / np.max(slk)

        slk = np.array(slk, dtype='float32')
        adj_mtrx = np.array(adj_mtrx, dtype='uint8')

        print('size of simmatrix: ', slk.shape)
        print('size of adjmatrix: ', adj_mtrx.shape)


        #return slk, adj_mtrx, v, indx, labls, dels, new_d1, new_d2
        return slk, adj_mtrx, v, indx, labls, M1, M2


def make_combined_simmatrix_euclidean(d1, d2, lst_g, adj_mtrx, r1, r2, reps1, reps2, movav, corr_type=False, dist="euclidean", umap_dim=2):

    """
    Before entering this function, the replicates have already been bioled down to a single "replicate"
    based on the average or the median.

    :param d1:
    :param d2:
    :param lst_g:
    :param adj_mtrx:
    :param corr_type:
    :return:
    """

    import sklearn.metrics
    from scipy.spatial import distance
    #from scipy.spatial.distance import pdist
    from scipy.spatial.distance import cdist
    nei = movav #50
    ln_d1 = len(d1)
    print('length of dataset 1: ', ln_d1)

    if nei > ln_d1:
        nei = len(d1[0]) - 1
    if nei < 0:
        ne1 = 0

    if len(d2) == 0:
        ### condition 1
        M = []#= np.zeros((d1.shape[0], d1.shape[1] - (nei + x1) + 1))
        #tmp_len = len(d1[0]) - (nei + x1) + 1
        for i in range(len(d1)):
            M.append(np.convolve(d1[i], np.ones(nei,)/nei, mode="valid"))
            #for ii in range(tmp_len):
            #    M[i][ii] += np.average(d1[i][ii:ii + nei + x1])
        M = np.array(M).copy()
        M = M - np.min(M)
        M1 = M / np.max(M)
        #M1 = M.copy()
        ln_d1 = len(M1)

        if umap_dim >= len(M1[0]):
            print("The inserted UMAP dims are unvalid - will be reduced to:", 2)
            umap_dim = 2

        import umap
        d1 = umap.UMAP(n_neighbors=30, n_components = umap_dim, 
                       min_dist=0.0,
                       metric=dist, random_state=42).fit_transform(M1)
        print("size of umapped data:", d1.shape)

        slk = sklearn.metrics.pairwise.euclidean_distances(np.array(d1),np.array(d1))
        slk = np.array(slk, dtype='float32')
        slk = slk - np.min(slk)
        slk = slk / (np.max(slk) + 0.000001)
        slk = abs(slk - 1)
        return slk, adj_mtrx, lst_g, M1



    if len(d2) != 0:

        if nei > len(d2[0]):
            nei = len(d2[0]) - 1
        if nei < 0:
            ne1 = 0



        def find_size_dif(d1,d2): 
            t = [d1,d2]   
            tmp = np.argmin(t)
            if tmp == 0:
                x1 = 0
                x2 = abs(t[0] - t[1])
            else:
                x1 = abs(t[0] - t[1])
                x2 = 0
            return x1, x2

        x1, x2 = find_size_dif(len(d1[0]),len(d2[0]))

        ### condition 1
        M = []#= np.zeros((d1.shape[0], d1.shape[1] - (nei + x1) + 1))
        #tmp_len = len(d1[0]) - (nei + x1) + 1
        for i in range(len(d1)):
            M.append(np.convolve(d1[i], np.ones((nei + x1,))/(nei + x1), mode="valid"))
            #for ii in range(tmp_len):
            #    M[i][ii] += np.average(d1[i][ii:ii + nei + x1])
        M1 = np.array(M).copy()
        ln_d1 = len(M1)

        ### condition 2
        M = []#np.zeros((d2.shape[0], d2.shape[1] - (nei + x2) + 1))
        #tmp_len = len(d2[0]) - (nei + x2) + 1
        for i in range(len(d2)):
            M.append(np.convolve(d2[i], np.ones((nei + x2,))/(nei + x2), mode="valid"))
            #for ii in range(tmp_len):
            #    M[i][ii] += np.average(d2[i][ii:ii + nei + x2])
        M2 = np.array(M).copy()
        print("here",M1.shape, M2.shape, x1, x2)

        ### Concatenation
        M3 = np.concatenate((M1,M2), axis=0)
        M3_1 = np.concatenate((M2,M1), axis=0)

        if umap_dim >= len(M3[0]):
            print("The inserted UMAP dims are unvalid - will be reduced to:",  2)
            umap_dim = 2

        ### prep timelines for print
        mi = np.min(M3)
        ma = np.max(M3-np.min(M3))
        M1 = M1 - mi
        M1 = M1 / ma
        M2 = M2 - mi
        M2 = M2 / ma

        import umap
        ## see here for clustering with UMAP: https://umap-learn.readthedocs.io/en/latest/clustering.html
        print("Reducing dimentions with UMAP")
        d3 = umap.UMAP(n_neighbors=30,  n_components = umap_dim,
                       min_dist=0.0,
                       metric=dist, random_state=42).fit_transform(M3) #M3
        d1 = d3[:ln_d1]
        d2 = d3[ln_d1:]
        print("size of umapped data:", d3.shape)

        d3_1 = umap.UMAP(n_neighbors=30, n_components = umap_dim,
                       min_dist=0.0,
                       metric=dist, random_state=42).fit_transform(M3_1) #M3
        d1_1 = d3_1[ln_d1:]
        d2_1 = d3_1[:ln_d1]
        print("size of umapped data:", d3_1.shape)
        assert d3.shape == d3_1.shape

        d3_1 = np.concatenate((d1_1,d2_1), axis=0)


        ## make sure the datasets have identical lengths...
        assert len(d1) == len(d2) == len(d1_1) == len(d2_1)
        print('length of dataset 2: ', ln_d1)
        print('\nCalculating correlation coeff. of data1(gene1) vs. data2(gene2) and etc')

        ### calculating v
        v = np.linalg.norm(np.array(d1) - np.array(d2), axis=-1) 
        v_1 = np.linalg.norm(np.array(d1_1) - np.array(d2_1), axis=-1)


        v = (np.array(v) + np.array(v_1)) / 2
        if corr_type == "neg":
            v = v - (np.min(v)-0.000001)
            v = v / np.max(v)

        if corr_type == "pos":
            v = v - np.min(v)
            v = v / (np.max(v)+0.000001)
            v = abs(v-1)


        indx = list(range(ln_d1))
        labls = lst_g

        r1[r1 < 0] = 0 ## Do this such that the furthest distance is not zeroed out.
        r2[r2 < 0] = 0

        print('calculating similarity matrix based on the two groups')
        ln = len(d1)
        zz = sklearn.metrics.pairwise.euclidean_distances(np.array(d3), np.array(d3))
        zz_1 = sklearn.metrics.pairwise.euclidean_distances(np.array(d3_1), np.array(d3_1))
        zz = (zz + zz_1) / 2
        zz = np.array(zz, dtype='float32')
        zz = zz / (np.max(zz)+0.000001)

        D1 = zz[:ln, :ln].copy()
        D1 = abs(D1 - 1)

        D2 = zz[ln:, ln:].copy()
        D2 = abs(D2 - 1)

        D21 = zz[:ln, ln:].copy()
        if corr_type == 'pos':
            D21 = abs(D21 - 1)
        D21[np.tril_indices(D21.shape[0], 0)] = 0.

        D12 = zz[:ln, ln:].copy()
        D12 = D12.T
        if corr_type == 'pos':
            D12 = abs(D12 - 1)
        D12[np.tril_indices(D12.shape[0], 0)] = 0.

        slk = D1 * D2 * D12 * D21
        #slk = dd.copy()

        if reps1 == 1 and reps2 == 1:
            X = v
        if reps1 == 1 and reps2 != 1:
            X = v * r2
            #X = (v+r2) / 2
        if reps1 != 1 and reps2 == 1:
            X = v * r1
            #X = (v+r1) / 2
        if reps1 != 1 and reps2 != 1:
            #X = (v+r1+r2) / 3
            X = v * r1 * r2
        X = X / np.max(X)

        slk[np.tril_indices(slk.shape[0], 0)] = 0.
        slk += slk.T

        slk = slk * X
        slk = slk * X.reshape((len(X),1))

        ### normelizing slk
        slk = slk - (np.min(slk) - 0.000001)
        slk = slk / np.max(slk)

        slk = np.array(slk, dtype='float32')
        adj_mtrx = np.array(adj_mtrx, dtype='uint8')

        print('size of simmatrix: ', slk.shape)
        print('size of adjmatrix: ', adj_mtrx.shape)


        return slk, adj_mtrx, v, indx, labls, M1, M2#,d1, d2 # M1, M2

