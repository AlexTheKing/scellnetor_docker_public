import argparse
import pandas as pd
from chacs_vals import cluster as clusterX
#from graph_functions import*
from process_temporal_data import*
import networkx
import os
import networkx as nx
import time
import numpy as np
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram, set_link_color_palette, linkage
from find_coefficient_of_determination_for_groups import boil_down_data
from new_process_temp_data import make_combined_simmatrix_dist
from annotations import retrieve_annotation
import pickle
from goterms import*

parser = argparse.ArgumentParser()

parser.add_argument("--path_edge_list",
                    required=True,
                    type=str,
                    default=False,
                    help="Path to file that contains that will receive a list of edges")

parser.add_argument("--link",
                    required=False,
                    type=str,
                    choices= ['average', 'complete', "single"],
                    default=False,
                    help="Path to file that contains that will receive a list of edges")

parser.add_argument("--group1",
                    required=True,
                    type=str,
                    nargs='+',
                    help="Path to the first or the only timeseries dataset/group of time series datasets used as input to CHAC")

parser.add_argument("--group2",
                    required=False,
                    type=str,
                    nargs='+',
                    default=[],
                    help="Path to the second timeseries dataset/group of time series datasets used as input to CHAC")

parser.add_argument("--reps1",
                    required=False,
                    type=int,
                    default=1,
                    help="Number of replicates if all replicates are gathered in one spreadsheet. ")

parser.add_argument("--reps2",
                    required=False,
                    type=int,
                    default=1,
                    help="the similarity matrix will be divided into frac*frac smaller matrices")

parser.add_argument("--mid",
                    required=False,
                    type=str,
                    choices= ['average', 'median'],
                    default='median',
                    help="When comparing timlines of diff. replicates how to find the middle value?")

parser.add_argument("--name",
                    required=True,
                    type=str,
                    default=None,
                    help="Name that will be used for the different plots and the different files")

parser.add_argument("-l",
                    required=False,
                    type=int,
                    default=10,
                    help="Only finds at least size 'l' clusters")

parser.add_argument("-K",
                    required=False,
                    type=int,
                    default=3,
                    help="Number of clusters to be found")


parser.add_argument("--corr_type",
                    required=False,
                    type=str,
                    default=False,
                    choices= ['pos', 'neg', False],
                    help="Type of correlation the the clustering will focus on")

parser.add_argument("--dist",
                    required=False,
                    type=str,
                    default="pearson",
                    choices= ['pearson', 'euclidean', "manhattan", "minkowski"],
                    help="Distance meassure used - pearson or euclidean")

parser.add_argument("--movav",
                    required=False,
                    type=int,
                    default=10,
                    help="Size of the moving average")

parser.add_argument("--umap",
                    required=False,
                    default="False",
                    choices= ["False","True"],
                    help="Wheter to use UMAP or not")

parser.add_argument("--umap_dim",
                    required=False,
                    type=int,
                    default=2, 
                    help="Dimensions UMAP will reduce the data to")

args = parser.parse_args()






if __name__ == "__main__":

    np.random.seed(111)


    print("Reading edge list and removing loops")
    g = networkx.read_edgelist(path=args.path_edge_list, delimiter="\t")
    tmp = 0
    for gene in g:
        if gene in g[gene]:
            tmp += 1
            g.remove_edge(gene, gene)
    print("Read edge list, network contains {} nodes and {} edges".format(len(g.nodes()), len(g.edges())))
    print('Removed', tmp, 'loops')
    print("computing adjacency matrix...")
    lst_g = list(g)
    lst_g.sort()
    adj = networkx.to_numpy_matrix(g, nodelist=lst_g) ##### kig lige på det her!!! sortere jeg generne i CSV filerne ?? kan godt være jeg skla lave om i den der laver entrez1 osv..
    adj = np.array(adj)
    print('This is the shape of the adjacency matrix:', adj.shape)



    ##################################################

    if args.dist == "correlation":
        args.dist = "pearson"


    if len(args.group2) == 0:

        t1, gene_list, r1, symbs1 = boil_down_data(args.group1, args.group2, args.reps1, args.reps2, args.mid)

        dl1 = []
        dlg = []
        temp1 = set(gene_list)
        temp_g = set(lst_g)

        ## Removing genes from gene_list if it contains genes that are not in the PPI network
        for i in gene_list:
            if i not in temp_g:
                dl1.append(gene_list.index(i))

        ## Removing genes from the network if it contains genes that are not in either gene lists
        for i in lst_g:
            if i not in temp1:# or i not in temp2:
                dlg.append(lst_g.index(i))

        adj = np.delete(adj, (dlg), axis=0)
        adj = np.delete(adj, (dlg), axis=1)
        lst_g = np.delete(np.array(lst_g), (dlg), axis=0)

        t1 = np.delete(t1, (dl1), axis=0)
        r1 = np.delete(np.array(r1), (dl1), axis=0)
        gene_list = np.delete(np.array(gene_list), (dl1), axis=0)

        print("everything has the same lenghts:")
        print(len(r1), len(gene_list), len(adj), len(t1), len(lst_g), 'GGGGGGGGGGG')



    if len(args.group2) != 0:
        t1, t2, gene_list, gene_list2, r1, r2, symbs1, symbs2 = boil_down_data(args.group1, args.group2, args.reps1, args.reps2, args.mid)
        for kmk in range(len(gene_list)):
            print(gene_list[kmk], gene_list2[kmk], symbs1[kmk], symbs2[kmk])

        dl1 = []
        dl2 = []
        dlg = []
        temp1 = set(gene_list)
        temp2 = set(gene_list2)
        temp_g = set(lst_g)

        ## Removing genes from gene_list if it contains genes that are not in the PPI network
        for i in gene_list:
            if i not in temp_g:
                dl1.append(gene_list.index(i))

        ## Removing genes from gene_list2 if it contains genes that are not in the PPI network
        for i in gene_list2:
            if i not in temp_g:
                dl2.append(gene_list2.index(i))

        ## Removing genes from the PPI network if it contains genes that are not in either gene lists
        for i in lst_g:
            if i not in temp1 or i not in temp2:
                dlg.append(lst_g.index(i))


        adj = np.delete(adj, (dlg), axis=0)
        adj = np.delete(adj, (dlg), axis=1)
        lst_g = np.delete(np.array(lst_g), (dlg), axis=0)

        t1 = np.delete(t1, (dl1), axis=0)
        t2 = np.delete(t2, (dl2), axis=0)

        r1 = np.delete(np.array(r1), (dl1), axis=0)
        r2 = np.delete(np.array(r2), (dl2), axis=0)


        gene_list = np.delete(np.array(gene_list), (dl1), axis=0)
        gene_list2 = np.delete(np.array(gene_list2), (dl2), axis=0)
        symbs1 = np.delete(np.array(symbs1), (dl1), axis=0)
        symbs2 = np.delete(np.array(symbs2), (dl2), axis=0)
        print("everything has the same lenghts:")
        print(len(r1), len(r2), len(gene_list), len(gene_list2), len(adj), len(t1), len(t2), len(lst_g), len(symbs1), len(symbs2))

        ## Now all the gene regulatory network and the gene lists should contain the same genes.


    #for i in range(len(lst_g)):
    #    print(lst_g[i], gene_list[i], gene_list2[i])

    #for i in range(len(lst_g)):
    #    assert lst_g[i] == gene_list[i] == gene_list2[i]


    tm = os.path.dirname(os.path.realpath(__file__))
    print("*******************", tm)
    other_path = tm.split("/")
    other_path = "/".join(other_path[:-1]) + "/" + args.name.split("_")[0]
    print("XXXXXXXXXXXXXXXXx", other_path)
    import glob

    if len(glob.glob1(other_path,"all_clustering_data_"+args.dist+"_.pkl")) == 0: ## should be removed, does not work and should not work

        if len(args.group2) != 0:
            if args.umap == "False":
                mtrx, adj_mtrx, v, indx, labls, M1, M2 = make_combined_simmatrix_dist(t1,t2, lst_g, adj, args.movav, corr_type=args.corr_type, dist=args.dist)
            if args.umap == "True":
                print(t1.shape, t2.shape)
                if args.dist == "pearson":
                    mtrx, adj_mtrx, v, indx, labls, M1, M2 = make_combined_simmatrix_pearson(t1,t2, lst_g, adj, r1, r2, args.reps1, args.reps2, args.movav, corr_type=args.corr_type, dist=args.dist, umap_dim=args.umap_dim)
                    M1 = np.array(M1)
                    M2 = np.array(M2)
                if args.dist == "euclidean" or args.dist == "minkowski" or args.dist == "manhattan":
                    mtrx, adj_mtrx, v, indx, labls, M1, M2  = make_combined_simmatrix_euclidean(t1, t2, lst_g, adj, r1, r2, args.reps1, args.reps2, args.movav, corr_type=args.corr_type, dist=args.dist, umap_dim=args.umap_dim)
                with open(other_path + "/all_clustering_data.pkl", "wb") as FIL:
                    pickle.dump([mtrx, adj_mtrx, v, indx, labls, M1, M2],FIL)



        if len(args.group2) == 0:
            
            if args.umap == "False":
                print("singleton pearson!!!!!")
                mtrx, adj_mtrx, labls, M1 = make_combined_simmatrix_dist(t1,[], lst_g, adj, args.movav, corr_type=args.corr_type, dist=args.dist)
            if args.umap == "True":
                if args.dist == "pearson":
                    print("*****Using CORRELATION*****")
                    print(len(t1), len(r1))
                    mtrx, adj_mtrx, labls, M1 = make_combined_simmatrix_pearson(t1, [], lst_g, adj, r1, [], args.reps1, args.reps2, args.movav, corr_type=args.corr_type, dist=args.dist, umap_dim=args.umap_dim)
    
                if args.dist == "euclidean" or args.dist == "minkowski" or args.dist == "manhattan":
                    mtrx, adj_mtrx, labls, M1 = make_combined_simmatrix_euclidean(t1, [], lst_g, adj, r1, [], args.reps1, args.reps2, args.movav, corr_type=args.corr_type, dist=args.dist, umap_dim=args.umap_dim)
                with open(other_path + "/all_clustering_data.pkl", "wb") as FIL:
                    pickle.dump([mtrx,adj_mtrx,labls,M1],FIL)

    if len(glob.glob1(other_path,"all_clustering_data.pkl")) == 1:
        print("already have clustering data...")
        if len(args.group2) != 0:
            with open(other_path + "/all_clustering_data.pkl", "rb") as FIL:
                mtrx, adj_mtrx, v, indx, labls, M1, M2 = pickle.load(FIL)

        if len(args.group2) == 0:
            with open(other_path + "/all_clustering_data.pkl", "rb") as FIL:
                mtrx, adj_mtrx, labls, M1 = pickle.load(FIL)



#####
# Here, remove zeros and NaNs
# comapre lists of gene IDs --> they have to contain the same
# make it into lists of datasets for each group
#####

#####################
# DO SOMETHING IF GROUPS > 1
# make a function that finds representational dataset for each group
#####################


#pkg = list(g.neighbors("5592"))
#print(pkg, 'CCCCCCCCCCCCCCCCCCC')
# PRKG1 5592
# PRKG2 5593
# NOX4 50507
# NOX5 79400
###############

    print("new size of matrices:", mtrx.shape, adj_mtrx.shape, np.max(mtrx))
    mod_glst = []

    mtrx_av = np.average(mtrx)

    #plt.plot(mtrx[list(labls).index("3040")]*adj_mtrx[list(labls).index("3040")])
    #plt.savefig("_red_{}_scatter.png")  # ""Timeseries_dendogram_adj.png")
    #plt.close()
    #adj_mtrx1 = np.ones(adj_mtrx.shape)
    #adj_mtrx1 = np.load("clique_adj.npy")
    #adj_mtrx1 = np.load("clique_adj_set23.npy")

    print('initiating the CHAC, sim_matrix size:', mtrx.shape)
    time1 = time.time()
    cl, p, trgn, clu_vals, weird = clusterX(mtrx, adj_mtrx, mod_glst, args.l, args.K, args.link)
    time2 = time.time()
    print('function took s', time2-time1)
    #np.set_printoptions(precision=2, suppress=True)


    all_vals = []
    nms = set()
    graph_nms = []
    clst_values = []
    print("Number of clusters:",len(p))
    for k,v in p.items():
        print("cluster size",k,len(v))#,v)
        nms.update(v.copy())
        graph_nms.append(list(v))
        clst_values.append(list(clu_vals[k]))
        all_vals += list(clu_vals[k])
        if len(v) > 1000:
            bb = v.copy()

    #if list(labls).index("3040") in nms:
    #    print("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ")


    #import seaborn as sns
    #def plot_with_conf_int(A, col):

    #    A_ = []
    #    indx_ = []
    #    for i in A:
    #        A_ += list(i)
    #        indx_ += list(range(len(i)))

    #    df = pd.DataFrame(A_, index=None, columns=["tmln"])
    #    df.insert(loc=0, column='indx', value=indx_)

    #    ax = sns.lineplot(x="indx", y="tmln", data=df, ci=95, estimator="mean", color=col) #np.median
    #    #plt.savefig(name)  # ""Timeseries_dendogram_adj.png")
    #    #plt.close()

    def plot_mean_and_CI(mean, lb, ub, color_mean=None, color_shading=None):
        # plot the shaded range of the confidence intervals
        plt.fill_between(range(mean.shape[0]), ub, lb,
                     color=color_shading, alpha=.5)
        # plot the mean on top
        plt.plot(mean, color_mean)
        plt.title('Mean 95% confidence interval')
        #plt.ylabel("Hyper-similarity score")
        plt.ylabel("Normalized moving-average-modified gene expression")
        plt.xlabel("Moving-average-modified number of single cells")

    def plot_mean_and_CI_ylim(mean, lb, ub, color_mean=None, color_shading=None):
        # plot the shaded range of the confidence intervals
        plt.fill_between(range(mean.shape[0]), ub, lb,
                     color=color_shading, alpha=.5)
        # plot the mean on top
        plt.plot(mean, color_mean)
        plt.title('Mean 95% confidence interval')
        #plt.ylabel("Hyper-similarity score")
        plt.ylabel("Normalized moving-average-modified gene expression")
        plt.xlabel("Moving-average-modified number of single cells")
        plt.ylim(0,1)

    def mean_confidence_interval(data, confidence=0.95):
        a = np.array(data)
        n = len(a)
        m, se = np.mean(a), scipy.stats.sem(a)
        h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
        return m, m-h, m+h

    import scipy.stats

    #tm = os.path.dirname(os.path.realpath(__file__))
    #print("*******************", tm)
    #other_path = tm.split("/")
    #other_path = "/".join(other_path[:-1]) + "/" + args.name.split("_")[0]
    #print("XXXXXXXXXXXXXXXXx", other_path)
    all_g_names = []

    if len(args.group2) == 0:
        print(M1.shape)
        col = ["red", "blue", "green", "yellow", "purple", "white", "brown", "orange", "teal", "silver", "pink"]
        j = -1
        for i in graph_nms:
            if len(i) != 0:#len(nms):
                if len(i) >= args.l:

                    if args.dist == "pearson" or args.dist == "euclidean" or args.dist == "minkowski" or args.dist == "manhattan":
                        j += 1

                        print(M1[i].shape)

                        tmm = {}
                        for rind in i:
                            tmm[str(gene_list[rind])] = symbs1[rind]
                        all_g_names.append(tmm)
                        print(tmm)
                        ub1 = []
                        lb1 = []
                        m1 = []
                        for ar in M1[i].T:
                            x,y,z = mean_confidence_interval(ar, confidence=0.95)
                            m1.append(x)
                            lb1.append(y)
                            ub1.append(z)
                        m1 = np.array(m1)
                        ub1 = np.array(ub1)
                        lb1 = np.array(lb1)

                        fig = plt.figure(1, figsize=(12, 10))
                        plot_mean_and_CI_ylim(m1, ub1, lb1, color_mean='#FFC20A', color_shading='#FFFF66')
                        plt.savefig(other_path + "/" + str(j) +"_"+ args.name + "_red_{}_ylim_conf.pdf".format(j)) 
                        plt.close()

                        fig = plt.figure(1, figsize=(12, 10))
                        plot_mean_and_CI(m1, ub1, lb1, color_mean='#FFC20A', color_shading='#FFFF66')
                        plt.savefig(other_path + "/" + str(j) +"_"+ args.name + "_red_{}_conf.png".format(j)) 
                        plt.savefig(other_path + "/" + str(j) +"_"+ args.name + "_red_{}_conf.pdf".format(j)) 
                        plt.close()


    wilc = []

    if len(args.group2) != 0:
        print(M1.shape)
        #plt.scatter(d1[:,0], d1[:,1], c="gray")
        col = ["red", "blue", "green", "yellow", "purple", "white", "brown", "orange", "teal", "silver", "pink"]
        j = -1
        for i in graph_nms:
            if len(i) != 0:#len(nms):
                #if list(labls).index("3040") in i:
                #    print("XXXXXXXXXXXXXXXXXXXX", i)
                if len(i) >= args.l:

                    if args.dist == "pearson" or args.dist == "euclidean" or args.dist == "minkowski" or args.dist == "manhattan":
                        j += 1

                        print(M1[i].shape, M2[i].shape)

                        tmm = {}
                        for rind in i:
                            tmm[str(gene_list[rind])] = symbs1[rind]
                        all_g_names.append(tmm)
                        print(tmm)
                        ub1 = []
                        lb1 = []
                        m1 = []
                        for ar in M1[i].T:
                            x,y,z = mean_confidence_interval(ar, confidence=0.95)
                            m1.append(x)
                            lb1.append(y)
                            ub1.append(z)
                        m1 = np.array(m1)
                        ub1 = np.array(ub1)
                        lb1 = np.array(lb1)
 

                        ub2 = []
                        lb2 = []
                        m2 = []
                        for ar in M2[i].T:
                            x,y,z = mean_confidence_interval(ar, confidence=0.95)
                            m2.append(x)
                            lb2.append(y)
                            ub2.append(z)
                        m2 = np.array(m2)
                        ub2 = np.array(ub2)
                        lb2 = np.array(lb2)

                        wilc.append([M1[i], M2[i]])
                        #print([M1[i].flatten(), M2[i].flatten()])

                        fig = plt.figure(1, figsize=(12, 10))
                        plot_mean_and_CI_ylim(m1, ub1, lb1, color_mean='#FFC20A', color_shading='#FFFF66')
                        plot_mean_and_CI_ylim(m2, ub2, lb2, color_mean='#0C7BDC', color_shading='#99FFFF')
                        plt.savefig(other_path + "/" + str(j) +"_"+ args.name + "_red_green_{}_ylim_conf.pdf".format(j)) ## png
                        plt.close()

                        fig = plt.figure(1, figsize=(12, 10))
                        plot_mean_and_CI(m1, ub1, lb1, color_mean='#FFC20A', color_shading='#FFFF66')
                        plot_mean_and_CI(m2, ub2, lb2, color_mean='#0C7BDC', color_shading='#99FFFF')
                        plt.savefig(other_path + "/" + str(j) +"_"+ args.name + "_red_green_{}_conf.pdf".format(j)) ## png
                        plt.savefig(other_path + "/" + str(j) +"_"+ args.name + "_red_green_{}_conf.png".format(j)) ## png
                        plt.close()


    thr = 1 - np.min(all_vals)

    nms = list(nms)
    nms = [labls[i] for i in nms]
    nms.sort()

    #new_names_list = nms

    n_graph_nms = []
    for i in range(len(graph_nms)):
        tmp = []
        for ii in range(len(graph_nms[i])):
            tmp.append(labls[graph_nms[i][ii]])
        n_graph_nms.append(tmp)

###############


    #### I do this below, to get the connecticity based on the similarity matrix. Remove connections if values are <=0
    """
    G = mtrx * adj_mtrx
    #print(G[:10,:10])
    G[G<0] = 0
    G[G>0] = 1
    
    rows, cols = np.where(G == 1)
    rows = list(rows)
    cols = list(cols)
    
    for i in range(len(rows)):
        rows[i] = labls[rows[i]]
    for i in range(len(cols)):
        cols[i] = labls[cols[i]]
    
    edges = zip(rows, cols)

    g = nx.Graph()
    g.add_edges_from(edges)
    
    """

    """
    #### this is the real one ####
    import sys
    sys.setrecursionlimit(15000)
    #print("VVVVVVVVVVVVVVVVVVV", list(labls).index("3040"))

    plt.figure(figsize=(250, 30))
    #plt.figure(figsize=(20, 15))
    plt.title('Constrained Hierarchical Clustering Dendrogram', {'fontsize': 30})
    plt.xlabel('Gene Symbols', {'fontsize': 25})
    plt.ylabel('Normalized Hypercorrelation', {'fontsize': 25})
    ytick1 = np.arange(0,11,1)/10
    ytick = [str(i) for i in ytick1[::-1]]
    plt.yticks(ytick1, ytick, size=20)
    R = dendrogram(
        cl,  # Z, cl
        leaf_rotation=90.,  # rotates the x axis labels
        leaf_font_size=16.,  # font size for the x axis labels
        #labels=new_names_list,#nnms,#new_names_list,  # list of string names
        color_threshold = thr,#0.2,#args.thr,
        above_threshold_color="grey"
    )

    plt.savefig(args.name + ".png")#""Timeseries_dendogram_adj.png")
    plt.close()
    ##################################

    
    from scipy.cluster.hierarchy import dendrogram, linkage
    import numpy as np
    import matplotlib.pyplot as plt

    d1 = d1/np.max(d1)

    Z = linkage(d1, method=args.link, metric="correlation")
    plt.figure(figsize=(20, 15))
    plt.title('Hierarchical Clustering Dendrogram')
    plt.xlabel('sample index')
    plt.ylabel('distance')
    dendrogram(
        Z,
        labels=labls,
        leaf_rotation=90.,  # rotates the x axis labels
        leaf_font_size=22.,  # font size for the x axis labels
    )
    plt.savefig("scipy_eu_pos_single.png")
    plt.close()
    """


    def make_simple_graph_plot(name, gtt1, edg, col, fsize=(40, 40), k=0.2, iterations=90):

        plt.figure(1, figsize=fsize)
        pos = nx.spring_layout(gtt1, seed=42, k=k, iterations=iterations)
        #nx.draw_networkx_nodes(gtt1, pos, nodelist=gtt1.nodes(), node_color="grey",node_size=1000)
        nx.draw_networkx_nodes(gtt1, pos, nodelist=gtt1.nodes(), node_color=col,node_size=1500)


        nx.draw_networkx_edges(gtt1, pos, alpha=0.5, width=3, edge_color="grey")
        nx.draw_networkx_labels(gtt1, pos, font_size=50, font_family='sans-serif', font_color="k")
        plt.axis('off')
        plt.savefig(name)
        plt.close()


    ### Change this shit!!! den skal over i den rigtige folder
    ##rp = os.getcwd()
    ##tm = rp.split("/")
    ##tm = tm[:-1]
    ##tm = "/".join(tm)
    ##tm += "/chac/"
    #tm = os.path.dirname(os.path.realpath(__file__))
    #other_path = tm.split("/")
    #other_path = "/".join(other_path[:-1]) + "/" + name.split("_")[0]

    taxid=9606
    import pickle
    with open(tm+"/all_goterms.pkl", "rb") as F:
        acc_go = pickle.load(F)
    #t = get_goeaobj(get_assoc_ncbi_taxids([taxid]), 9606)
    t = get_goeaobj(acc_go, 9606, tm+"/")
    n_graph_nms = all_g_names

    #mannwhitneyu
    mtrx_av = mtrx[mtrx > 0]
    print(mtrx_av)
    from scipy import stats
    from statsmodels.stats.multitest import multipletests
    #print("********",stats.mannwhitneyu(clst_values[-1], mtrx_av.flatten(), alternative="two-sided"))
    pvals = []
    #print("****", wilc)
    assert len(graph_nms) == len(clst_values)
    for i in range(len(graph_nms)):
        if len(graph_nms[i]) >= args.l:
            pvals.append( stats.mannwhitneyu(clst_values[i], mtrx_av.flatten(), alternative="two-sided")[1] )
            print("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ", pvals[-1])
    print(pvals)


    nr_of_pvs = len(pvals)
    for i in wilc:
        pvals.append(stats.wilcoxon(np.average(i[0],axis=0), np.average(i[1],axis=0) )[1])
    

    qvals_true = multipletests(pvals, method="fdr_bh")[1]
    print(qvals_true)
    qvals = qvals_true[:nr_of_pvs]
    p_gr = pvals[:nr_of_pvs] 
    with open(other_path + "/p_and_q_values_graph.pkl", "wb") as pq:
        pickle.dump([p_gr, qvals], pq)

    with open(other_path + "/p_and_q_values_lines.pkl", "wb") as pq:
        pickle.dump([pvals[nr_of_pvs:], qvals_true[nr_of_pvs:]], pq)
    
    with open(other_path + "/data_p_and_q_values_lines.pkl", "wb") as pq:
        pickle.dump(wilc, pq)


    qvals = qvals_true[:nr_of_pvs]

    print("making graphs")
    #### for bias graph
    j = -1
    for i in range(len(n_graph_nms)):
        if len(n_graph_nms[i]) != 0:#len(nms):
            if len(n_graph_nms[i]) >= args.l:
                j += 1
                #gtmp = g.subgraph(n_graph_nms[i])
                #gtmp, new_names_list, Descriptions, Summaries = retrieve_annotation(n_graph_nms[i], gtmp)
                gtmp = g.subgraph(list(n_graph_nms[i].keys()))

                nx.write_edgelist(gtmp, other_path + "/" +"{}_graph_entrez.csv".format(j), delimiter="\t")
                gtmp = nx.relabel_nodes(gtmp, n_graph_nms[i])
                nx.write_edgelist(gtmp, other_path + "/" +"{}_graph_symbol.csv".format(j), delimiter="\t")
                #np.average(clst_values[i])/mtrx_av) 
                #mann_u_test = stats.wilcoxon(M1[graph_nms[i]].flatten(), M2[graph_nms[i]].flatten(), correction=True)
                #print(mann_u_test)
                make_simple_graph_plot(other_path + "/" + "{}_".format(j)+args.name + "_size_{}_{}_graph.pdf".format(len(n_graph_nms[i]), qvals[j]), gtmp, list(gtmp.edges()), "tomato")#col[j] )
                make_simple_graph_plot(other_path + "/" + "{}_".format(j)+args.name + "_size_{}_{}_graph.png".format(len(n_graph_nms[i]), qvals[j]), gtmp, list(gtmp.edges()), "tomato")#col[j] )
                print("printed graph {}".format(i))
                print(mtrx_av, np.average(clst_values[i]), clst_values[i])
                v = t.run_study(set([int(i) for i in list(n_graph_nms[i].keys())]))
                sig = [r for r in v if r.p_fdr_bh < 0.05]
                #t.wr_txt("{}_goterms.txt".format(j), sig)
                t.wr_tsv(other_path + "/" + "{}_goterms.tsv".format(j), sig)



