import sys
from Bio import Entrez
import networkx as nx
import numpy as np


Entrez.email = 'agroen@imada.sdu.dk'


"""
based on function from https://biopython.org/wiki/Annotate_Entrez_Gene_IDs

"""


def retrieve_annotation(id_list, g):

    """
    This function is from:
    https://biopython.org/wiki/Annotate_Entrez_Gene_IDs

    :param id_list:
    :return:
    """

    def remap_annotations_to_g(id_list, g, annotations):

        new_names = {}
        new_names_list = []
        Descriptions = []
        Summaries = []

        for i in range(len(annotations)):
            for ii in range(len(id_list[i])):
                new_names[id_list[i][ii]] = annotations[i]["DocumentSummarySet"]["DocumentSummary"][ii]["Name"]
                new_names_list.append(annotations[i]["DocumentSummarySet"]["DocumentSummary"][ii]["Name"])
                Descriptions.append(annotations[i]["DocumentSummarySet"]["DocumentSummary"][ii]["Description"])
                Summaries.append(annotations[i]["DocumentSummarySet"]["DocumentSummary"][ii]["Summary"])

        G = nx.relabel_nodes(g, new_names)

        return G, new_names_list, Descriptions, Summaries

    number = int(np.ceil(len(id_list)/10000))

    tmp_ids = []
    for i in range(number):
        tmp_ids.append(id_list[10000 * i: 10000 * i + 10000])

    """Annotates Entrez Gene IDs using Bio.Entrez, in particular epost (to
    submit the data to NCBI) and esummary to retrieve the information.
    Returns a list of dictionaries with the annotations."""

    request = Entrez.epost("gene",id=",".join(id_list))
    try:
        result = Entrez.read(request)
    except RuntimeError as e:
        #FIXME: How generate NAs instead of causing an error with invalid IDs?
        print("An error occurred while retrieving the annotations.")
        print("The error returned was %s" % e)
        sys.exit(-1)

    webEnv = result["WebEnv"]
    queryKey = result["QueryKey"]

    annotations = []
    for i in range(len(tmp_ids)):
        data = Entrez.esummary(db="gene", webenv=webEnv, query_key =
            queryKey, )
        annotations.append(Entrez.read(data))

    print("Retrieved %d annotations for %d genes" % (len(annotations),
            len(id_list)))

    assert len(tmp_ids) == len(annotations)


    return remap_annotations_to_g(tmp_ids, g, annotations)



