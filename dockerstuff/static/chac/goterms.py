#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  6 10:37:15 2019

@author: alexander
"""
#from collections import defaultdict
#from goatools.associations import get_assoc_ncbi_taxids
#from goatools.go_search import GoSearch
#from goatools.base import get_godag
#from goatools.go_enrichment import GOEnrichmentStudy
from goatools.goea.go_enrichment_ns import GOEnrichmentStudyNS
#from goatools.associations import read_ncbi_gene2go
import numpy as np
from goatools.obo_parser import GODag

#taxid=9606
#taxid2asscs = defaultdict(lambda: defaultdict(lambda: defaultdict(set)))
#get_assoc_ncbi_taxids([taxid], taxid2asscs=taxid2asscs)
# taxid2asscs contains both GO2GeneIDs and GeneID2GOs.
#srch = GoSearch("go-basic.obo", go2items=taxid2asscs[taxid]['GeneID2GOs'])



"""
## this was how I generated the "acc_stuff" object

from goatools.base import download_ncbi_associations
fin_gene2go = download_ncbi_associations()

from goatools.anno.genetogo_reader import Gene2GoReader
# Read NCBI's gene2go. Store annotations in a list of namedtuples
objanno = Gene2GoReader(fin_gene2go, taxids=[9606])
ns2assoc = objanno.get_ns2assc()

for nspc, id2gos in ns2assoc.items():
    print("{NS} {N:,} annotated mouse genes".format(NS=nspc, N=len(id2gos)))

with open("all_goterms.pkl", "wb") as f:
    pickle.dump(ns2assoc, f)
"""


def get_goeaobj(acc_stuff, taxid, dr, method="fdr_bh"):

    #from goatools.test_data.genes_NCBI_9606_ProteinCoding import GENEID2NT
    from goatools.test_data.genes_NCBI_9606_All import GENEID2NT
    obo_dag = GODag(dr + "go-basic.obo")
    
    goeaobj = GOEnrichmentStudyNS(
        GENEID2NT.keys(), # List of mouse protein-coding genes
        acc_stuff, # geneid/GO associations
        obo_dag, # Ontologies
        propagate_counts = False,
        alpha = 0.05, # default significance cut-off
        methods = ['fdr_bh'])

    return goeaobj


#t = get_goeaobj(get_assoc_ncbi_taxids([taxid]), 9606)
#t = get_goeaobj(taxid2asscs[9606]["GeneID2GOs"], 9606)

#v = t.run_study(set([int(i) for i in list(g1.keys())]))
#sig = [r for r in v if r.p_fdr_bh < 0.05]
#t.wr_txt("x2_goTrms_{}.txt".format("test"), sig)



"""
xg = []
for i in ggg:
    xg.append([int(ii) for ii in i])

for i in xg:
    XX += 1
    pop = {}
    #for ii in i:
    #    pop[int(ii)] = taxid2asscs[9606]["GeneID2GOs"][int(ii)]
    #tmp1.append(pop)        

    v = t.run_study(set(i))
    sig = [r for r in v if r.p_fdr_bh < 0.05]
    t.wr_txt("x2_goTrms_{}.txt".format(clls[XX]), sig)
"""