import numpy as np



def make_combined_simmatrix_pearson(d1, d2, lst_g, adj_mtrx, r1, r2, reps1, reps2, movav, corr_type=False, dist="euclidean"):

    """
    Before entering this function, the replicates have already been bioled down to a single "replicate"
    based on the average or the median.

    :param d1:
    :param d2:
    :param lst_g:
    :param adj_mtrx:
    :param corr_type:
    :return:
    """
    from scipy.spatial.distance import cdist
    v = []
    indx = []
    labls = []

    nei = movav
    dist = "correlation"

    ln_d1 = len(d1)
    print('length of dataset 1: ', ln_d1)



    if nei > ln_d1:
        nei = len(d1[0]) - 1
    if nei < 0:
        ne1 = 0


    if len(d2) == 0:

        ### condition 1
        M = []
        for i in range(len(d1)):
            M.append(np.convolve(d1[i], np.ones(nei,)/nei, mode="valid"))
        M = np.array(M).copy()
        M = M - np.min(M)
        M1 = M / np.max(M)
        ln_d1 = len(M1)


        slk = cdist(np.array(M1),np.array(M1), metric = dist)
        slk = np.array(slk, dtype='float32')
        slk = slk - np.min(slk)
        slk = slk / (np.max(slk) + 0.000001) 
        slk = abs(slk - 1)
        return slk, adj_mtrx, lst_g, M1


    if len(d2) != 0:


        if nei > len(d2[0]):
            nei = len(d2[0]) - 1
        if nei < 0:
            nei = 0


        def find_size_dif(d1,d2):
            t = [d1,d2]   
            tmp = np.argmin(t)
            if tmp == 0:
                x1 = 0
                x2 = abs(t[0] - t[1])
            else:
                x1 = abs(t[0] - t[1])
                x2 = 0
            return x1, x2

        x1, x2 = find_size_dif(len(d1[0]),len(d2[0]))

        ### condition 1
        M = []
        for i in range(len(d1)):
            M.append(np.convolve(d1[i], np.ones((nei + x1,))/(nei + x1), mode="valid"))
        M1 = np.array(M).copy()
        ln_d1 = len(M1)

        ### condition 2
        M = []
        for i in range(len(d2)):
            M.append(np.convolve(d2[i], np.ones((nei + x2,))/(nei + x2), mode="valid"))
        M2 = np.array(M).copy()
        print("here",M1.shape, M2.shape, x1, x2)

        M3 = np.concatenate([M1,M2],axis=0)

        mi = np.min(M3)
        ma = np.max(M3)
        ma = ma - mi

        M1 = M1 - mi
        M1 = M1 / ma
        M2 = M2 - mi
        M2 = M2 / ma


        assert M1.shape == M2.shape
        print('length of dataset 2: ', ln_d1)
        print('\nCalculating correlation coeff. of data1(gene1) vs. data2(gene2) and etc')

        indx = list(range(ln_d1))
        labls = lst_g

        print('calculating similarity matrix based on the two groups')
        ln = len(d1)


        zz = cdist(np.array(M3), np.array(M3), metric = dist)
        zz = np.array(zz, dtype='float32')
        zz = zz / (np.max(zz)+0.000001)

        D1 = zz[:ln, :ln].copy()
        D1 = abs(D1 - 1)

        D2 = zz[ln:, ln:].copy()
        D2 = abs(D2 - 1)

        D21 = zz[:ln, ln:].copy()
        v = np.diagonal(D21)
        if corr_type == 'pos':
            D21 = abs(D21 - 1)
        D21[np.tril_indices(D21.shape[0], 0)] = 0.

        D12 = zz[:ln, ln:].copy()
        D12 = D12.T
        if corr_type == 'pos':
            D12 = abs(D12 - 1)
        D12[np.tril_indices(D12.shape[0], 0)] = 0.

        slk = D1 * D2 * D12 * D21

        if corr_type == "neg":
            v = v - (np.min(v)-0.000001)
            v = v / np.max(v)

        if corr_type == "pos":
            v = v - np.min(v)
            v = v / (np.max(v)+0.000001)
            v = abs(v-1)


        slk[np.tril_indices(slk.shape[0], 0)] = 0.
        slk += slk.T

        slk = slk * v
        slk = slk * v.reshape((len(v),1))

        slk = slk - (np.min(slk) - 0.000001)
        slk = slk / np.max(slk)

        slk = np.array(slk, dtype='float32')
        adj_mtrx = np.array(adj_mtrx, dtype='uint8')

        print('size of simmatrix: ', slk.shape)
        print('size of adjmatrix: ', adj_mtrx.shape)


        return slk, adj_mtrx, v, indx, labls, M1, M2


def make_combined_simmatrix_dist(d1, d2, lst_g, adj_mtrx, movav, corr_type=False, dist="euclidean"):


    from scipy.spatial.distance import cdist
    nei = movav #50
    ln_d1 = len(d1)
    print('length of dataset 1: ', ln_d1)

    if nei > ln_d1:
        nei = len(d1[0]) - 1
    if nei < 0:
        nei = 0

    if dist == "manhattan":
        dist = "cityblock"
    
    if dist == "pearson":
        dist = "correlation"
        
    print("metric", dist)

    if len(d2) == 0:

        ### condition 1
        M = []
        for i in range(len(d1)):
            M.append(np.convolve(d1[i], np.ones(nei,)/nei, mode="valid"))
        M = np.array(M).copy()
        M = M - np.min(M)
        M1 = M / np.max(M)
        ln_d1 = len(M1)


        slk = cdist(M1,M1, metric = dist)
        slk = np.array(slk, dtype='float32')
        if dist == "correlation":
            nan = np.isnan(slk)
            slk[nan] = 0
        slk = slk - np.min(slk)
        slk = slk / (np.max(slk) + 0.000001) 
        slk = abs(slk - 1)
        if dist == "correlation":
            slk[nan] = 0
            
        #if True:#weigh_by_value:
        #w = np.repeat([np.sum(d1,axis=-1)],len(d1), axis=0) + np.reshape(np.sum(d1,axis=-1),(len(d1),1))
        #w = (w - np.min(w)) + 0.000001#(abs(np.min(w)+0.000001)
        #w = w / np.max(w)
        #slk = slk*w
        #slk = slk/np.max(slk)
        #print(w, "MIIIIIIIIIIIIIIIIIIIIIIIIIIIILKY")
        slk[np.diag_indices(len(slk))] = 0
        return slk, adj_mtrx, lst_g, M1


    if len(d2) != 0:


        if nei > len(d2[0]):
            nei = len(d2[0]) - 1
        if nei < 0:
            nei = 0


        def find_size_dif(d1,d2):
            t = [d1,d2]   
            tmp = np.argmin(t)
            if tmp == 0:
                x1 = 0
                x2 = abs(t[0] - t[1])
            else:
                x1 = abs(t[0] - t[1])
                x2 = 0
            return x1, x2

        x1, x2 = find_size_dif(len(d1[0]),len(d2[0]))

        ### condition 1
        M = []
        for i in range(len(d1)):
            M.append(np.convolve(d1[i], np.ones((nei + x1,))/(nei + x1), mode="valid"))
        M1 = np.array(M).copy()
        ln_d1 = len(M1)

        ### condition 2
        M = []
        for i in range(len(d2)):
            M.append(np.convolve(d2[i], np.ones((nei + x2,))/(nei + x2), mode="valid"))
        M2 = np.array(M).copy()
        print("here",M1.shape, M2.shape, x1, x2)

        M3 = np.concatenate([M1,M2],axis=0)

        mi = np.min(M3)
        ma = np.max(M3)
        ma = ma - mi

        M1 = M1 - mi
        M1 = M1 / ma
        M2 = M2 - mi
        M2 = M2 / ma


        assert M1.shape == M2.shape
        print('length of dataset 2: ', ln_d1)
        print('\nCalculating correlation coeff. of data1(gene1) vs. data2(gene2) and etc')

        indx = list(range(ln_d1))
        labls = lst_g

        print('calculating similarity matrix based on the two groups')
        ln = len(d1)

        zz = cdist(np.array(M3), np.array(M3), metric = dist)
        zz = np.array(zz, dtype='float32')
        if dist == "correlation":
            nan = np.isnan(zz)
            zz[nan] = 0
        zz = zz / (np.max(zz)+0.000001)

        D1 = zz[:ln, :ln].copy()
        D1 = abs(D1 - 1)
        #if dist == "correlation":
        #    D1[nan[:ln, :ln]] = 0

        D2 = zz[ln:, ln:].copy()
        D2 = abs(D2 - 1)
        #if dist == "correlation":
        #    D2[nan[ln:, ln:]] = 0

        D21 = zz[:ln, ln:].copy()
        if corr_type == 'pos':
            D21 = abs(D21 - 1)
        #if dist == "correlation":
        #    D21[nan[:ln, ln:]] = 0
        D21[np.tril_indices(D21.shape[0], 0)] = 0.

        D12 = zz[:ln, ln:].copy()
        if corr_type == 'pos':
            D12 = abs(D12 - 1)
        if dist == "correlation": 
            D1[nan[:ln, :ln]] = 0
            D2[nan[ln:, ln:]] = 0
            D21[nan[:ln, ln:]] = 0
            D12[nan[:ln, ln:]] = 0
        D12 = D12.T
        D12[np.tril_indices(D12.shape[0], 0)] = 0.

        slk = D1 * D2 * D12 * D21

        v = np.diagonal(zz[:ln, ln:]).copy()
        if corr_type == "neg":
            v = v - (np.min(v)-0.000001)
            v = v / np.max(v)

        if corr_type == "pos":
            v = v - np.min(v)
            v = v / (np.max(v)+0.000001)
            v = abs(v-1)
        if dist == "correlation": 
            dnan = np.diagonal(nan[:ln, ln:]).copy()
            v[dnan] = 0


        slk[np.tril_indices(slk.shape[0], 0)] = 0.
        slk += slk.T

        slk = slk * v
        slk = slk * v.reshape((len(v),1))

        slk = slk - (np.min(slk) - 0.000001)
        slk = slk / np.max(slk)

        slk = np.array(slk, dtype='float32')
        adj_mtrx = np.array(adj_mtrx, dtype='uint8')

        print('size of simmatrix: ', slk.shape)
        print('size of adjmatrix: ', adj_mtrx.shape)


        #return slk, adj_mtrx, v, indx, labls, dels, new_d1, new_d2
        return slk, adj_mtrx, v, indx, labls, M1, M2


