from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from composeexample.core import views


urlpatterns = [
    url(r'^$', views.home, name='home'),
	url(r'^scrnaseq/documentation$', views.documentation, name='documentation'),
	url(r'^scrnaseq/publications$', views.publications, name='publications'),
	url(r'^scrnaseq/contact$', views.contact, name='contact'),

    url(r'^scrnaseq/signup/$', views.signup, name='signup'),
    url(r'^scrnaseq/logout/$', views.logout_view, name='logout'),
    url(r'^scrnaseq/login/$', views.login_view, name='login'),

    url(r'^scrnaseq/upload_old/$', views.upload_old, name='upload_old'),

    url(r'^scrnaseq/simple/$', views.simple_upload, name='simple_upload'),
    url(r'^scrnaseq/after_simple/$', views.after_simple_upload, name='after_simple_upload'),

    url(r'^scrnaseq/change_anndata/$', views.change_anndata, name='change_anndata'),
    url(r'^scrnaseq/change_start_over/$', views.change_start_over, name='change_start_over'),


    url(r'^scrnaseq/display_name/$', views.display_name, name='display_name'),
    url(r'^scrnaseq/start_over_draw/$', views.start_over_draw, name='start_over_draw'),

    url(r'^scrnaseq/cluster_pick/$', views.cluster_pick, name='cluster_pick'),
    url(r'^scrnaseq/cluster_start_over/$', views.cluster_start_over, name='cluster_start_over'),

    url(r'^scrnaseq/chac_params/$', views.chac_params, name='chac_params'),
    url(r'^scrnaseq/reload_chac_params/$', views.reload_chac_params, name='reload_chac_params'),

    url(r'^scrnaseq/loading/$', views.loading, name='loading'),

    url(r'^scrnaseq/networkplot/$', views.networkplot, name='networkplot'),

    url(r'^scrnaseq/run_test1/$', views.run_test1, name='run_test1'),
    url(r'^scrnaseq/run_test2/$', views.run_test2, name='run_test2'),
    url(r'^scrnaseq/run_test3/$', views.run_test3, name='run_test3'),
    url(r'^scrnaseq/run_test4/$', views.run_test4, name='run_test4'),
    url(r'^scrnaseq/run_test5/$', views.run_test5, name='run_test5'),


    url(r'^scrnaseq/run_chac/$', views.run_chac, name='run_chac'),

    url(r'^scrnaseq/results/$', views.results, name='results'),

    url(r'^admin/', admin.site.urls),
] 

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
