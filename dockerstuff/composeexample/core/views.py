from __future__ import absolute_import
from django.shortcuts import render, redirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from composeexample.core.models import Document, Post, G1, G2, Prcnt, Post1, Time
from composeexample.core.forms import DocumentForm, PostForm, Gr1, Gr2,Percent, PostForm1, Sort
import scanpy as sc
import matplotlib.pyplot as pl
from matplotlib import rcParams
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from django.template import loader
from django.template import RequestContext, Template, Context#, engines
from analytical.templatetags.clickmap import clickmap, ClickmapNode
from analytical.templatetags.analytical import TAG_LOCATIONS
from django.views.decorators.csrf import csrf_protect
from PIL import Image
import numpy as np
from scipy.spatial.distance import cdist
import pickle
import os
import glob
import pandas as pd
import networkx as nx
import subprocess
import imgkit
import zipfile
import datetime  
import shutil
sc.set_figure_params(dpi_save=200, frameon=False)
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate,logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from shutil import copyfile
import random
import uuid
import string as streng
from django.template.defaulttags import register


@register.filter
def get_range(value):
    return range(value)
	
@register.filter
def index(indexable, i):
    return indexable[i]

def get_timeline_coordinates(adata, wi, he, type1="draw_graph_fa"):
    """
    type can either be diffmap or draw_graph_fa
    """
    from scanpy.plotting._tools.scatterplots import _get_data_points
    V = _get_data_points(adata, basis=type1, projection="2d", components='1,2', img_key=None, library_id=None)
    return V[0][0]


def plot_new_fig(coords, path, lst_cords, wi, he, col="red"):
    pl.figure(figsize=(10, 10))
    #pl.figure(figsize=(800/100, 800/100), dpi=100)
    pl.scatter(coords[:,0],coords[:,1], color = "k") ## cahnge to grey
    for k,v in lst_cords.items():
        if k == "red":
            k1 = "#FFC20A"
        if k == "green":
            k1 = "#0C7BDC"
        pl.scatter(coords[v][:,0],coords[v][:,1], color = k1) 
    pl.yticks([])
    pl.xticks([])
    pl.xlim(0, wi)
    pl.ylim(0,he)
    #pl.axis("off")
    print("printing {}".format(path + "/scatter_template.png") )
    #print(lst_cords)
    pl.savefig(path + "/scatter_template.png",bbox_inches="tight", pad_inches=0, dpi=100)
    pl.close()

def plot_new_fig_pdf(coords, path, lst_cords, wi, he, col="red"):
    pl.figure(figsize=(10, 10))
    #pl.figure(figsize=(800/100, 800/100), dpi=100)
    pl.scatter(coords[:,0],coords[:,1], color = "k") ## cahnge to grey
    for k,v in lst_cords.items():
        if k == "red":
            k1 = "#FFC20A"
        if k == "green":
            k1 = "#0C7BDC"
        pl.scatter(coords[v][:,0],coords[v][:,1], color = k1) 
    inter = list( set( lst_cords["red"] ).intersection( set(lst_cords["green"]) ) )
    pl.scatter(coords[inter][:,0],coords[inter][:,1], color = "#1AFF1A")
    pl.yticks([])
    pl.xticks([])
    pl.xlim(0, wi)
    pl.ylim(0,he)
    #pl.axis("off")
    print("printing {}".format(path + "/scatter_template.png") )
    #print(lst_cords)
    pl.savefig(path + "/scatter_template.pdf",bbox_inches="tight", pad_inches=0, dpi=100)
    pl.close()


def plot_new_fig_fast(coords, path, lst_cords, width, height, im, col="red"):
    pl.figure(figsize=(10.4, 10.3))
    pl.imshow(im, origin='upper', extent=[0-8,width+8,0-8, height+8])
    for k,v in lst_cords.items():
        pl.scatter(coords[v][:,0],coords[v][:,1], color = k, s=50) 
    pl.yticks([])
    pl.xticks([])
    pl.xlim(-6, width+6)
    pl.ylim(-6,height+6)
    pl.axis("off")
    pl.savefig(path + "/scatter_template.png", bbox_inches="tight", pad_inches=0, dpi=100)
    pl.close()


"""
    pl.figure(figsize=(10, 10))
    #pl.figure(figsize=(800/100, 800/100), dpi=100)
    pl.scatter(coords[:,0],coords[:,1], color = "k") ## cahnge to grey
    for k,v in lst_cords.items():
        pl.scatter(coords[v][:,0],coords[v][:,1], color = k) 
    pl.yticks([])
    pl.xticks([])
    pl.xlim(0-6, wi+6)
    pl.ylim(0-6,he+6)
    #pl.axis("off")
    print("printing {}".format(path + "/scatter_template.png") )
    #print(lst_cords)
    pl.savefig(path + "/scatter_template.png",bbox_inches="tight", pad_inches=0, dpi=100)
    pl.close()
"""

"""
def plot_new_clustfig(coords, path, lst_cords, wi, he, col="red"):fff
    pl.figure(figsize=(10, 10))
    pl.scatter(coords[:,0],coords[:,1], color = "k") ## cahnge to grey
    for k,v in lst_cords.items():
        pl.scatter(coords[v][:,0],coords[v][:,1], color = k) 
    pl.yticks([])
    pl.xticks([])
    pl.xlim(0-6, wi+6)
    pl.ylim(0-6,he+6)
    #pl.axis("off")
    print("printing {}".format(path + "/scatter_template.png") )
    pl.savefig(path + "/scatter_template.png",bbox_inches="tight", pad_inches=0, dpi=100)
    pl.close()
"""


def randomstring():
    import uuid
    return uuid.uuid4().hex


def make_dir(dir_, folder = "static"):
    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    dr = dr.split("/")
    dr[-2] = folder
    dr[-1] = dir_
    dir_ = "/".join(dr)
    try:
        os.mkdir(dir_)
    except:
        print("dir already exists")
    os.chdir(dir_)
    print("going to:",os.getcwd())




def greedy_shortest_path(mtx, a, b, coor, srt, pct=0):

    dist = mtx[a][b]
    dist_diff = dist        
    mx = np.max(mtx) + 1

    if pct != 0:
        #extra = [list(range(len(mtx))) for i in range(len(mtx))]
        mtx1 = mtx.copy()
        np.fill_diagonal(mtx1, -np.max(mtx1)-1)
        prct = int(np.ceil((len(mtx[0])/100) * pct))


    #np.argpartition(slk[0], -4)[-4:]

    c = a
    coords = [a]
    if pct != 0:
        coords += list(np.argpartition(mtx1[c], prct)[:prct])
    now = False
    while now == False:
        tmp = mtx[c].copy()
        for i in range(len(tmp)):
            tmp1 = np.argmin(tmp)
            tmp_d = mtx[tmp1][b]

            if tmp_d < dist_diff:
                dist_diff = tmp_d
                c = tmp1
                coords.append(c)
                if pct != 0:
                    coords += list(np.argpartition(mtx1[c], prct)[:prct])
                break

            if tmp1 == b:
                now = True
                coords.append(tmp1)
                if pct != 0:
                    coords += list(np.argpartition(mtx1[c], prct)[:prct])
                break

            if tmp_d >= dist_diff:
                #print("now")
                tmp[tmp1] = mx

    if srt == "None":
        new_coords = []
        new_coords_set = set()
        for i in coords:
            if i not in new_coords_set:
                new_coords.append(i)
                new_coords_set.add(i)
        return new_coords
    else:
        return list(set(coords))

def the_drawer(coords, crds, pct, slk, path, tm, ps, srt, COL1):

    lst = {"red":[],"green":[]}
    cnter = -1
    for key,val in crds.items():
        cnter += 1
        if len(val) == 1:
            for i in range(len(val)):
                tmpp = greedy_shortest_path(slk, val[i], val[i], crds, srt, pct = pct[cnter])
                if srt != "None":
                    lst[key] += list(ps.intersection(tmpp))
                if srt == "None":
                    lst[key] += tmpp
        if len(val) > 1:
            for i in range(len(val)-1):
                tmpp = greedy_shortest_path(slk, val[i], val[i+1], crds, srt, pct = pct[cnter])
                if srt != "None":
                    lst[key] += list(ps.intersection(tmpp))
                if srt == "None":
                    lst[key] += tmpp

    for kk,vv in lst.items():               ####### NEW
        lst[kk] = list(set(vv))

    with open(path + "/objects.pkl", "wb") as f: 
        pickle.dump(lst,f)
    
    
    colz = ['#00000'] * len(coords) ###### NEW
    for kk,vv in lst.items():
        if kk == "red":
            for i in vv:
                colz[i] = "#FFC20A"#"#FF0000"
        if kk == "green":
            for i in vv:
                if colz[i] == "#FFC20A":#"#FF0000":
                    colz[i] = "#1AFF1A"#"#008080"
                else:
                    colz[i] = "#0C7BDC"#"#00FF00"
    
    #colz = ['#00000'] * len(coords)
    #for kk,vv in lst.items():
    #    if kk == "red":
    #        for i in vv:
    #            colz[i] = "#FF0000"
    #    if kk == "green":
    #        for i in vv:
    #            colz[i] = "#00FF00"

    with open(path + "/col_list.pkl","wb") as f:
        pickle.dump(colz, f)


    imgnm = glob.glob1(path, "*_the_pic.png")[0]
    imgx = tm + "/" + imgnm
    tmp1x = '{% load static %}<img src="{% static "'
    tmp2x = '" %}" id="non"/>'
    htmlx = Template(tmp1x + imgx + tmp2x)    
    return htmlx, COL1


def get_adata_timelines_func(cl1, adata, time, clst):

    tmp = adata.obs[[time, clst]].values
    tmp = np.concatenate((tmp,np.arange(len(tmp)).reshape((len(tmp),1))),axis=-1)
    print("added temporary index",tmp.shape)
    print("removing inf time values",tmp.shape)
    tmp = tmp[tmp[:,0] != np.inf]

    print("extracting clusters")
    set1 = []
    set1_indx = []
    count = {}
    for i in cl1:
        set1 += list(tmp[:,0][tmp[:,1] == i])   
        set1_indx += list(tmp[:,2][tmp[:,1] == i])
        count[i] = len(set1_indx)

    for k,v in count.items():
        print(k,v)

    if len(set1) == 0:
        print("the all cluster object were NaN")
        return [], []
    
    t = list(zip(set1,set1_indx))
    t.sort()
    set1, set1_indx = zip(*t)

    print("length of set1:", len(set1), len(set1_indx))   

    return list(set1), list(set1_indx), count


def work_with_names(n1,n2):
    
    if n2 == 'X_draw_graph_fa' or n2 == 'X_draw_graph_fr':
        n2x = n2[2:-3]
    else:
        n2x =n2[2:]
    st = "sc.pl.{}(adata, color=['{}'], legend_loc='on data', save='_the_pic.png', show=False, legend_fontsize=8)".format(n2x,n1,)
    #st = "sc.pl.{}(adata, color=['{}'], save='_the_pic.png', show=False)".format(n2x,n1,)
    #exec(st)
    
    return n1, n2[2:], st


def work_with_names_pdf(n1,n2):
    
    if n2 == 'X_draw_graph_fa':
        n2x = n2[2:-3]
    else:
        n2x =n2[2:]
    st = "sc.pl.{}(adata, color=['{}'], legend_loc='on data', save='_the_pic.pdf', show=False, legend_fontsize=8)".format(n2x,n1,)
    #exec(st)
    #st = "sc.pl.{}(adata, color=['{}'], save='_the_pic.pdf', show=False)".format(n2x,n1,)

    return n1, n2[2:], st



def mini_function(path):
    with open(path+"/objects.pkl","rb") as f:               
        objs = pickle.load(f)					
    ob = [[k,str(len(v))] for k,v in objs.items() if len(v) != 0]	
    if ob == []:							
        return "You need to select some points/cells in the scatter plot"
    else:
        return ob



"""
def make_csv_for_CHAC(mat, col_n, entrez, gene_symbols, name):
    
    df = pd.DataFrame(mat,index=None, columns=col_n)
    df.insert(loc=0, column='Entrez', value=entrez)
    df.insert(loc=0, column='Clusters', value=gene_symbols)
    df.insert(loc=0, column='ID', value=list(range(len(mat))))
    
    df.to_csv(name,sep="\t", index=False)
"""



def receive_gene_info(info, upload, path1):

    def get_info(nm, df, ge, g):
        s = set(df[nm])
        q = list(df[nm])
        b = []
        n = []
        indx = []
        not_entID = []
        for i in range(len(ge)):
            if ge[i] in s:
                try:
                    ind = q.index(ge[i])
                    if str(int(df["NCBI gene ID"][ind])) in g:
                        b.append(int(df["NCBI gene ID"][ind]))
                        n.append(df["Gene name"][ind])
                        indx.append(i)
                    else:
                        not_entID.append(i)
                except:
                    not_entID.append(i)
            else:
                not_entID.append(i)
        return b,n,indx, not_entID


    def count_doubles(b1):
        con1 = {}
        for i,j in enumerate(b1):
            if b1.count(j) > 1:
                print(b1.count(j), j)
                if j in con1:
                    con1[j].append(i)
                else:
                    con1[j] = [i]
        return con1

    def sort_doubles(b,n,indx,con):
        b1 = []
        n1 = []
        indx1 = []
        for i in range(len(b)):
            if b[i] in con:
                if b[i] not in b1:
                    b1.append(b[i])
                    n1.append(n[i])
                    indx1.append(indx[i])
                else:
                    continue
            else:
                b1.append(b[i])
                n1.append(n[i])
                indx1.append(indx[i])
        print("****************",len(b), len(b1))
        return b1,n1,indx1

    path = path1.split("/")
    path = "/".join(path[:-1])

    #####
    df = pd.read_csv(path + "/mart_human.txt")    
    upl = upload#[]
    #with open(path1 + "/"+upload,"r") as f:
    #    for i in f:
    #        upl.append(i.strip())
    tmp = ""
    b = []
    n = []
    indx = []
    not_entID = []
    #g = nx.read_edgelist(path=path + "/networkBIOGRID-ALL-3.5.171.tab2", delimiter="\t")
    #with open(path+"/graph_n_entIDs.pkl", "rb") as f:
    #    g,q = pickle.load(f)

    zz = glob.glob1(path1, "*.tab2")
    if len(zz)!= 0:
        #df = pd.read_csv(path + "/mart_human.txt")
        #q = set(list(df["NCBI gene ID"]))
        with open(path+"/graph_n_entIDs.pkl", "rb") as f:
            g,q = pickle.load(f)
        g = nx.read_edgelist(path1+"/"+zz[0])
        g = set(list(g))
    else: 
        print("you have not uploaded a network")
        #path_netw = folder + "/" + "networkBIOGRID-ALL-3.5.171.tab2"
        with open(path+"/graph_n_entIDs.pkl", "rb") as f:
            g,q = pickle.load(f)
        #df = pd.read_csv(path + "/mart_human.txt")



    #g = set(list(g))
    #q = list(df["NCBI gene ID"])
    #####
    if info == "hm_entrez_ID":         
        for i,j in enumerate(upl):
            if str(j) in g:
                try:
                    t = int(j)
                    ind = q.index(t) 
                    nn = df["Gene name"][ind]
                    b.append(t)
                    n.append(nn)
                    indx.append(i)
                except:
                    not_entID.append(i)
            else:
                not_entID.append(i)
        print(len(indx) , len(not_entID),len(indx) + len(not_entID),len(upl))
        assert len(indx) + len(not_entID) == len(upl)

    #####
    if info == "hm_gene_symbol":
        tmp = "Gene name"

    #####    
    if info == "mm_gene_symbol":
        with open(path+"/mouse_symb_to_hm_entrez.pkl","rb") as f:
            p = pickle.load(f)
        for i,j in enumerate(upl):
            if j in p:
                try:
                    if str(p[j]) in g:
                        ind = q.index(p[j])
                        nn = df["Gene name"][ind]
                        b.append(p[j])
                        n.append(nn)
                        indx.append(i)
                    else:
                        not_entID.append(i)
                except:
                    not_entID.append(i)
            else:
                not_entID.append(i)
        print(len(indx) , len(not_entID),len(indx) + len(not_entID),len(upl))
        assert len(indx) + len(not_entID) == len(upl)
    #####   
    if info == "hm_ensambl_ID":
        cc =  []
        for i in upl:
            cc.append(i.split(".")[0])
        upl = cc
        tmp = "Gene stable ID"

    #####
    if tmp != "":
        b, n, indx, not_entID = get_info(tmp, df, upl, g)
        print(len(indx) , len(not_entID),len(indx) + len(not_entID),len(upl))
        assert len(indx) + len(not_entID) == len(upl)
        con = count_doubles(b)
        big_con = {}
        for kk,vv in con.items():
            big_con[n[vv[0]]] = []
            for qq in vv:
                big_con[n[vv[0]]].append(indx[qq])
                print("GENE_x:",n[qq])
                print("GENE_x:",indx[qq])
        print(big_con)
        b,n,indx = sort_doubles(b,n,indx,con)
        return b, n, indx, not_entID, con, upl, big_con

    if tmp == "":
        con = count_doubles(b)
        #for kk,vv in con.items():
        #    for qq in vv:
        #        print("GENE:",n[qq])
        #        print("GENE:",indx[qq])
        big_con = {}
        for kk,vv in con.items():
            big_con[n[vv[0]]] = []
            for qq in vv:
                big_con[n[vv[0]]].append(indx[qq])
                print("GENE_x:",n[qq])
                print("GENE_x:",indx[qq])
        print(big_con)
        b,n,indx = sort_doubles(b,n,indx,con)
        print(len(indx) , len(not_entID),len(indx) + len(not_entID),len(upl))
        print("b",b)
        print("n",n)
        print("indx", indx)
        print("not_entID",not_entID)
        print("con",con)
        return b, n, indx, not_entID, con, upl, big_con



def make_csv_for_CHAC(path, mat, col_n, entrez, gene_symbols, name):

    df = pd.DataFrame(mat,index=None, columns=col_n)
    df.insert(loc=0, column='Entrez', value=entrez)
    df.insert(loc=0, column='Gene_symb', value=gene_symbols)
    df.insert(loc=0, column='ID', value=list(range(len(mat))))
    df.to_csv(path + "/" +name+".csv",sep="\t", index=False)


def sorter(a,b):
    c = list(zip(list(a),list(b)))
    c.sort()
    a1,b1 = zip(*c)
    return list(a1),list(b1)


def prep_data(path, adata, genes, group, srt, entrez, gene_symbols, name, dob, big_con):
    print("MAKING FILES, MAKING FILES!!!!")
    mtrx = adata.X.T ## now it is genes * cells
    print("****************",dob)
    if dob == "Take average_NaN":
        print(big_con)
        new_array = 0
        for k,v in big_con.items():
            for i in v:
                if i != np.min(v):
                    new_array += mtrx[i]
            mtrx[np.min(v)] += new_array
            mtrx[np.min(v)] = mtrx[np.min(v)]/len(v)
            

    mtrx = mtrx[genes]
    if srt != "None":
        pseudo = adata.obs[srt]
        pseudo = pseudo[group]
        ps, gr = sorter(pseudo, group)
        mtrx = mtrx[:,gr]
    if srt == "None":
        mtrx = mtrx[:,group]
        #ps = list(range(len(group)))
        ps = group
    make_csv_for_CHAC(path, mtrx, ps, entrez, gene_symbols, name)

def get_goeaobj(geneids_pop, taxid, method="fdr_bh"):
    taxid=9606
    """Load: ontologies, associations, and population geneids."""
    obo_dag = get_godag()
    assoc_geneid2gos = get_assoc_ncbi_taxids([taxid])
    goeaobj = GOEnrichmentStudy(
        geneids_pop,
        assoc_geneid2gos,
        obo_dag,
        propagate_counts=False,
        alpha=0.05,
        methods=[method])
     # obo_dag is also found in goeaobj.obo_dag
    return goeaobj



def delete_folders(lst, dr):
    
    def time_func(my_dt_ob):
        return [my_dt_ob.year, my_dt_ob.month, my_dt_ob.day, my_dt_ob.hour, my_dt_ob.minute, my_dt_ob.second]
    
    my_dt_ob = datetime.datetime.now()
    date_list = np.array(time_func(my_dt_ob))
    nr = 0
    for i in lst:
        if len(i) == 32:
            st=os.stat(dr+"/"+i)    
            mtime=st.st_mtime
            dif = date_list - np.array(time_func(datetime.datetime.fromtimestamp(mtime)))
            #print(i, dif, dif[2]*24+dif[3])
            if dif[2]*24+dif[3] > 21:
                print(i)            
                nr += 1
                shutil.rmtree(dr+"/"+i)
    print(nr,"old user directories were deleted")

#########################################################################
@login_required(login_url='home')
@csrf_protect
def results(request):

    print("now in loading")
       	
    dr = os.path.dirname(os.path.realpath(__file__))
    folder = "/".join(dr.split("/")[:-2]) + "/static"
    
    #if len(os.getcwd().split("/")[-1]) == 32:
    if 1 == 1:
        tm = request.session[request.user.username]
        path = folder + "/" + tm

                
        #with open(rp+"/graph_pics.pkl","rb") as f:
        #    gs = pickle.load(f)
        #with open(rp+"/conf_pics.pkl","rb") as f:
        #    cf = pickle.load(f)

        #tsvs = glob.glob("*.tsv")
        #graphs = glob.glob("*graph.png")
        #confs = glob.glob("*conf.png")
        hda5_file = glob.glob1(path, "*.h5ad")
        onepng = glob.glob1(path, "*the_pic.png")
        all_pdfs = glob.glob1(path, "*.pdf")
        all_pkls = glob.glob1(path, "*.pkl")
        tsvs = glob.glob1(path, "*.tsv")
        csvs = glob.glob1(path, "*.csv")
        graphs = glob.glob1(path, "*graph.png")
        confs = glob.glob1(path, "*conf.png")
        
        tsvs.sort()
        graphs.sort()
        confs.sort()
        #tedst = []
        #for links in confs:
        #    tedst.append("/static/" + tm + "/" + links)
        #tedst = confs[0]
        #tedst = "/static/" + tm + "/" + tedst
        from os.path import basename
        all_downl = tsvs + all_pdfs + onepng + all_pkls + hda5_file + csvs
        all_downl = [path + "/" + navne for navne in all_downl]
        zf = zipfile.ZipFile(path + "/" 'all_results.zip', mode='w')
        for lnk in all_downl:
            zf.write(lnk, basename(lnk))
        zf.close()
        tedst = "/static/" + tm + "/" + 'all_results.zip'
        print("*******",tedst,"*******")

        all = []
        netw = glob.glob1(path, "*symbol.csv")
        netw.sort()
        for iii in netw:
            l = []
            fh = open(path + "/" + iii, 'r')
            for line in fh:
                l.append(line.strip().split('\t'))
                # print(line)
            all.append(l)

        def datamaker(l, sortage):
            import numpy as np
            a = np.asarray(l)
            b = np.delete(a, 2, axis=1)
            c = np.unique(b)
            # print(len(c))
            idx = []
            for i in range(len(c)):
                idx.append([c[i], i])
            idxx = np.asarray(idx)

            with open(path + "/" + str(sortage) + "nods.txt", 'w') as f:
                for i in range(len(idxx)):
                    # print(idxx[i][1])
                    print(str('{ id: ') + str(idxx[i][1]) + str(', label: "') + str(idxx[i][0]) + str('" },'), end =" ",file=f)
                f.close()

            # print(len(b))
            for i in range(len(b)):
                for j in range(len(idxx)):
                    # print(b[i][0])
                    if b[i][0] == idxx[j][0]:
                        # print(idxx[j][1])
                        b[i][0] = idxx[j][1]
                    if b[i][1] == idxx[j][0]:
                        # print(idxx[j][1])
                        b[i][1] = idxx[j][1]

            with open(path + "/" + str(sortage) + "edges.txt", 'w') as f1:
                for i in range(len(b)):
                    # print(b[i])
                    print(str('{ from: ') + str(b[i][0]) + str(', to: ') + str(b[i][1]) + str(' },'), end =" ",file=f1)
                f1.close()

            noddd=[]
            edgeee=[]
            ff = open(path+"/"+str(sortage) + "nods.txt", "r")
            for y in ff:
                print(y)
                noddd.append(y)
            fff = open(path+"/"+str(sortage) + "edges.txt", "r")
            for x in fff:
                print(x)
                edgeee.append(x)
            print("add the text files to the ZIP file")
            zf = zipfile.ZipFile(path + "/" 'all_results.zip', mode='a')
            zf.write(path+"/"+str(sortage) + "nods.txt")
            zf.write(path+"/"+str(sortage) + "edges.txt")
            zf.close()
            return (noddd,edgeee)

        lastn1 = []
        laste2 = []
        for i in range(len(all)):
            sortage = i
            datamaker(all[i], sortage)
            lastn1.append(datamaker(all[i], sortage)[0])
            laste2.append(datamaker(all[i], sortage)[1])
        for i in range(len(lastn1)):
            lastn1[i] = lastn1[i][0]
            l = len(lastn1[i]) - 2
            lastn1[i] = lastn1[i][:l]
        for i in range(len(laste2)):
            laste2[i] = laste2[i][0]
            l = len(laste2[i]) - 2
            laste2[i] = laste2[i][:l]

        with open(path + "/interactive_graphs.pkl", "wb") as pq:
            pickle.dump([lastn1, laste2],pq)


        print(tsvs)
        print(graphs)
        print(confs)
        imgkitoptions = {"format": "png"}
        sizes = []
        for gra in graphs:
            tnp = gra.split("_")
            #sizes.append("size: {} - score: {}".format(tnp[-3], tnp[-2][:tnp[-2].index(".")+3]))
            sizes.append("size: {} - q-value: {}".format(tnp[-3], tnp[-2]))

        #qsl = graphs = glob.glob1(path, "p_and_q_values_lines.pkl")

        #if len(qsl) == 1:
        with open(path + "/p_and_q_values_lines.pkl", "rb") as pq:
            pvl, qvl = pickle.load(pq)
        if len(qvl) > 0:
            qvl_out = ["q-value: {}".format(qvx) for qvx in qvl]
        if len(qvl) == 0:
            qvl_out = [" " for qvx in sizes]

        gs = []
        for j,i in enumerate(graphs):
            imgx = tm + "/" +i
            tmp1x = '{% load static %}<img src="{% static "'
            tmp2x = '" %}" '+'id="'+'gs{}" class="pop thump"/>'.format(j)
            gs.append(Template(tmp1x + imgx + tmp2x).render(Context(request)))
    
        cf = []
        for j,i in enumerate(confs):
            imgx = tm + "/" +i
            tmp1x = '{% load static %}<img src="{% static "'
            tmp2x = '" %}" '+'id="'+'cf{}"'+' class="pop thump"/>'.format(j)
            cf.append(Template(tmp1x + imgx + tmp2x).render(Context(request)))
        hed = 15
        gotrs = []
        for j in range(len(confs)):
            tmp1 = [str(j) == ii[0] for ii in tsvs]
            col_names = ["# GO", "NS", "name", "ratio_in_study", "p_fdr_bh", "study_items"]

            if sum(tmp1) == 1:
                argm = np.argmax(tmp1)
                tmp = pd.read_csv(path + "/" + tsvs[argm],sep="\t")
                tmp = tmp.head(hed)[col_names]
                if len(tmp) < hed:
                    ex = hed - len(tmp)
                    tmp = tmp.append(pd.DataFrame([[" - " for i in range(len(col_names))] for ii in range(ex)], columns=list(col_names)), ignore_index=True)
                    imgkit.from_string(tmp.to_html(), path + "/" + tsvs[argm][:-4]+"_table_.png", options=imgkitoptions)
                else:
                    imgkit.from_string(tmp.to_html(), path + "/" + tsvs[argm][:-4]+"_table_.png", options=imgkitoptions)
                #gotrs.append(Template(tmp.to_html()).render(Context(request)))
            else:
                tmp1 = pd.DataFrame([[" - " for i in range(len(col_names))] for ii in range(hed)],columns=list(col_names))
                imgkit.from_string(tmp1.to_html(), path + "/" + str(j)+"_table_.png", options=imgkitoptions)
                #gotrs.append(Template(tmp1.to_html()).render(Context(request)))

        table_img = glob.glob1(path, "*_table_.png")
        table_img.sort()
        print(table_img)
        for j,i in enumerate(table_img):
            imgx = tm + "/" +i
            tmp1x = '{% load static %}<img src="{% static "'
            tmp2x = '" %}" '+'id="'+'tb{}" class="pop thump"/>'.format(j)
            gotrs.append(Template(tmp1x + imgx + tmp2x).render(Context(request)) )

        #cf = zip(list(range(len(confs))), cf)
        rang = zip(sizes, gs)
        #"gs":gs, 
        return render(request, 'core/results.html', {"cf":cf, "gotrs":gotrs, "sizes":sizes, "rang":rang, "gs":gs, "tedst":tedst, "qvl":qvl_out, "lastn1":lastn1,"laste2":laste2})
    else:
        return render(request, 'core/results.html')
        #return redirect('chac_params')



##tmp.append(pd.DataFrame([["-" for i in range(len(tmp.columns))]], columns=list(tmp.columns)), ignore_index=True)
@login_required(login_url='home')
@csrf_protect
def run_chac(request):

    def give_corr_type(hej):
        if hej == 'Far away/negative corr.':
            out = "neg"
        if hej == "Close/positive corr.":
            out = "pos"
        return out

    def running_th_chac(path, path_netw, path_run, tm):
        try:
            f1 = glob.glob1(path, "*red.csv")[0]
        except:
            f1 = []
            tel = 0
        try:
            f2 = glob.glob1(path, "*green.csv")[0]
        except:
            f2 = []
            tel = 1

        print(f1,f2)
        with open(path+"/objects.pkl","rb") as f:
            objcts = pickle.load(f)
        with open(path+"/chac_par.pkl","rb") as f:
            inp = pickle.load(f)
        tp_cor = give_corr_type(inp[2])
        print("the one used:",inp)
        with open(path+"/graph_pics.pkl","wb") as f:
            pickle.dump([], f)
        with open(path+"/conf_pics.pkl","wb") as f:
            pickle.dump([], f)


        #if len(objcts) == 2 and len(objcts):# == len(files):
        if f1 != [] and f2 != []:
            print("111111111111111111111111111111111111111111111111111")
            f1 = path + "/" + f1
            f2 = path + "/" + f2
            runz = "python {} --group1 {} --group2 {} --reps1 1 --reps2 1 --path_edge_list {} --link {} -l {} -K {} --corr_type {} --dist {} --name {} --movav {} --umap {} --umap_dim {}".format(
            path_run, f1, f2, path_netw, inp[0].lower(),int(inp[3]) , int(inp[4]), tp_cor, inp[1].lower(), tm + "_chac", int(inp[5]), inp[-2], int(inp[-1]))

        elif f1 != [] and f2 == []:
            print("2222222222222222222222222222222222222222222222222222")
            f1 = path + "/" + f1
            runz = "python {} --group1 {} --reps1 1 --reps2 1 --path_edge_list {} --link {} -l {} -K {} --corr_type {} --dist {} --name {} --movav {} --umap {} --umap_dim {}".format(
            path_run, f1, path_netw, inp[0].lower(),int(inp[3]) , int(inp[4]), tp_cor, inp[1].lower(), tm + "_chac", int(inp[5]), inp[-2], int(inp[-1]))

        elif f1 == [] and f2 != []:
            print("333333333333333333333333333333333333333333333333")
            f2 = path + "/" + f2
            runz = "python {} --group1 {} --reps1 1 --reps2 1 --path_edge_list {} --link {} -l {} -K {} --corr_type {} --dist {} --name {} --movav {} --umap {} --umap_dim {}".format(
            path_run, f2, path_netw, inp[0].lower(),int(inp[3]) , int(inp[4]), tp_cor, inp[1].lower(), tm + "_chac", int(inp[5]), inp[-2], int(inp[-1]))

        test = subprocess.Popen(runz, shell=True)#stdout=subprocess.PIPE)
        output = test.communicate()[0]
        print("----",output)
        print("all done now")

    def init_params(path):
        
        with open(path + "/chac_par.pkl", "rb") as f:
            inp = pickle.load(f)
            print("***",inp)
        with open(path + "/genes_in_set.pkl", "rb") as f:
            b, n, indx, not_entID, con, navn, big_con = pickle.load(f) 
        
        ad = glob.glob1(path, "*.h5ad")[0]
        adata = sc.read(path + "/" + ad)
        tm1 = path.split("/")
        tm1 = tm1[-1]
        with open(path+"/objects.pkl","rb") as f:
            objcts = pickle.load(f)
        with open(path+"/all_nms.pkl","rb") as f:
            nmq,nmq1,srt = pickle.load(f)
        for k1,v1 in objcts.items():
            if v1 != []:
                prep_data(path, adata, indx, v1, srt, b, n, tm1 + k1, inp[-3], big_con) #big_con
        #with open(path + "/chac_par.pkl", "wb") as f:
        #    pickle.dump(inp,f)
    


    print("now in run_chac")

    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    folder = "/".join(dr.split("/")[:-2]) + "/static"
    
    #if len(os.getcwd().split("/")[-1]) == 32:
    if 1 == 1:
        tm = request.session[request.user.username]
        path = folder + "/" + tm
        zz = glob.glob1(path, "*.tab2")
        if len(zz)!= 0:
            print("You have uploaded a network")
            print(zz[0])
            print(path)
            path_netw = path + "/" + zz[0]
            print(path_netw)
        else:
            print("you have not uploaded a network")
            path_netw = folder + "/" + "networkBIOGRID-ALL-3.5.171.tab2"
        #path_netw = folder + "/" + "networkBIOGRID-ALL-3.5.171.tab2"
        path_run = folder + "/" + "chac/main2.py"
        if len(glob.glob1(path,"all_clustering_data.pkl")) == 0:
            init_params(path)
        running_th_chac(path, path_netw, path_run, tm)

        return render(request, 'core/run_chac.html')

    else:
        return render(request, 'core/run_chac.html')
        #return redirect('chac_params')
@login_required(login_url='home')
@csrf_protect
def loading(request):

    print("now in loading")

    if 'stuff[x]' in request.POST:
        dir_ = request.POST['stuff[x]']
        make_dir(dir_)
        path = os.getcwd()
        print("-----", path, "------")
        tm = path.split("/")
        tm = tm[-1]
        return render(request, 'core/loading.html')
        #return redirect('chac_params')
    else:
        return render(request, 'core/loading.html')
        #return redirect('chac_params')

@login_required(login_url='home')
@csrf_protect
def reload_chac_params(request):
    
    ## maybe use popen for this
    
    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    folder = "/".join(dr.split("/")[:-2]) + "/static"
    tm = request.session[request.user.username]
    path = folder + "/" + tm
    
    
    def make_csv_files(path, inp, adata):
        print("YYYYYYY", os.getcwd())
        navn = str(inp)
        #FF = adata.var.inp[1]
        FF = eval("list(adata.var.{})".format(inp[1]))
        b, n, indx, not_entID, con, upl, big_con = receive_gene_info(inp[0], list(FF), path)
        with open(path + "/genes_in_set.pkl", "wb") as f:
            pickle.dump([b, n, indx, not_entID, con, navn, big_con],f)
        ln_upl = len(upl)
        ln_indx = len(indx)
        ln_nindx = len(not_entID)
        ln_fil = ln_indx + ln_nindx + len(con)
        print("XCXCX",ln_indx, ln_nindx,ln_fil)
        if len(con) != 0:
            print(big_con)
    
        return navn, b, n, indx, not_entID, con, upl, big_con, ln_fil, ln_indx, ln_nindx, ln_upl, FF
    
    
    FFf = glob.glob1(path, "*.h5ad")[0]
    adata = sc.read(path + "/" + FFf)

    ## putting the adata.var cols into a dict    
    try:
        print("The adata object has an index")
        name_dict = {"index":set(adata.var.index)}
    except:
        print("Does not have an index")
        name_dict = {}
    for i in adata.var.columns:
        name_dict[i] = set(adata.var[i])    
    
    
    print("now in reload_chac_params")
    types = ["hm_entrez_ID", "hm_gene_symbol", "mm_gene_symbol", "hm_ensambl_ID"]
    col_names = ["NCBI gene ID", "Gene name", "mm_gene_symbol", "Gene stable ID"]
    
    #### need to check with biogrid
    #### the entrex IDs in df are as flaots --> change.
    ### loading the graph
    zz = glob.glob1(path, "*.tab2")
    if len(zz)!= 0:
        #df = pd.read_csv(folder + "/mart_human.txt")
        #q = set(list(df["NCBI gene ID"]))
        with open(folder+"/graph_n_entIDs.pkl", "rb") as f:
            g,q = pickle.load(f)
        g = nx.read_edgelist(path+"/"+zz[0])
        g = set(list(g))
    else:
        print("you have not uploaded a network")
        #path_netw = folder + "/" + "networkBIOGRID-ALL-3.5.171.tab2"
        with open(folder+"/graph_n_entIDs.pkl", "rb") as f:
            g,q = pickle.load(f)
        #df = pd.read_csv(folder + "/mart_human.txt")

    
    ## Dict for comparing input gene instances with database
    compare_db = {}
    ## finding mouse genes
    with open(folder+"/mouse_symb_to_hm_entrez.pkl","rb") as f:
        p = pickle.load(f)
    ## finding otehr genes
    df = pd.read_csv(folder + "/mart_human.txt")
    
    
    
    #DF = df[["Gene name",  "NCBI gene ID",   "Gene stable ID"]][np.isfinite(df["NCBI gene ID"])]
    valid_gene_indx = []
    for i,j in enumerate( df["NCBI gene ID"] ):
        try:
            if str(int(j)) in g:
                valid_gene_indx.append(i)
        except:
            pass
    
    
    
    for i in col_names:
        if i not in types:
            compare_db[i] = set(df[i][valid_gene_indx])
        else:
            mouse = []
            for k,v in p.items():
                if str(v) in g:
                    mouse.append(k)
            compare_db[i] = set(mouse)

    ent_id = set()    
    for i in list(df["NCBI gene ID"]):
        try:
            ent_id.add(str(int(i)))
        except:
            pass
    compare_db["NCBI gene ID"] = ent_id
    print("*****************",len(ent_id))

    
    best_fit = {}
    len_ = 0
    for k,v in name_dict.items():           # From uploaded file
        for kk,vv in compare_db.items():    # From database
            
            #tmp_intersect = v.intersection(vv)
            tmp_intersect_len = len(v.intersection(vv))#len(tmp_intersect)
            print(k, kk, tmp_intersect_len)
            
            if tmp_intersect_len > len_:
                #len_biogrid = 0
                #for i in tmp_intersect:
                #    if kk != "mm_gene_symbol":
                #        if str(int( q[df[kk][df[kk] == i].index[0]] )) in g:
                #            len_biogrid += 1
                #    else:
                #        if str(p[i]) in g:
                #            len_biogrid += 1
                        
                len_ = tmp_intersect_len  #len_biogrid  #tmp_intersect_len
                best_fit[kk+"&&#&"+k] = len_
    
    final_type = ""
    len_ = 0
    for k,v in best_fit.items():
        if v > len_:
            final_type = k.split("&&#&")

    final_type[0] = types[col_names.index(final_type[0])]
    print("***************", final_type, "**************" )
    
    
    with open(path+"/chac_par_gene_type.pkl","wb") as f:
        pickle.dump(final_type, f)
    #with open(path+"/chac_par_gene_type.pkl","rb") as f:
    #    inp2 = pickle.load(f)
    
    
    navn, b, n, indx, not_entID, con, upl, big_con, ln_fil, ln_indx, ln_nindx, ln_upl, FF = make_csv_files(path, final_type, adata) 

    return redirect('chac_params')


@login_required(login_url='home')
@csrf_protect
def chac_params(request):

    print("now chac_params")
    
    
    def remove_unwanted_data(path):
        all_pdfs = glob.glob1(path, "*.pdf")
        tsvs = glob.glob1(path, "*.tsv")
        graphs = glob.glob1(path, "*graph.png")
        confs = glob.glob1(path, "*conf.png")
        tables = glob.glob1(path, "*table_.png")
        csv_symb = glob.glob1(path, "*symbol.csv")
        csv_ent = glob.glob1(path, "*entrez.csv")
        #clst_inp = glob.glob1(path, "all_clustering_data.pkl")
        all_data = tsvs + all_pdfs + graphs + confs + tables + csv_symb + csv_ent + glob.glob1(path, "*.zip")
        for i in all_data:
            os.remove(path + "/" + i)
        
        
    def prep_params(path):
        
        with open(path+"/params.pkl","rb") as f:
            offsets, true_height, coords, width, height, im = pickle.load(f)
        with open(path+"/color.pkl","rb") as f:
            col = pickle.load(f)
        with open(path+"/objects.pkl","rb") as f:
            objcts = pickle.load(f)
        with open(path+"/all_nms.pkl","rb") as f:
            nm,nm1,srt = pickle.load(f)

        plot_new_fig_pdf(coords, path, objcts, width, height, col)
        #nm,nm1, st = work_with_names_pdf(nm,nm1)
        #exec(st)


        FFf = glob.glob1(path, "*.h5ad")[0]
        adata = sc.read(path + "/" + FFf)
        upl1 = adata.var.head(5)
        cols = ["index"] + list(upl1.columns)
        upl1 = Template(upl1.to_html())

        form = PostForm()

        with open(path+"/genes_in_set.pkl","rb") as f:
            b, n, indx, not_entID, con, navn, big_con = pickle.load(f)
        assert len(b) == len(n) == len(indx)
        ln_indx = len(indx)
        ln_nindx = len(not_entID)
        ln_fil = ln_indx + ln_nindx + len(con)
        
        with open(path+"/chac_par.pkl","rb") as f:
            inp = pickle.load(f)
    
        return offsets, true_height, coords, width, height, im, col, objcts, nm,nm1,srt, cols, upl1, form, b, n, indx, not_entID, con, navn, big_con, ln_fil, ln_indx, ln_nindx, inp
    
    
    
    def make_csv_files(path, inp, adata):
        print("YYYYYYY", os.getcwd())
        navn = str(inp)
        #FF = adata.var.inp[1]
        FF = eval("list(adata.var.{})".format(inp[1]))
        b, n, indx, not_entID, con, upl, big_con = receive_gene_info(inp[0], list(FF), path)
        with open(path + "/genes_in_set.pkl", "wb") as f:
            pickle.dump([b, n, indx, not_entID, con, navn, big_con],f)
        ln_upl = len(upl)
        #print(n)
        ln_indx = len(indx)
        ln_nindx = len(not_entID)
        ln_fil = ln_indx + ln_nindx + len(con)
        print("XCXCX",ln_indx, ln_nindx,ln_fil)
        if len(con) != 0:
            print(big_con)
    
        return navn, b, n, indx, not_entID, con, upl, big_con, ln_fil, ln_indx, ln_nindx, ln_upl, FF
    
    
    def init_params(path,inp, adata, indx, srt, b, n, big_con):
        ad = glob.glob1(path, "*.h5ad")[0]
        adata = sc.read(path + "/" + ad)
        tm1 = path.split("/")
        tm1 = tm1[-1]
        with open(path+"/objects.pkl","rb") as f:
            objcts = pickle.load(f)
        with open(path+"/all_nms.pkl","rb") as f:
            nmq,nmq1,srt = pickle.load(f)
        for k1,v1 in objcts.items():
            if v1 != []:
                prep_data(path, adata, indx, v1, srt, b, n, tm1 + k1, inp[-3], big_con) 
        with open(path + "/chac_par.pkl", "wb") as f:
            pickle.dump(inp,f)
    
    
    
    #FF = glob.glob("*.txt")
    #if len(FF) > 0:
    #    txt = FF[0]
    #if len(FF) == 0:
    #    txt = "No file has been uploaded"dss

    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    folder = "/".join(dr.split("/")[:-2]) + "/static"
    tm = request.session[request.user.username]
    path = folder + "/" + tm
    remove_unwanted_data(path)
    #if len(os.getcwd().split("/")[-1]) == 32:
    
    
    offsets, true_height, coords, width, height, im, col, objcts, nm,nm1,srt, cols, upl1, form, b, n, indx, not_entID, con, navn, big_con, ln_fil, ln_indx, ln_nindx, inp = prep_params(path)
        
    with open(path+"/objects.pkl","rb") as f:
        objcts = pickle.load(f)
    print("ØØØØØØØØØØØØØØØØØØØØØØØØØØØØØ", objcts.keys())
    grops = 1
    for k,v in objcts.items():
        grops *= len(v)

    new_net = glob.glob1(path, "*.tab2")
    if len(new_net) > 0:
        with open(path +"/len_upl_netw.pkl", "rb") as f:
            net_nm, net_ln = pickle.load(f)
    if len(new_net) == 0:
        net_nm = "networkBIOGRID-ALL-3.5.171.tab2"

    if 'input[x]' in request.POST:
            
        inp = request.POST['input[x]']
        inp = inp.split(",")
        print(inp)
        #FFf = glob.glob1(path, "*.h5ad")[0]
        #adata = sc.read(path + "/" + FFf)

        #if len(inp) == 2 and inp[1] != "":
        #    with open(path + "/chac_par_gene_type.pkl", "wb") as f:
        #        pickle.dump(inp,f)
        #    navn, b, n, indx, not_entID, con, upl, big_con, ln_fil, ln_indx, ln_nindx, ln_upl, FF = make_csv_files(path, inp, adata) 
                
        if len(inp) == 9:
            #try:
                #print(int(inp[3]), int(inp[4]), int(inp[5]))
                #if int(inp[3]) > 0 and int(inp[4]) > 0 and int(inp[5]) > 0: 
                #    init_params(path,inp, adata, indx, srt, b, n, big_con)
            with open(path + "/chac_par.pkl", "wb") as f:
                pickle.dump(inp,f)
                #except:
                #    pass
    return render(request, 'core/chac_params.html', {"net_nm":net_nm,"ln_indx":ln_indx, "navn":navn.split(",")[1][2:-2], "ln_fil":ln_fil, "upl1":upl1.render(Context(request)),"form":form, "now":big_con, "cols":cols, "inp":inp, "grops":grops})


    #else:
    #    return render(request, 'core/chac_params.html')


@login_required(login_url='home')
@csrf_protect
def change_anndata(request):

    def get_adata_clusters_from_points(point, adata, time, clst):
        tmp = adata.obs[clst]
        if time != "None":
            ps = adata.obs[time]
        if time == "None":
            print("HHHHHHHHHHHHHHHHHHHHHH")
            ps = list(range(len(tmp)))
        tmp1 = list(np.arange(0,len(tmp))[tmp == tmp[point]])
        #tmp1 = [int(i) for i in tmp1]
        try:
            tmp1 = [int(i) for i in tmp1 if np.isfinite(ps[i])]
        except:
            pass
        return tmp1, tmp[point]

    def do_the_stuff(lst_,obj, cls1, adata, time, clst):
        reg = "somthinglonganddifftopredict"
        for k,v in lst_.items():
            c1x = []
            c2x = []
            for nr in v:
                c1, c2 = get_adata_clusters_from_points(nr, adata, time, clst)
                if c1 == []:
                    reg = c2
                else:
                    if c2 not in c2x:
                        c2x.append(c2)
                        c1x += c1
            obj[k] = c1x
            cls1[k] = c2x
        return obj, cls1, reg


    def get_saved_clsut_data(path):
        ## bruger jeg overhovedet dette?
        FF = glob.glob1(path, "*.h5ad")[0]
        adata = sc.read(path + "/" + FF)
        data = list(adata.obs.columns)

        with open(path+"/params.pkl","rb") as f:
            offsets, true_height, coords, width, height, im = pickle.load(f)
        offsetx,offsety = offsets

        with open(path+"/coords.pkl","rb") as f:
            lst_cords = pickle.load(f)

        with open(path+"/color.pkl","rb") as f:
            col = pickle.load(f)

        with open(path+"/all_nms.pkl","rb") as f:
            nm,nm1,srt = pickle.load(f)

        with open(path+"/objects.pkl","rb") as f:
            objcts = pickle.load(f)

        with open(path+"/clusters.pkl","rb") as f:
            clsts = pickle.load(f)

        with open(path+"/array.pkl","rb") as f:
            ps = pickle.load(f)

        with open(path + "/col_list.pkl","rb") as f:
            COL = pickle.load(f)

        new_diff = "nej"
        try:
            with open(path+"/coords_tempp.pkl","rb") as f:
                coords = pickle.load(f)

            with open(path + "/col_list_tempp.pkl","rb") as f:
                COL = pickle.load(f)

            new_diff = "ja"

        except:
            print("*****  COULD NOT FIND THE NEW DATA *****")

        
        ps_cl = glob.glob1(path, "ps_cl.pkl")
        for i in ps_cl:
            with open(path + "/" + i, "rb") as f:
                ps_cl = pickle.load(f)
     
        

        return adata, data, coords, im, lst_cords, col, nm,nm1,srt, objcts, clsts, ps, COL, new_diff, ps_cl

    def draw_clusters(path, cc, lst_cords, col, objcts,clsts, adata, srt, nm, tm, coords, COL1):

        if col == "#FF0000":
            ccl = "red"
        if col == "#00FF00":
            ccl = "green"

        print("now -_-_-_-_-_-_", srt, "lllllllllllllllllll")
        lst_cords[ccl].append(cc)
        print("now1")

        objcts, clsts, reg = do_the_stuff(lst_cords,objcts,clsts, adata, srt, nm)
        print("\n","XXXXXXX", reg,"XXXXXX", "\n")

        for kk, vv in objcts.items():           ####### NEW
            objcts[kk] = list(set(vv))

        if reg != "somthinglonganddifftopredict":
            with open(path+"/nans.pkl","rb") as f:
                nans = pickle.load(f)
            if reg not in nans:
                nans.append(reg)
                with open(path + "/nans.pkl", "wb") as f:
                    pickle.dump(nans,f)
        if reg == "somthinglonganddifftopredict":
            nans = ""

        print("clsts",clsts)
        print("objcts",objcts)
        with open(path + "/clusters.pkl","wb") as f:
            pickle.dump(clsts, f )
        with open(path + "/objects.pkl","wb") as f:
            pickle.dump(objcts, f )
        with open(path + "/coords.pkl","wb") as f:
            pickle.dump(lst_cords, f )


        #colz = ['#00000'] * len(coords)
        for kk,vv in objcts.items():        ####### NEW
            if kk == "red":
                for i in vv:
                    COL1[i] = "#FF0000"
            if kk == "green":
                for i in vv:
                    if COL1[i] == "#FF0000":
                        COL1[i] = "#008080"
                    else:
                        COL1[i] = "#00FF00"


        with open(path + "/col_list.pkl","wb") as f:
            pickle.dump(COL1, f)

        print("\n !1! \n")

        imgnm = glob.glob1(path, "*_the_pic.png")[0]
        imgx = tm + "/" + imgnm
        tmp1x = '{% load static %}<img src="{% static "'
        tmp2x = '" %}" id="non"/>'
        htmlx = Template(tmp1x + imgx + tmp2x)

        print("\n",clsts,"\n")
            #cl = [[ke,va] for ke,va in clsts.items()]
        ob = mini_function(path)
        return htmlx, ob, nans


    def remove_unwanted_data(path):
        pngs = glob.glob1(path, "*.png")
        for i in pngs:
            os.remove(path + "/" + i)
        
        ncsv = glob.glob1(path, "*.h5ad_")
        for i in ncsv:
            os.remove(path + "/" + i)

        ncsv = glob.glob1(path, "*.png_")
        for i in ncsv:
            os.remove(path + "/" + i)




    def make_new_object(path, p1=50, p2=150, p3=50):
        print("\n |||||||||||   in make_new_object   ||||||||||||| \n")
        FF = glob.glob1(path, "*.h5ad")[0]
        adata = sc.read(path + "/" + FF)

        with open(path+"/clusters.pkl","rb") as f:
            not_include = pickle.load(f)
        not_include = not_include["red"]

        with open(path+"/all_nms.pkl","rb") as f:
            plot_col,plot_type,srt = pickle.load(f)


        branch_indx = []
        branch_not_indx = []
        #t = 0
        old_data = list(adata.obs[plot_col])
        #print(old_data,"----")
        #print(not_include,"---")
        for t,i in enumerate(old_data): 
            if i not in not_include:
                branch_indx.append(t)
            else:
                branch_not_indx.append(t)

        try:## vær sikker på at dette virker
            index = list(adata.obs.index[branch_indx])
            col_indx = list(adata.var.index)
        except:
            index = None
            col_indx = None
        
        mtx = adata.X[branch_indx]

        ln_mtx = len(mtx)
    
        df1 = pd.DataFrame(mtx, index=index, columns=col_indx)
        #df1.insert(loc=0, column='Cells', value=tra_cells)
        df1.to_csv(path + "/new_h5ad_.csv",sep=",", index=False)

        adata_x = sc.read_csv(path + "/new_h5ad_.csv")
    
        for i in list(adata.var.columns):
            adata_x.var[i] = adata.var[i]
        

        cols = list(adata.obs.columns)
        for i in cols:
            print(i)
            adata_x.obs["old_"+i] = list(adata.obs[i][branch_indx])
            #print(len(list(adata.obs[i][branch_indx])), len(list(adata.obs[i])))

        if ln_mtx < p1:
            p1 = ln_mtx - 1
        if ln_mtx < p2:
            p2 = ln_mtx - 1
        if ln_mtx < p3:
            p3 = ln_mtx - 1

        sc.tl.pca(adata_x, svd_solver='arpack', n_comps=p1)
        sc.pp.neighbors(adata_x, n_neighbors=p2, n_pcs=p3)
        sc.tl.diffmap(adata_x, n_comps=15)

        imgnm = glob.glob1(path, "*_the_pic.png")[0]
        os.rename(path+"/"+imgnm,path+"/"+imgnm+"_")

        sc.settings.figdir = path
        sc.pl.diffmap(adata_x, color=["old_"+plot_col], save="_the_pic.png", show=False)
    
        adata_x.write_h5ad(path + "/new_h5ad.h5ad")
    
        coords = get_timeline_coordinates(adata_x, 0, 0, type1 = "diffmap")
        with open(path + "/coords_tempp.pkl","wb") as f:
            pickle.dump(coords, f )
    
        colz = ['#00000'] * len(coords)
        with open(path + "/col_list_tempp.pkl","wb") as f:
            pickle.dump(colz, f)

        FF = glob.glob1(path, "*.h5ad")
        tmp_h5ad = ""
        for i in FF:
            print(i)
            if i != "new_h5ad.h5ad":
                tmp_h5ad = i
        print("-->",tmp_h5ad)
        os.rename(path+"/"+tmp_h5ad,path+"/"+tmp_h5ad+"_")


    def calc_pseudo_etc(path, start):
        print("**** calculating pseudotime and etc", start) 
        FF = glob.glob1(path, "*.h5ad")[0]
        adata = sc.read(path + "/" + FF)
        print("**** data has been found")
        #with open(path+"/all_nms.pkl","rb") as f:
        #    plot_col,plot_type,srt = pickle.load(f)

        adata.uns['iroot'] = start
        sc.tl.dpt(adata, n_dcs=15)
        print("PS has been calculated")
        ##3 PAGA
        #sc.tl.louvain(adata)
        #sc.tl.paga(adata)#, groups="old_"+plot_col)
        #sc.pl.paga(adata, show=False)
        #sc.tl.umap(adata)#, init_pos="paga")# , min_dist=0.001)    
        #sc.tl.draw_graph(adata)#, init_pos='paga')
    
        adata.write_h5ad(path + "/new_h5ad.h5ad")
        remove_unwanted_data(path)

        with open(path+"/coords_tempp.pkl","rb") as f:
            coords = pickle.load(f)

        with open(path + "/col_list_tempp.pkl","rb") as f:
            COL = pickle.load(f)

        with open(path+"/coords_change_data.pkl","wb") as f:
            pickle.dump(coords,f)

        with open(path + "/col_list_change_data.pkl","wb") as f:
            pickle.dump(COL, f)

        os.remove(path+"/coords_tempp.pkl")
        os.remove(path+"/col_list_tempp.pkl")
        print("done")



    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    folder = "/".join(dr.split("/")[:-2]) + "/static"
    tm = request.session[request.user.username]
    print(tm)
    path = folder+"/"+tm

    if request.method == 'POST' and 'input[x]' in request.POST:
        print("input[x]", request.POST)
        tmp = request.POST['input[x]']
        p1, p2, p3 = tmp.split(",")
        make_new_object(path, p1=int(p1), p2=int(p2), p3=int(p3))

    adata, data, coords, im, lst_cords, col, nm,nm1,srt, objcts, clsts, ps, COL1, new_diff, ps_cl = get_saved_clsut_data(path)
    ps1 = list(ps)
    ps1.sort()
    X = json.dumps(coords[:,0], cls=NumpyEncoder)
    Y = json.dumps(coords[:,1], cls=NumpyEncoder)
    COL = json.dumps(COL1)
    ps1 = json.dumps(ps1, cls=NumpyEncoder)

    imgnm = glob.glob1(path, "*_the_pic.png")[0]
    imgx = tm + "/" + imgnm
    tmp1x = '{% load static %}<img src="{% static "'
    tmp2x = '" %}" id="non"/>'
    htmlx = Template(tmp1x + imgx + tmp2x)

    if "col[c]" in request.POST:
        col = request.POST["col[c]"]
        with open(path + "/color.pkl","wb") as f:
            pickle.dump(col, f )

    ob = mini_function(path)


    form1 = Percent ## should delete this form
    form = PostForm()
    print("\n",request.POST,"\n")


    if request.method == 'POST' and 'stuff[z]' in request.POST:
        #tmp = [int(request.POST['stuff[x]'])-offsetx, int(request.POST['stuff[y]'])-offsety]
        tmp = request.POST['stuff[z]']
        cc, col = tmp.split("_")
        cc = int(cc)
        if new_diff == "nej":#cc in ps:

            htmlx, ob, nans  = draw_clusters(path, cc, lst_cords, col, objcts,clsts, adata, "None", nm, tm, coords, COL1) # "None" = srt

            if col == '#FF0000':
                col = "red"
                with open(path + "/color.pkl","wb") as f:
                    pickle.dump(col, f )
                with open(path + "/col_list.pkl","wb") as f:
                    COL1[cc] = '#FF0000'
                    pickle.dump(COL1, f )

            if col == '#00FF00':
                col = "green"
                with open(path + "/color.pkl","wb") as f:
                    pickle.dump(col, f )
                with open(path + "/col_list.pkl","wb") as f:
                    COL1[cc] = '#00FF00'
                    pickle.dump(COL1, f )

        if new_diff == "ja":
            print("skal initialisere pseudotime")

            if col == '#FF0000':
                col = "red"
                with open(path + "/color.pkl","wb") as f:
                    pickle.dump(col, f )
                with open(path + "/col_list_tempp.pkl","wb") as f:
                    colz = ['#00000'] * len(COL1)
                    colz[cc] = '#FF0000'
                    pickle.dump(colz, f)
            ps_cl = adata.obs["old_"+nm][cc]
            with open(path + "/ps_cl.pkl","wb") as f:
                pickle.dump(ps_cl, f )



    if request.method == 'POST' and 'input2[x]' in request.POST:
        with open(path + "/col_list_tempp.pkl","rb") as f:
            colz = pickle.load(f)
        start1 = colz.index('#FF0000')
        calc_pseudo_etc(path, start1)


    #if ob == "You need to select some points/cells in the scatter plot":
    #    ob = {"red":[]}
    return render(request, 'core/change_anndata.html', {'form':form, 'imgx':htmlx.render(Context(request)), 'form1':form1, "col":col, "objs":ob, "cl":clsts["red"], "X":X, "Y":Y, "COL":COL, "inf_":ps1, "new_diff":new_diff, "ps_cl":ps_cl})

#ps1





@login_required(login_url='home')
@csrf_protect
def cluster_pick(request):

    def get_adata_clusters_from_points(point, adata, time, clst):
        tmp = adata.obs[clst]
        if time != "None":
            ps = adata.obs[time]
        if time == "None":
            ps = list(range(len(tmp)))
        tmp1 = list(np.arange(0,len(tmp))[tmp == tmp[point]])
        #tmp1 = [int(i) for i in tmp1]
        try:
            tmp1 = [int(i) for i in tmp1 if np.isfinite(ps[i])]
        except:
            pass
        return tmp1, tmp[point]

    def do_the_stuff(lst_,obj, cls1, adata, time, clst):
        reg = "somthinglonganddifftopredict"
        for k,v in lst_.items():
            c1x = []
            c2x = []
            for nr in v:
                c1, c2 = get_adata_clusters_from_points(nr, adata, time, clst)
                if c1 == []:
                    reg = c2
                else:
                    if c2 not in c2x:
                        c2x.append(c2)
                        c1x += c1 
            obj[k] = c1x
            cls1[k] = c2x
        return obj, cls1, reg


    def get_saved_clsut_data(path):
        ## bruger jeg overhovedet dette?
        FF = glob.glob1(path, "*.h5ad")[0]
        adata = sc.read(path + "/" + FF)
        data = list(adata.obs.columns)

        with open(path+"/params.pkl","rb") as f:
            offsets, true_height, coords, width, height, im = pickle.load(f)
        offsetx,offsety = offsets

        with open(path+"/coords.pkl","rb") as f:
            lst_cords = pickle.load(f)

        with open(path+"/color.pkl","rb") as f:
            col = pickle.load(f)

        with open(path+"/all_nms.pkl","rb") as f:
            nm,nm1,srt = pickle.load(f)
        
        with open(path+"/objects.pkl","rb") as f:
            objcts = pickle.load(f)
        
        with open(path+"/clusters.pkl","rb") as f:
            clsts = pickle.load(f)
            
        with open(path+"/array.pkl","rb") as f:
            ps = pickle.load(f)  
            
        with open(path + "/col_list.pkl","rb") as f:
            COL = pickle.load(f)
            
        return adata, data, coords, im, lst_cords, col, nm,nm1,srt, objcts, clsts, ps, COL


    def draw_clusters(path, cc, lst_cords, col, objcts,clsts, adata, srt, nm, tm, coords, COL1):

        if col == "#FFC20A":
            ccl = "red"
        if col == "#0C7BDC":
            ccl = "green"
        
        print("now")
        lst_cords[ccl].append(cc)
        print("now1")

        objcts, clsts, reg = do_the_stuff(lst_cords,objcts,clsts, adata, srt, nm)
        print("\n","XXXXXXX", reg,"XXXXXX", "\n")

        for kk, vv in objcts.items():           ####### NEW
            objcts[kk] = list(set(vv))

        if reg != "somthinglonganddifftopredict":
            with open(path+"/nans.pkl","rb") as f:
                nans = pickle.load(f)   
            if reg not in nans:
                nans.append(reg)
                with open(path + "/nans.pkl", "wb") as f:
                    pickle.dump(nans,f)
        if reg == "somthinglonganddifftopredict": 
            nans = ""     

        print("clsts",clsts)
        print("objcts",objcts)
        with open(path + "/clusters.pkl","wb") as f:
            pickle.dump(clsts, f )
        with open(path + "/objects.pkl","wb") as f:
            pickle.dump(objcts, f )
        with open(path + "/coords.pkl","wb") as f:
            pickle.dump(lst_cords, f )


        #colz = ['#00000'] * len(coords)
        for kk,vv in objcts.items():        ####### NEW
            if kk == "red":
                for i in vv:
                    COL1[i] = "#FFC20A"#"#FF0000"
            if kk == "green":
                for i in vv:
                    if COL1[i] == "#FFC20A":#"#FF0000":
                        COL1[i] = "#1AFF1A"#"#008080"
                    else:
                        COL1[i] = "#0C7BDC"#"#00FF00"


        #for kk,vv in objcts.items():
        #    if kk == "red":
        #        for i in vv:
        #            COL1[i] = "#FF0000"
        #    if kk == "green":
        #        for i in vv:
        #            COL1[i] = "#00FF00"
                    
        with open(path + "/col_list.pkl","wb") as f:
            pickle.dump(COL1, f)

        print("\n !1! \n")

        imgnm = glob.glob1(path, "*_the_pic.png")[0]
        imgx = tm + "/" + imgnm
        tmp1x = '{% load static %}<img src="{% static "'
        tmp2x = '" %}" id="non"/>'
        htmlx = Template(tmp1x + imgx + tmp2x)

        print("\n",clsts,"\n")
            #cl = [[ke,va] for ke,va in clsts.items()]
        ob = mini_function(path)
        return htmlx, ob, nans

    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    folder = "/".join(dr.split("/")[:-2]) + "/static"
    tm = request.session[request.user.username]
    path = folder+"/"+tm
    
    adata, data, coords, im, lst_cords, col, nm,nm1,srt, objcts, clsts, ps, COL1 = get_saved_clsut_data(path)
    ps1 = list(ps)
    ps1.sort()
    X = json.dumps(coords[:,0], cls=NumpyEncoder)
    Y = json.dumps(coords[:,1], cls=NumpyEncoder)
    COL = json.dumps(COL1)
    ps1 = json.dumps(ps1, cls=NumpyEncoder)
    
    imgnm = glob.glob1(path, "*_the_pic.png")[0]
    imgx = tm + "/" + imgnm
    tmp1x = '{% load static %}<img src="{% static "'
    tmp2x = '" %}" id="non"/>'
    htmlx = Template(tmp1x + imgx + tmp2x)
    
    if "col[c]" in request.POST:
        col = request.POST["col[c]"]
        with open(path + "/color.pkl","wb") as f:
            pickle.dump(col, f )

    ob = mini_function(path)
            
    form1 = Percent ## should delete this form
    form = PostForm()
    print("\n",request.POST,"\n")


    if request.method == 'POST' and 'stuff[z]' in request.POST:
        #tmp = [int(request.POST['stuff[x]'])-offsetx, int(request.POST['stuff[y]'])-offsety]
        tmp = request.POST['stuff[z]']
        cc, col = tmp.split("_")
        cc = int(cc)
        if cc in ps:
            
            htmlx, ob, nans  = draw_clusters(path, cc, lst_cords, col, objcts,clsts, adata, srt, nm, tm, coords, COL1)

            if col == '#FFC20A':
                col = "red"
                with open(path + "/color.pkl","wb") as f:
                    pickle.dump(col, f )
                with open(path + "/col_list.pkl","wb") as f:
                    COL1[cc] = '#FFC20A'
                    pickle.dump(COL1, f )
                    
            if col == '#0C7BDC':
                col = "green"
                with open(path + "/color.pkl","wb") as f:
                    pickle.dump(col, f )
                with open(path + "/col_list.pkl","wb") as f:
                    COL1[cc] = '#0C7BDC'
                    pickle.dump(COL1, f )

    if col == "red":
        col1 = ["Group 1", "#FFC20A"]
    if col == "green":
        col1 = ["Group 2", "#0C7BDC"]
    
    if len(ob) != 56:
        if len(ob) == 1:
            if ob[0][0] == "red":
                ob[0][0] = "Group 1"
            if ob[0][0] == "green":
                ob[0][0] = "Group 2"
        if len(ob) == 2:
            ob[0][0] = "Group 1"
            ob[1][0] = "Group 2"
    clsts1 = {}
    clsts1["Group 1"] = clsts["red"] 
    clsts1["Group 2"]= clsts["green"] 
    return render(request, 'core/cluster_pick.html', {'form':form, 'imgx':htmlx.render(Context(request)), 'form1':form1, "col":col1, "objs":ob, "cl":clsts1, "X":X, "Y":Y, "COL":COL, "inf_":ps1})



"""
        if request.method == 'POST' and 'stuff[z]' in request.POST:
            html, htmlx, ob, nans  = draw_clusters(folder + "/" +tm, request.POST['stuff[x]'], request.POST['stuff[y]'], lst_cords, col, objcts,clsts, adata, srt, nm, tm, coords)
            return render(request, 'core/cluster_pick.html', {'form':form, 'imgx':htmlx.render(Context(request)), 'form1':form1, "col":col, "objs":ob, "cl":clsts, "reg":nans, "X":X, "Y":Y, "COL":COL, "inf_":ps1})
            #return redirect('display_name')
        else:

            imgnm = glob.glob1(folder + "/" +tm, "*_the_pic.png")[0]
            imgx = tm + "/" +imgnm
            tmp1x = '{% load static %}<img src="{% static "'
            tmp2x = '" %}" id="non"/>'
            htmlx = Template(tmp1x + imgx + tmp2x)
            #cl = [[ke,va] for ke,va in clsts.items()]
            ob = mini_function(folder + "/" +tm)
            with open(folder + "/" +tm+"/nans.pkl","rb") as f:
                nans = pickle.load(f)
            return render(request, 'core/cluster_pick.html', {'form':form, 'imgx':htmlx.render(Context(request)), 'form1':form1, "col":col, "objs":ob, "cl":clsts, "reg":nans, "X":X, "Y":Y, "COL":COL, "inf_":ps1})
   """
    #except:
    #else:
    #    render(request, 'core/display_name.html')



@login_required(login_url='home')
@csrf_protect
def change_start_over(request):

    print("now in start over")
    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    folder = "/".join(dr.split("/")[:-2]) + "/static"
    tm = request.session[request.user.username]#tm[-1]
    path = folder + "/" + tm

    tmpp = glob.glob1(path, "*tempp.pkl")
    for i in tmpp:
        os.remove(path + "/" + i)


    FF = glob.glob1(path, "*.h5ad_")
    if len(FF) == 1:
        print("DELEING EVERYTHING")
        tmpp = glob.glob1(path, "*.h5ad")
        for i in tmpp:
            os.remove(path + "/" + i)

        ff = FF[0] #glob.glob1(path, "*.h5ad_")[0]
        os.rename(path+"/"+ff, path+"/"+ff[:-1])
        #"/new_h5ad.h5ad"
        pngs = glob.glob1(path, "*.png")
        for i in pngs:
            os.remove(path + "/" + i)
   
        imgnm = glob.glob1(path, "*_the_pic.png_")[0]
        os.rename(path+"/"+imgnm,path+"/"+imgnm[:-1])

        pc_cl = glob.glob1(path, "ps_cl.pkl")
        for i in pc_cl:
            os.remove(path + "/" + i)

        ncsv = glob.glob1(path, "*h5ad_.csv")
        for i in ncsv:
            os.remove(path + "/" + i)




    with open(path + "/coords.pkl","wb") as f:
        pickle.dump({"red":[], "green":[]}, f )
    with open(path + "/objects.pkl", "wb") as f:
        pickle.dump({"red":[], "green":[]},f)
    with open(path + "/clusters.pkl","wb") as f:
        pickle.dump({"red":[], "green":[]}, f )
    #with open(path + "/nans.pkl", "wb") as f:
    #    pickle.dump([],f)
    with open(path + "/params.pkl","rb") as f:
        offsets, true_height, coords, width, height, im = pickle.load(f)

    colz = ['#00000'] * len(coords)
    with open(path + "/col_list.pkl","wb") as f:
        pickle.dump(colz, f )

    #plot_new_fig(coords, path, {"red":[], "green":[]}, width, height)
    print("\n Should redirect now \n")
    return redirect('change_anndata')




@login_required(login_url='home')
@csrf_protect
def cluster_start_over(request):

    print("now in start over")
    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    folder = "/".join(dr.split("/")[:-2]) + "/static"
    tm = request.session[request.user.username]#tm[-1]
    path = folder + "/" + tm
    
    with open(path + "/coords.pkl","wb") as f:
        pickle.dump({"red":[], "green":[]}, f )
    with open(path + "/objects.pkl", "wb") as f:
        pickle.dump({"red":[], "green":[]},f)
    with open(path + "/clusters.pkl","wb") as f:
        pickle.dump({"red":[], "green":[]}, f )
    #with open(path + "/nans.pkl", "wb") as f:
    #    pickle.dump([],f)
    with open(path + "/params.pkl","rb") as f:
        offsets, true_height, coords, width, height, im = pickle.load(f)

    colz = ['#00000'] * len(coords)
    with open(path + "/col_list.pkl","wb") as f:
        pickle.dump(colz, f )

    #plot_new_fig(coords, path, {"red":[], "green":[]}, width, height)
    print("\n Should redirect now \n")
    return redirect('cluster_pick')




@csrf_protect
def start_over_draw(request):

    print("now in start over")
    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    folder = "/".join(dr.split("/")[:-2]) + "/static"
    tm = request.session[request.user.username]#tm[-1]
    path = folder + "/" + tm

    with open(path + "/coords.pkl","wb") as f:
        pickle.dump({"red":[], "green":[]}, f )
    with open(path + "/objects.pkl", "wb") as f:
        pickle.dump({"red":[], "green":[]},f)
    with open(path + "/clusters.pkl","wb") as f:
        pickle.dump({"red":[], "green":[]}, f )
        
    with open(path + "/params.pkl","rb") as f:
        offsets, true_height, coords, width, height, im = pickle.load(f)

    colz = ['#00000'] * len(coords)
    with open(path + "/col_list.pkl","wb") as f:
        pickle.dump(colz, f )

    #plot_new_fig(coords, path, {"red":[], "green":[]}, width, height)
    print("\n Should redirect now \n")
    return redirect('display_name')
    #return render(request, 'core/start_over_draw.html')
    #else:
    #    return render(request, 'core/start_over_draw.html')


import json
class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
            np.int16, np.int32, np.int64, np.uint8,
            np.uint16, np.uint32, np.uint64)):
            return int(obj)
        elif isinstance(obj, (np.float_, np.float16, np.float32, 
            np.float64)):
            return float(obj)
        elif isinstance(obj,(np.ndarray,)): #### This is the fix
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)



@login_required(login_url='home')
@csrf_protect
def display_name(request):
        
    def match_path(mtx, lst, diff, pct, ps, lst_zero):            ########## NEW
        
        ln_mtx = len(mtx)
        new_lst = set(lst)
    
        if diff > 0:
            tmp_lst = lst.copy()
            #print("***",pct,"**",diff,tmp_lst)
            for i in range(diff):
                x = random.choice(tmp_lst)
                #tmp_lst.remove(x)
                tmp_range = list(range(ln_mtx))
                a = list(zip(mtx[x], tmp_range))
                a.sort()
                b,c = zip(*a)
                for ii in c:
                    if ii not in new_lst and ii in ps:
                        new_lst.add(ii)
                        break

        if diff < 0:
            #mtx1 = mtx.copy()
            #np.fill_diagonal(mtx1, -np.max(mtx1)-1)
            tmp_lst = []
            tmp_set = set(lst_zero)
            for i in lst:
                if i not in tmp_set:
                    tmp_lst.append(i)
            tmp_lst = lst_zero + tmp_lst
            
            ln_mx = len(lst_zero)
            mtx1 = cdist(coords[lst_zero], coords[tmp_lst], metric="euclidean")
            mtx1 = mtx1[:,ln_mx:]
            
            av_dist = np.average(mtx1, axis=0) #-- perhaps use this one, !! Maybe multiply !!

            for i in range(abs(diff)): 
                pos = np.argmax(av_dist)
                #argpos = margs[pos]
                val = tmp_lst[ln_mx + pos]
                if 0 < av_dist[pos]: #in new_lst:
                    new_lst.remove(val)
                    av_dist[pos] = -1
                    
                    print("starting!!!", pos, val)
                print(i)
            

        return new_lst
    
    
    def even_out(crds, slk, path, ps, srt, c1, c2, pct): ########## NEW
        ### The other path has to have the minumum sized trajectory. 
        ### --> take these points and subtract from bulk_cell_c2 and then add 
        ###     for every cell like normal
        
        # c1 is the one you want to look like c2 - it is colors
        # lst is the list with c1 path-cells
        with open(path + "/objects.pkl","rb") as f:
            objcts = pickle.load(f)
        
        if c1 == "red":
            p = pct[1]
        else:
            p = pct[0]
        
        lst_zero = []
        ### below the trajectory with prct = 0 will be calculated
        picked_cells_c1 = crds[c1]
        if len(picked_cells_c1) > 0:
            if len(picked_cells_c1) == 1:
                for i in range(len(picked_cells_c1)):
                    tmpp = greedy_shortest_path(slk, picked_cells_c1[i], picked_cells_c1[i], crds, srt, pct = 0)#pct[cnter])
                    if srt != "None":
                        lst_zero += list(ps.intersection(tmpp))
                    if srt == "None":
                        lst_zero += tmpp
            if len(picked_cells_c1) > 1:
                for i in range(len(picked_cells_c1)-1):
                    tmpp = greedy_shortest_path(slk, picked_cells_c1[i], picked_cells_c1[i+1], crds, srt, pct = 0)#pct[cnter])
                    if srt != "None":
                        lst_zero += list(ps.intersection(tmpp))
                    if srt == "None":
                        lst_zero += tmpp
            lst_zero = set(lst_zero)
            
            lst = []
            #picked_cells_c1 = crds[c1]
            if len(picked_cells_c1) == 1:
                for i in range(len(picked_cells_c1)):
                    tmpp = greedy_shortest_path(slk, picked_cells_c1[i], picked_cells_c1[i], crds, srt, pct = p)#pct[cnter])
                    if srt != "None":
                        lst += list(ps.intersection(tmpp))
                    if srt == "None":
                        lst += tmpp
            if len(picked_cells_c1) > 1:
                for i in range(len(picked_cells_c1)-1):
                    tmpp = greedy_shortest_path(slk, picked_cells_c1[i], picked_cells_c1[i+1], crds, srt, pct = p)#pct[cnter])
                    if srt != "None":
                        lst += list(ps.intersection(tmpp))
                    if srt == "None":
                        lst += tmpp
            lst = set(lst)
            lst_zero = list(lst.intersection(lst_zero))
            lst = list(lst)
            
            ### the number of cells needed per cell in path_c1
            objcts[c1] = list(set(lst))
            ln_lst = len(lst)
            ln_objt = len(objcts[c2])
            
            if len(lst_zero) < ln_objt:
                
                print("***", len(lst), ln_objt, ln_objt-ln_lst)
                
                if 0 <= abs(ln_lst - ln_objt):
                    
                    cells_needed = ln_objt-ln_lst
                    #print(cells_needed,"***",len(lst),"***",lst)
                    #average_cells_included_pr_cell_in_c1 = int(np.round(cells_needed / ln_lst))
                    #print("***",cells_needed,lst)
                    
                    p = int(np.ceil((len(slk[0])/100) * p))
                    new_lst = match_path(slk, lst, cells_needed, p, ps, lst_zero)
        
                    ### if it doesn't fit, then take the needed cell from random other cells in lst.
                    ### --> the sam with removal
                    objcts[c1] = list(new_lst)
        
                with open(path + "/objects.pkl", "wb") as f:
                    pickle.dump(objcts,f)
        
        
                colz = ['#00000'] * len(coords)
                for kk,vv in objcts.items():
                    if kk == "red":
                        for i in vv:
                            colz[i] = "#FFC20A"#"#FF0000"
                    if kk == "green":
                        for i in vv:
                            if colz[i] == "#FFC20A":#"#FF0000":
                                colz[i] = "#1AFF1A"#"#008080"
                            else:
                                colz[i] = "#0C7BDC"#"#00FF00"
            
                with open(path + "/col_list.pkl","wb") as f:
                    pickle.dump(colz, f)
                
                return colz
            else:
                return []
        else:
            return []
        
    
    
    def get_data_for_template(path):

        with open(path + "/params.pkl","rb") as f:
            offsets, true_height, coords, width, height, im = pickle.load(f)

        with open(path + "/coords.pkl","rb") as f:
            lst_cords = pickle.load(f)
            
        with open(path + "/color.pkl","rb") as f:
            col = pickle.load(f)

        with open(path+"/array.pkl","rb") as f:
            ps = pickle.load(f)        
            
        ##with open(path + "/array_inf.pkl", "rb") as f:
        ##    ps_inf = pickle.load(f) 

        with open(path+"/nans.pkl","rb") as f:
            nans = pickle.load(f) 

        with open(path + "/all_nms.pkl", "rb") as f:
            nm, nm1, srt = pickle.load(f)
        
        with open(path + "/col_list.pkl","rb") as f:
            COL = pickle.load(f)

        with open(path + "/percentages.pkl","rb") as f:
            pct = pickle.load(f)

            
        slk = cdist(coords, coords, metric="euclidean")
        np.fill_diagonal(slk, np.max(slk) + 2)    
        
        return col, coords, im, lst_cords, ps, nm, nm1, srt, nans, COL, slk, pct
    
    
    
    def checking_nans_in_data(path, cc, lst_cords, col, tm):
        lst_cords[col].append(cc)
        with open(path + "/coords.pkl","wb") as f:
            pickle.dump(lst_cords, f )
            #os.getcwd()
        print("\n !1! \n")
        
        #plot_new_fig(coords, path, lst_cords, width, height, col)
        
        return lst_cords

    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    folder = "/".join(dr.split("/")[:-2]) + "/static"
    tm = request.session[request.user.username]
    path = folder+"/"+tm
    
    col, coords, im, lst_cords, ps, nm, nm1, srt, nans, COL1, slk, pct = get_data_for_template(folder+"/"+request.session[request.user.username])
    ps1 = list(ps)
    ps1.sort()
    X = json.dumps(coords[:,0], cls=NumpyEncoder)
    Y = json.dumps(coords[:,1], cls=NumpyEncoder)
    COL = json.dumps(COL1)
    ps1 = json.dumps(ps1, cls=NumpyEncoder)
    
    imgnm = glob.glob1(path, "*_the_pic.png")[0]
    imgx = tm + "/" + imgnm
    tmp1x = '{% load static %}<img src="{% static "'
    tmp2x = '" %}" id="non"/>'
    htmlx = Template(tmp1x + imgx + tmp2x)
    
    ob = mini_function(path)
    if len(ob) != 56:
        if len(ob) == 1:
            if ob[0][0] == "red":
                ob[0][0] = "Group 1"
            if ob[0][0] == "green":
                ob[0][0] = "Group 2"
        if len(ob) == 2:
            ob[0][0] = "Group 1"
            ob[1][0] = "Group 2"

    if "col[c]" in request.POST:
        col = request.POST["col[c]"]
        with open(path + "/color.pkl","wb") as f:
            pickle.dump(col, f )


    form1 = Percent 
    form = PostForm()
    
    
    if "even[even]" in request.POST:            ########## NEW
        print(request.POST)
        print("in even!!!!!!!!!")
        if request.POST["even[even]"] == "match_g_w_r":
            C = even_out(lst_cords, slk, path, ps, srt, "green", "red", pct)
            if len(C) > 0:
                COL = json.dumps(C)
        elif request.POST["even[even]"] == "match_r_w_g":
            C = even_out(lst_cords, slk, path, ps, srt, "red", "green", pct)
            if len(C) > 0:
                COL = json.dumps(C)
        
    
    if "calc[calc]" in request.POST:
        #if 1 == 1:
        try:
            print("-------TRY------")
            #red_pct, green_pct = request.POST["calc[calc]"].split("_")
            pct = request.POST["calc[calc]"].split("_")
            pct = [float(PCT) for PCT in pct]
            #pct = float(request.POST["calc[calc]"])

            with open(path + "/percentages.pkl", "wb") as f: ########## NEW
                pickle.dump(pct,f)


            if pct[0] < 100 and pct[0] >= 0 and pct[1] < 100 and pct[1] >= 0:
                print("-------TRY IF------", pct)
                htmlx, COL1 = the_drawer(coords,lst_cords, pct, slk, path, tm,ps, srt, COL1)
                print("should return the fat on now")
                COL = json.dumps(COL1)
                return render(request, 'core/display_name.html', {'form':form, 'form1':form1, 'imgx':htmlx.render(Context(request)), "col":col, "objs":ob, "X":X, "Y":Y, "COL":COL, "inf_":ps1, "pct":pct})
            else:
                print("-------TRY ELSE------")
                #pct_error = "The percentage should be a values between 0 and 100"
                htmlx, COL1 = the_drawer(coords, lst_cords, 0, slk, path, tm,ps, srt, COL1)
                COL = json.dumps(COL1)
                return render(request, 'core/display_name.html', {'form':form, 'form1':form1, 'imgx':htmlx.render(Context(request)), "col":col, "objs":ob, "X":X, "Y":Y, "COL":COL, "inf_":ps1, "pct":pct})
        #else:
        except:
            print("-------EXCEPT------")
            #val_error = "The inserted values should be a float or an integer"
            htmlx, COL1 = the_drawer(coords, lst_cords, 0, slk, path, tm, ps, srt, COL1)
            COL = json.dumps(COL1)
            return render(request, 'core/display_name.html', {'form':form, 'form1':form1, 'imgx':htmlx.render(Context(request)), "col":col, "objs":ob, "X":X, "Y":Y, "COL":COL, "inf_":ps1, "pct":pct})

        
    print("\n before post \n")
    if request.method == 'POST' and 'stuff[z]' in request.POST:
        #tmp = [int(request.POST['stuff[x]'])-offsetx, int(request.POST['stuff[y]'])-offsety]
        tmp = request.POST['stuff[z]']
        cc, col = tmp.split("_")
        cc = int(cc)
        if cc in ps:
            if col == "#FFC20A":#'#FF0000':
                col = "red"
                with open(path + "/color.pkl","wb") as f:
                    pickle.dump(col, f )
                with open(path + "/col_list.pkl","wb") as f:
                    COL1[cc] = "#FFC20A"#'#FF0000'
                    pickle.dump(COL1, f )
            if col == "#0C7BDC":#'#00FF00':
                col = "green"
                with open(path + "/color.pkl","wb") as f:
                    pickle.dump(col, f )
                with open(path + "/col_list.pkl","wb") as f:
                    COL1[cc] = "#0C7BDC"#'#00FF00'
                    pickle.dump(COL1, f )
 
            lst_cords = checking_nans_in_data(path, cc, lst_cords, col, tm)

    if col == "red":
        col1 = ["Group 1", "#FFC20A"]
    if col == "green":
        col1 = ["Group 2", "#0C7BDC"]
    return render(request, 'core/display_name.html', {'form':form, 'imgx':htmlx.render(Context(request)), 'form1':form1, "col":col1, "objs":ob, "er":nans, "X":X, "Y":Y, "COL":COL, "inf_":ps1, "pct":pct} )



def check_location(x):
    
    dirr = os.path.dirname(os.path.realpath(__file__)) +"/static/"
    dirr = dirr.split("/")[:-4] + ["static/"]
    dirr = "/".join(dirr)
    print(1, dirr)
    tmp = 0
    al = glob.glob1(dirr,"*")
    print(2, al)
    print(dirr+x+"/")
    for i in al:
        if i == x:
            print(i)
            al1 = glob.glob1(dirr+x+"/","*")
            print(3, al1)
            #if len(al1) == 2:
            for ii in al1:
                if ii.split("_")[0] == x:
                    tmp += 1
            for ii in al1:
                try:
                    if ii.split("_")[1][-9:] == '.h5ad.txt':
                        tmp += 1
                    if ii.split("_")[1][-5:] == '.h5ad':
                        tmp += 1
                except:
                    pass
    if tmp/2 == 2:
        return 1
    else: 
        return 0



@login_required(login_url='home')
@csrf_protect
def after_simple_upload(request):
    
    
    def remove_unwanted_data(path):
        all_pdfs = glob.glob1(path, "*.pdf")
        tsvs = glob.glob1(path, "*.tsv")
        pngs = glob.glob1(path, "*.png")
        csvs = glob.glob1(path, "*.csv")
        clst_inp = glob.glob1(path, "all_clustering_data.pkl")
        png = []
        for i in pngs:
            if "_the_pic.png" not in i:
                png.append(i)
        all_data = tsvs + all_pdfs + png + csvs + clst_inp + glob.glob1(path, "*.zip")
        for i in all_data:
            os.remove(path + "/" + i)
    
    
    print("-----", os.getcwd(), "------")
    def read_adata_and_more(path, user_nm):
        print("in AFTER...")
        try:
            with open(path + "/all_nms.pkl", "rb") as f:
                nm, nm1, srt = pickle.load(f)
            parms = [nm, nm1, srt]
        except:
            parms = []
             

        #print("-----", os.getcwd(), "------")
        #tm = os.getcwd().split("/")
        #tm = tm[-1]
        #tm = request.session[request.user.username]
        imgnm = glob.glob1(path, "*_the_pic.png")
        if len(imgnm) == 1:
            img = user_nm + "/" + imgnm[0]
            tmp1 = '{% load static %}<img src="{% static "'
            tmp2 = '" %}" id="graph"/>'
            html = Template(tmp1 + img + tmp2)
            html = html.render(Context(request))
        if len(imgnm) != 1:
            html = "0"

        #print(tm)
        FF = glob.glob1(path,"*.h5ad")[0]
        FF = path + "/" + FF
        adata = sc.read(FF)

        data = list(adata.obs.columns)
        if "dpt_pseudotime" in data:
            data1 = data.copy()
            data1.remove("dpt_pseudotime")
            data1 = ["dpt_pseudotime"] + data1 + ["None"]
        if "dpt_pseudotime" not in data:
            data1 = data + ["None"]
        ndata1 = []
        for items in data1:
            try:
                int(list(adata.obs[items])[0])
                ndata1.append(items)
            except:
                pass
        data1 = ndata1
        obsm = list(adata.obsm_keys())
    
        return parms, html, obsm, data1, data, adata
    
    
    def print_plots_and_save_data(path1,all_nms, user_id):
        print(all_nms.split(","))
        nm, nm1, srt = all_nms.split(",")
  
        c = glob.glob1(path1, "*_the_pic.png")
        fil = glob.glob1(path1, "*.h5ad")
        #c2 = [i for i in c if "test.png" in i]
        if len(c) > 0:
            os.remove(path1 + "/" + c[0])
        nm,nm1, st = work_with_names(nm,nm1)
        print(">>>>>>>>>>>>>>>>>>>>",nm,nm1,st,fil)
        sc.settings.figdir = path1
        #sc.tl.draw_graph(adata)
        if fil[0] in ["test1.h5ad", "test4.h5ad","test5.h5ad", "dietmar.h5ad"] and nm1 in ["draw_graph_fa", "draw_graph_fr"]:
            sc.tl.draw_graph(adata)
        exec(st)
                #p = path.split("/")
                #p = "/".join(p[:-1])
        width = 786-15#, height = Image.open(p + "/scatter_template.png").size
        height = 767-15
        print("\n3\n")
        coords = get_timeline_coordinates(adata, width, height, type1 = nm1)
        offsetx = 0
        offsety = 0
        true_height = height - offsety
        coords[:,0] -= np.min(coords[:,0])
        coords[:,1] -= np.min(coords[:,1])
        coords[:,0] = (coords[:,0] / np.max(coords[:,0])) * (width-30)# - offsetx)
        coords[:,1] = (coords[:,1] / np.max(coords[:,1])) * (height-30)# - offsety)
        coords[:,0] += 15
        coords[:,1] += 15
        lst_cords = {"red":[], "green":[]}
        clst = {"red":[], "green":[]}#, "name":nm}
        plot_new_fig(coords, path1, lst_cords, width, height)
        im = ""#pl.imread(path + "/scatter_template.png",format="png")
        print("\n4\n")
        colz = ['#00000'] * len(coords)
        with open(path1 + "/col_list.pkl","wb") as f:
            pickle.dump(colz, f )
        with open(path1 + "/params.pkl","wb") as f:
            pickle.dump([ [offsetx,offsety],true_height, coords, width, height, im], f )
        with open(path1 + "/coords.pkl","wb") as f:
            pickle.dump(lst_cords, f )
        with open(path1 + "/color.pkl","wb") as f:
            pickle.dump("red", f )
        with open(path1 + "/clusters.pkl","wb") as f:
            pickle.dump(clst, f )
        with open(path1 + "/objects.pkl", "wb") as f:
            pickle.dump({"red":[], "green":[]},f)
        with open(path1 + "/all_nms.pkl", "wb") as f:
            pickle.dump([nm, nm1, srt],f)
        #with open(path1 + "/order.pkl", "wb") as f:
        #    pickle.dump(srt,f)
        with open(path1 + "/nans.pkl", "wb") as f:
            pickle.dump([],f)
        with open(path1 + "/genes_in_set.pkl", "wb") as f:
            pickle.dump([[],[],[],[],{}, "",{}],f)
        with open(path1 + "/chac_par.pkl", "wb") as f:
            pickle.dump([],f)
        with open(path1 + "/chac_par.pkl", "wb") as f:
            pickle.dump([],f)
        with open(path1 + "/percentages.pkl", "wb") as f: ########## NEW
            pickle.dump([],f)
        if srt == "None":
                    #ps = list(adata.obs[data[0]].index - path)
            ps = list(range(len(adata.obs[data[0]])))
                    #ps = [int(i) for i in ps]
            with open(path1 + "/array.pkl", "wb") as f:
                pickle.dump(ps,f)  
        if srt != "None":
            ps = adata.obs[srt] 
            ps = list(np.arange(0,len(ps))[np.isfinite(ps)])
            ##ps_inf = list(np.arange(0,len(ps))[np.isinf(ps)])
            #ps = list(ps[np.isfinite(ps)].index)
            #ps = set([int(i) for i in ps])
            ps = set(ps)
            with open(path1 + "/array.pkl", "wb") as f:
                pickle.dump(ps,f)  
            ##with open(path1 + "/array_inf.pkl", "wb") as f:
            ##    pickle.dump(ps_inf,f)  
                
        imgnm = glob.glob1(path1, "*_the_pic.png")[0]
        img = user_id + "/" + imgnm
        tmp1 = '{% load static %}<img src="{% static "'
        tmp2 = '" %}" id="graph"/>'
        html = Template(tmp1 + img + tmp2)
        print("\n5\n")
        return html, all_nms.split(",")
    
    print("AFTER___xcxcxcxcxc",request.session[request.user.username])
    #form2 = Sort()
    #form = PostForm()
    #form1 = PostForm1()
    path = os.path.dirname(os.path.realpath(__file__))
    path = "/".join(path.split("/")[:-2]) + "/static"
    remove_unwanted_data(path + "/" + request.session[request.user.username])
    #print("MMMMMMMMMM", path + "/" + request.session[request.user.username])
    #if 'stuff[x]' in request.POST:
    #    print("STUUUUUUUUUUUUUUUUUUUUF")
    #    dir_ = request.POST['stuff[x]']
    #    dir_ = request.session[request.user.username]
    #    make_dir(dir_)
    
        #important = check_location(dir_)# + "sdf")
        #print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", important)
        #if important == 0:
        #    print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", important)
        #    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
        #    os.chdir("/".join(dr.split("/")[:-2]) + "/static")
        #    return redirect('home')
            #return render(request, 'core/after_simple_upload.html', {"stop":important})


    #if len(os.getcwd().split("/")[-1]) == 32:
    
    nad = glob.glob1(path + "/" + request.session[request.user.username], "*change_data.pkl")
    nad = len(nad)
    if nad > 0:
        nad = 1

    print("CCCCCCCCCCCCCCCCC", request.POST)
    if "delete[x]" in request.POST:
        new_net = glob.glob1(path + "/" + request.session[request.user.username], "*.tab2")
        if len(new_net) > 0:
            os.remove(path + "/" + request.session[request.user.username]+"/"+new_net[0])

    new_net = glob.glob1(path + "/" + request.session[request.user.username], "*.tab2")
    if len(new_net) > 0:
        with open(path + "/" + request.session[request.user.username]+"/len_upl_netw.pkl", "rb") as f:
            net_nm, net_ln = pickle.load(f)
    if len(new_net) == 0:
        #net_nm = "networkBIOGRID-ALL-3.5.171.tab2"
        #net_ln = 0
        net_nm = ""
        if "input2[x]" in request.POST:
            net_nm = request.POST["input2[x]"]
            print("using this network:", net_nm)
            copyfile(path+"/"+net_nm, path + "/" + request.session[request.user.username]+"/"+net_nm)
            net_ln = nx.read_edgelist(path + "/" + request.session[request.user.username]+"/"+net_nm)
            net_ln = len(set(list(net_ln)))
            print(net_ln)
            with open(path + "/" + request.session[request.user.username]+"/len_upl_netw.pkl", "wb") as f:
                pickle.dump([net_nm, net_ln], f)
            
        if "input3[x]" in request.POST:
            net_nm = request.POST["input3[x]"]
            print("using this network:", net_nm)
            copyfile(path+"/"+net_nm, path + "/" + request.session[request.user.username]+"/"+net_nm)
            net_ln = nx.read_edgelist(path + "/" + request.session[request.user.username]+"/"+net_nm)
            net_ln = len(set(list(net_ln)))
            print(net_ln)
            with open(path + "/" + request.session[request.user.username]+"/len_upl_netw.pkl", "wb") as f:
                pickle.dump([net_nm, net_ln], f)
        

    parms, html, obsm, data1, data, adata = read_adata_and_more(path + "/" + request.session[request.user.username], request.session[request.user.username])
        
    if "input[x]" in request.POST:
        all_nms = request.POST["input[x]"]
        html, parms = print_plots_and_save_data(path + "/" + request.session[request.user.username],all_nms, request.session[request.user.username])


        
        
    print("\nno post\n")
    return render(request, 'core/after_simple_upload.html', {'net_nm':net_nm,'uploaded_file_stuff':data,'obsm':obsm, 'img':html, "parms":parms, "data1":data1, "nad":nad})
    #else:
    #    print("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    #    try:
    #        print("this is the one!!!!")
    #        return render(request, 'core/after_simple_upload.html', {'form1':form1,'form2':form2,
    #                                                                 'form':form, 'img':html})
    #    except:
    #    #return redirect('home')
    #        return render(request, 'core/after_simple_upload.html', {'stop':1})
    #else:
    #return redirect('home')


### perhaps insert something here, so check if the file is a h5ad format
#@login_required(login_url='home')
@csrf_protect
def simple_upload(request):

    if 'myfile' in request.FILES:
        myfile = request.FILES['myfile']
        
        if 1 == 1:
            ## and the file can be opened
            if str(myfile)[-5:] == ".h5ad":
            
                #form = UserCreationForm()
                #form.save()
                username = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
                raw_password = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
                u = User(username=username, first_name=raw_password)
                u.set_unusable_password()
                u.save()
                #u = authenticate(user=u)
                login(request, u)
                string = randomstring()
                print("xcxcxcxcxc",string)
                request.session[username] = string
                dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
                folder = "/".join(dr.split("/")[:-2]) + "/static"
                dir_ = folder+"/"+string
                os.mkdir(dir_)
                
                fs = FileSystemStorage(location = dir_)
                filename = fs.save(myfile.name, myfile)
                if 'mynetwork' in request.FILES:
                    myfile1 = request.FILES['mynetwork']
                    #if str(myfile1)[-5:] == ".tab2":
                    #df = pd.read_csv(folder + "/mart_human.txt")
                    #df=set(list(df["NCBI gene ID"]))
                    with open(folder+"/graph_n_entIDs.pkl", "rb") as f:
                        g,q = pickle.load(f)
                    fs1 = FileSystemStorage(location=dir_)
                    filename1 = fs1.save(myfile1.name+".tab2", myfile1)
                    zz = glob.glob1(dir_, "*.tab2")
                    print("NNNNNNNNNNNNNNNN",zz)
                    k = 0
                    try:
                        g = nx.read_edgelist(dir_+"/"+zz[0])
                        g = list(g)
                        print(len(g))
                        g = [int(node) for node in g]
                        g = set(g)
                        inter = len(g.intersection(q))
                        print("VHVHVHVHVHVHVH",inter)
                        if inter < 3:
                            k = 1
                    except:
                        os.remove(dir_+"/"+zz[0])
                    if k == 1:
                        os.remove(dir_+"/"+zz[0])

            zz = glob.glob1(dir_, "*.tab2")
            if len(zz) == 1:
                with open(dir_+"/len_upl_netw.pkl", "wb") as f:
                    pickle.dump( [myfile1.name, len(g)], f)


            return redirect('after_simple_upload')
        else:
            return render(request, 'core/simple_upload.html',{'hej':"Could not read the uploaded file"})
    return render(request, 'core/simple_upload.html', {'hej':"HEJ"})


@csrf_protect
def networkplot(request):
    dr = os.path.dirname(os.path.realpath(__file__))
    folder = "/".join(dr.split("/")[:-2]) + "/static"

    #if len(os.getcwd().split("/")[-1]) == 32:
    tm = request.session[request.user.username]
    path = folder + "/" + tm

    with open(path + "/interactive_graphs.pkl", "rb") as pq:
        lastn1, laste2 = pickle.load(pq)

    wach = request.GET.get('search')

    laste2=laste2[int(wach)]
    lastn1 = lastn1[int(wach)]

    laste2=list(laste2.split("'"))
    lastn1 = list(lastn1.split("'"))


    print(lastn1)
    print(lastn1[0].count("{"))
    print("*********************************")
    print(laste2)





    return render(request, 'core/networkplot.html', {'hej': "HEJ","lastn1":lastn1,"laste2":laste2,"wach":wach})


    #return render(request, 'core/networkplot.html', {'hej': "HEJ","lastn1":lastn1,"laste2":laste2})

@csrf_protect
def upload_old(request):

    print("***** You are in upload old *****")


    if 'myfile' in request.FILES:
        myfile = request.FILES['myfile']

        #if 1 == 1:
        ## and the file can be opened
        if str(myfile)[-4:] == ".zip":




            #print(str(myfile), "llllll")
            #ps = zipfile.ZipFile(str(myfile),"r")
            #pickles = []
            #pcl = 0
            #h5 = 0
            #for i in ps.namelist():
            #    if i[-4:] == ".pkl":
            #        pickles.append(i)
            #        pcl += 1
            #    if i[-5:] == ".h5ad":
            #        pickles.appens(i)
            #        h5 += 1

            #if pcl == 16 and h5 == 1:

            username = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
            raw_password = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
            u = User(username=username, first_name=raw_password)
            u.set_unusable_password()
            u.save()
            login(request, u)
            
            string = randomstring()
            print("xcxcxcxcxc",string)
            request.session[username] = string
            dr = os.path.dirname(os.path.realpath(__file__))
            folder = "/".join(dr.split("/")[:-2]) + "/static"
            dir_ = folder+"/"+string
            os.mkdir(dir_)

            fs = FileSystemStorage(location = dir_)
            filename = fs.save(myfile.name, myfile)


            print(str(myfile), "llllll")
            ps = zipfile.ZipFile(dir_+"/"+str(myfile),"r")
            pickles = []
            pcl = 0
            h5 = 0
            ntwf = 0
            for i in ps.namelist():
                if i[-4:] == ".pkl":
                    pickles.append(i)
                    pcl += 1
                if i[-5:] == ".h5ad":
                    pickles.append(i)
                    h5 += 1
                if i[-5:] == ".tab2":
                    ntwf += 1
                    pickles.append(i)
            print(pcl, h5, pickles)
            if 'all_clustering_data.pkl' in pickles:
                pickles.remove('all_clustering_data.pkl')
            if pcl >= 16 and h5 == 1 and ntwf <= 1: ## Now the true number of pkl files is 17
                for i in pickles:
                    ps.extract(i, dir_)


                FF = glob.glob1(dir_,"*.h5ad")[0]
                FF = dir_ + "/" + FF
                adata = sc.read(FF)
                if "test1.h5ad" in FF:
                    sc.tl.draw_graph(adata)

                if ntwf == 1:
                    with open(folder+"/graph_n_entIDs.pkl", "rb") as f:
                        g,q = pickle.load(f)
                    zz = glob.glob1(dir_, "*.tab2")
                    
                    print("NNNNNNNNNNNNNNNN",zz)
                    k = 0
                    try:
                        g = nx.read_edgelist(dir_+"/"+zz[0])
                        g = list(g)
                        print(len(g))
                        g = [int(node) for node in g]
                        g = set(g)
                        inter = len(g.intersection(q))
                        print("VHVHVHVHVHVHVH",inter)
                        if inter < 3:
                            k = 1
                    except:
                        os.remove(dir_+"/"+zz[0])
                    if k == 1:
                        os.remove(dir_+"/"+zz[0])
                zz = glob.glob1(dir_, "*.tab2")
                if len(zz) == 1:
                    with open(dir_+"/len_upl_netw.pkl", "wb") as f:
                        pickle.dump( [zz[0], len(g)], f)

                with open(dir_ + "/" + "all_nms.pkl","rb") as f:
                    all_nms = pickle.load(f)
                nm, nm1, srt = all_nms#.split(",")
                if nm1 == "draw_graph_fa":
                    nnm = "draw_graph"
                if nm1 != "draw_graph_fa":
                    nnm = nm1

                #nm,nm1, st = work_with_names(nm,nm1)
                st = "sc.pl.{}(adata, color=['{}'], legend_loc='on data', save='_the_pic.png', show=False, legend_fontsize=8)".format(nnm,nm)
                sc.settings.figdir = dir_
                print("----------------", st)
                exec(st)

                cls = []
                with open(dir_ + "/" + "clusters.pkl","rb") as f:
                    clusters = pickle.load(f)
                for k,v in clusters.items():
                    cls += v

                if len(cls) == 0:
                    return redirect('display_name')
                else:
                    return redirect('cluster_pick')

                #fs = FileSystemStorage(location = dir_)
                #filename = fs.save(myfile.name, myfile)
                ##login(request, user)
                #return redirect('after_simple_upload')
        else:
            return render(request, 'core/upload_old.html',{'hej':"Could not read the uploaded file - You need to have the 16 PKL files you get when you download results from Scellnetor"})
    return render(request, 'core/upload_old.html', {'hej':"HEJ"})




#@login_required(login_url='home')
@csrf_protect
def run_test1(request):
    #if 'stuff[x]' in request.POST:
    if 1 == 1:
        username = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
        raw_password = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
        u = User(username=username, first_name=raw_password)
        u.set_unusable_password()
        u.save()
        #u = authenticate(user=u)
        login(request, u)
        string = randomstring()
        print("xcxcxcxcxc",string)
        request.session[username] = string
        dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
        folder = "/".join(dr.split("/")[:-2]) + "/static"
        dir_ = folder+"/"+string
        os.mkdir(dir_)
        copyfile(folder+"/"+"test1.h5ad", dir_+"/"+"test1.h5ad")
        return redirect('after_simple_upload')
    else:
        return render(request, 'core/run_test1.html', {'hej':string})


#@login_required(login_url='home')
@csrf_protect
def run_test2(request):

    if 1 == 1:
        username = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
        raw_password = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
        u = User(username=username, first_name=raw_password)
        u.set_unusable_password()
        u.save()
        #u = authenticate(user=u)
        login(request, u)
        string = randomstring()
        print("xcxcxcxcxc",string)
        request.session[username] = string
        dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
        folder = "/".join(dr.split("/")[:-2]) + "/static"
        dir_ = folder+"/"+string
        os.mkdir(dir_)
        copyfile(folder+"/"+"test2.h5ad", dir_+"/"+"test2.h5ad")
        return redirect('after_simple_upload')
    else:
        return render(request, 'core/run_test2.html', {'hej':string})

#@login_required(login_url='home')
@csrf_protect
def run_test3(request):

    if 1 == 1:
        username = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
        raw_password = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
        u = User(username=username, first_name=raw_password)
        u.set_unusable_password()
        u.save()
        #u = authenticate(user=u)
        login(request, u)
        string = randomstring()
        print("xcxcxcxcxc",string)
        request.session[username] = string
        dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
        folder = "/".join(dr.split("/")[:-2]) + "/static"
        dir_ = folder+"/"+string
        os.mkdir(dir_)
        copyfile(folder+"/"+"test3.h5ad", dir_+"/"+"test3.h5ad")
        return redirect('after_simple_upload')
    else:
        return render(request, 'core/run_test3.html', {'hej':string})



@csrf_protect
def run_test4(request):

    if 1 == 1:
        username = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
        raw_password = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
        u = User(username=username, first_name=raw_password)
        u.set_unusable_password()
        u.save()
        #u = authenticate(user=u)
        login(request, u)
        string = randomstring()
        print("xcxcxcxcxc",string)
        request.session[username] = string
        dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
        folder = "/".join(dr.split("/")[:-2]) + "/static"
        dir_ = folder+"/"+string
        os.mkdir(dir_)
        copyfile(folder+"/"+"test4.h5ad", dir_+"/"+"test4.h5ad")
        return redirect('after_simple_upload')
    else:
        return render(request, 'core/run_test4.html', {'hej':string})


@csrf_protect
def run_test5(request):

    if 1 == 1:
        username = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
        raw_password = ''.join(random.SystemRandom().choice(streng.ascii_lowercase + streng.ascii_uppercase + streng.digits) for _ in range(32))
        u = User(username=username, first_name=raw_password)
        u.set_unusable_password()
        u.save()
        #u = authenticate(user=u)
        login(request, u)
        string = randomstring()
        print("xcxcxcxcxc",string)
        request.session[username] = string
        dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
        folder = "/".join(dr.split("/")[:-2]) + "/static"
        dir_ = folder+"/"+string
        os.mkdir(dir_)
        copyfile(folder+"/"+"test5.h5ad", dir_+"/"+"test5.h5ad")
        return redirect('after_simple_upload')
    else:
        return render(request, 'core/run_test5.html', {'hej':string})



def signup(request):
    
    username = None
    if request.user.is_authenticated():
        username = request.user.username
    
    print(username)
    
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
        else:
            print(form)
    else:
        form = UserCreationForm()
    return render(request, 'core/signup.html', {'form': form})

from django.contrib.auth import logout

def logout_view(request):
    logout(request)
    return redirect('home')


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}")
                return redirect('home')
            else:
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request = request,
                    template_name = "core/login.html",
                    context={"form":form})



def home(request):
    
    #s = SessionStore()
    # stored as seconds since epoch since datetimes are not serializable in JSON.
    #s.create()
    #print(s.session_key)
    #print(Session.objects.get().all())
    #print(request.session.session_key)
    #ip = request.META.get('REMOTE_ADDR')
    #print(ip)

    username = None
    if request.user.is_authenticated():
        username = request.user.username


    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    dr = "/".join(dr.split("/")[:-2]) + "/static"
    print("----- YOU ARE IN HOME", dr, "------")
    all_files = glob.glob1(dr,"*")
    delete_folders(all_files, dr)

    return render(request, 'core/home.html', {"user":username})
	
	
def documentation(request):

    username = None
    if request.user.is_authenticated():
        username = request.user.username


    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    dr = "/".join(dr.split("/")[:-2]) + "/static"
    print("----- YOU ARE IN DOCU", dr, "------")
    

    return render(request, 'core/documentation.html', {"user":username})
	
def publications(request):

    username = None
    if request.user.is_authenticated():
        username = request.user.username


    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    dr = "/".join(dr.split("/")[:-2]) + "/static"
    print("----- YOU ARE IN publications", dr, "------")
    

    return render(request, 'core/publications.html', {"user":username})
	
def contact(request):

    username = None
    if request.user.is_authenticated():
        username = request.user.username


    dr = os.path.dirname(os.path.realpath(__file__))#os.getcwd()
    dr = "/".join(dr.split("/")[:-2]) + "/static"
    print("----- YOU ARE IN contact", dr, "------")
    

    return render(request, 'core/contact.html', {"user":username})
