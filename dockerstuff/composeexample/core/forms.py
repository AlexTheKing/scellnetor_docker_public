from django import forms

from composeexample.core.models import Document, Post, G1, G2, Prcnt, Post1, Time


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ('description', 'document', )



class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('name',)

class PostForm1(forms.ModelForm):
    class Meta:
        model = Post1
        fields = ('name1',)


class Gr1(forms.ModelForm):
    class Meta:
        model = G1
        fields = ('group1',)


class Gr2(forms.ModelForm):
    class Meta:
        model = G2
        fields = ('group2',)


class Percent(forms.ModelForm):
    class Meta:
        model = Prcnt
        fields = ('percentage',)


class Sort(forms.ModelForm):
    class Meta:
        model = Time
        fields = ('sorting_metric',)
