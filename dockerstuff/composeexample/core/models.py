from __future__ import unicode_literals

from django.db import models


class Document(models.Model):
    description = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to='documents/')
    uploaded_at = models.DateTimeField(auto_now_add=True)


class G1(models.Model):
    group1 = models.CharField(max_length=100)

class G2(models.Model):
    group2 = models.CharField(max_length=100)


class Prcnt(models.Model):
    percentage = models.CharField(max_length=100)


class Post(models.Model):
    name = models.CharField(max_length=200)
    #uploaded_at = models.DateTimeField(auto_now_add=True)

class Post1(models.Model):
    name1 = models.CharField(max_length=200)


class Time(models.Model):
    sorting_metric = models.CharField(max_length=200, default="dpt_pseudotime")
