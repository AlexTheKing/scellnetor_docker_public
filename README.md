- [Installation](#installation)
- [Documentation](#documentation)
- [System requirements](#system-requirements)

<h3 id="Installation">Installation</h3>
Hello my dear friend!

Here you can download Scellnetor to your local computer.

**NOTE that this version of Scellnetor has been generated for local usage.**

The webtool is in a docker image that is generated using docker-compose. Follow 
this [link](https://docs.docker.com/compose/install/), if you want to install 
docker-compose:

Once this is done, all you need to do is:

1. clone the repository
2. sudo docker-compose build (takes ~1 hour :-O)
2. sudo docker-compose up
3. go to: http://0.0.0.0:8000/

and you can play with Scellnetor as much as you want.

Enjoy.


<h3 id="Documentation">Documentation</h3>
See https://exbio.wzw.tum.de/scellnetor// documentation or install the program.


<h3 id="system-requirements">System requirements</h3>
<h5>Hardware requirements</h5>
The program requires a standard computer with enough RAM to support the in-memory operations.

<h5>OS requirements</h5>
The program has only been tested on:

- Linux: Ubuntu: 16.04

However, it should work in other platforms as well.

